<?php

namespace AppBundle\Repository;

use AppBundle\Entity\User;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

class UserRepository extends EntityRepository implements UserLoaderInterface
{
    const BATTLES_PARTICIPANTS_COUNT = 5;

    /**
     * @param string $username
     * @return mixed|UserInterface
     * @throws \Symfony\Component\Security\Core\Exception\UsernameNotFoundException
     */
    public function loadUserByUsername($username)
    {
        $q = $this
            ->createQueryBuilder('u')
            ->select('u, r')
            ->leftJoin('u.roles', 'r')
            ->where('u.username = :username')
            ->setParameter('username', $username)
            ->getQuery();

        try {
            // The Query::getSingleResult() method throws an exception
            // if there is no record matching the criteria.
            $user = $q->getSingleResult();
        } catch (NoResultException $e) {
            $message = sprintf(
                'Unable to find an active admin AppBundle:User object identified by "%s".',
                $username
            );
            throw new UsernameNotFoundException($message, 0, $e);
        }

        return $user;
    }

    /**
     * @param UserInterface | User $user
     * @return null|object|UserInterface
     * @throws \Symfony\Component\Security\Core\Exception\UnsupportedUserException
     */
    public function refreshUser(UserInterface $user)
    {
        $class = get_class($user);
        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(
                sprintf(
                    'Instances of "%s" are not supported.',
                    $class
                )
            );
        }

        return $this->find($user->getId());
    }

    /**
     * @param string $class
     * @return bool
     */
    public function supportsClass($class)
    {
        return $this->getEntityName() === $class
        || is_subclass_of($class, $this->getEntityName());
    }

    /**
     * @param int $userId
     * @return array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getParticipantsForUserId($userId)
    {
        $em = $this->getEntityManager();
        $connection = $em->getConnection();

        $ids = [];
        $participants = [];

        for ($counter = 0; $counter < self::BATTLES_PARTICIPANTS_COUNT; $counter++) {
            $sql = <<<SQL
SELECT id
FROM users
WHERE NOT (id = ANY (('{' || :ids || '}') :: INT [])) AND id != :user_id
OFFSET floor(random() * (SELECT count(*)
                         FROM users
                         WHERE NOT (id = ANY (('{' || :ids || '}') :: INT [])) AND id != :user_id))
LIMIT 1
SQL;

            if ($id = $connection->fetchColumn($sql, [
                'ids' => implode(',', $ids),
                'user_id' => $userId,
            ])) {
                $ids[] = $id;
                $participants[] = $em->find($this->getEntityName(), $id);
            }
        }

        return $participants;
    }
}
