<?php

namespace AppBundle\Entity\Autograph;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="autograph_crowd_person_fans")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Autograph\CrowdPersonFanRepository")
 */
class CrowdPersonFan extends CrowdPerson
{
    const FAN_1 = 'fan_1';
    const FAN_2 = 'fan_2';
    const FAN_3 = 'fan_3';
    const FAN_4 = 'fan_4';
    const FAN_EXTRA_1 = 'fan_extra_1';
    const FAN_EXTRA_2 = 'fan_extra_2';
    const FAN_EXTRA_3 = 'fan_extra_3';

    /**
     * @var array
     *
     * @ORM\Column(name="reward", type="json_array", options={"default": "[]"})
     */
    private $reward = [];

    public function getType()
    {
        return self::FAN_TYPE;
    }

    /**
     * @return array
     */
    public function getReward()
    {
        return $this->reward;
    }

    /**
     * @param array $reward
     */
    public function setReward($reward)
    {
        $this->reward = $reward;
    }
}