<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="user_profiles")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserProfileRepository")
 * @UniqueEntity(fields="email", message="Пользователь с таким e-mail уже существует")
 * @ORM\HasLifecycleCallbacks
 * @ORM\EntityListeners({
 *     "AppBundle\Service\Listener\NextLevelForProfileSetterListener",
 *     "AppBundle\Service\Listener\EnergyCheckForUpdateListener"
 * })
 */
class UserProfile
{
    const DEFAULT_ACTING = 5;
    const DEFAULT_SPORTS_TRAINING = 5;
    const DEFAULT_POPULARITY = 5;
    const DEFAULT_ENERGY = 50;
    const DEFAULT_ENERGY_PERIOD_SECONDS = 30;
    const DEFAULT_MANI = 2000;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\User", inversedBy="profile")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @var UserProfileUserThingRelation|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\UserProfileUserThingRelation", mappedBy="profile")
     */
    private $userProfileUserThingRelations;

    /**
     * @var int
     *
     * @ORM\Column(name="mani", type="bigint", options={"default": UserProfile::DEFAULT_MANI})
     */
    private $mani = self::DEFAULT_MANI;

    /**
     * @var int
     *
     * @ORM\Column(name="dances", type="bigint", options={"default": 0})
     */
    private $dances = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="acting", type="bigint", options={"default": UserProfile::DEFAULT_ACTING})
     */
    private $acting = self::DEFAULT_ACTING;

    /**
     * @var int
     *
     * @ORM\Column(name="sports_training", type="bigint", options={"default": UserProfile::DEFAULT_SPORTS_TRAINING})
     */
    private $sportsTraining = self::DEFAULT_SPORTS_TRAINING;

    /**
     * @var int
     *
     * @ORM\Column(name="popularity", type="bigint", options={"default": UserProfile::DEFAULT_POPULARITY})
     */
    private $popularity = self::DEFAULT_POPULARITY;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=true)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birth_date", type="date", nullable=true)
     */
    private $birthDate;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", nullable=true)
     *
     * @Assert\Regex("/^\d+$/")
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", nullable=true, length=60, unique=true)
     *
     * @Assert\Email(
     *     message = "Неврный формат email."
     * )
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="sex", type="string", length=1, columnDefinition="VARCHAR(1) NOT NULL CHECK (sex IN ('f','m'))")
     */
    private $sex;

    /**
     * @var int
     *
     * @ORM\Column(name="exp", type="bigint", options={"default": 0})
     */
    private $exp = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="rating", type="bigint", options={"default": 0})
     */
    private $rating = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="avatar", type="string", nullable=true)
     */
    private $avatar;

    /**
     * @var UserLevel
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\UserLevel", inversedBy="profiles")
     * @ORM\JoinColumn(name="level_id", referencedColumnName="id", nullable=false)
     */
    private $level;

    /**
     * @var UserLevel
     */
    private $nextLevel;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="energy_update_date", type="datetime")
     */
    private $energyUpdateDate;

    /**
     * @var int
     *
     * @ORM\Column(name="energy", type="bigint", options={"default": UserProfile::DEFAULT_ENERGY})
     */
    private $energy = self::DEFAULT_ENERGY;

    /**
     * @var int
     *
     * @ORM\Column(name="energy_max", type="bigint", options={"default": UserProfile::DEFAULT_ENERGY})
     */
    private $energyMax = self::DEFAULT_ENERGY;

    /**
     * @var int
     *
     * @ORM\Column(name="energy_period_seconds", type="integer", options={"default": UserProfile::DEFAULT_ENERGY_PERIOD_SECONDS})
     */
    private $energyPeriodSeconds = self::DEFAULT_ENERGY_PERIOD_SECONDS;

    public function __construct()
    {
        $this->userProfileUserThingRelations = new ArrayCollection();
    }

    /** @ORM\PrePersist */
    public function prePersist()
    {
        $this->setEnergyUpdateDate(new \DateTime());
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getMani()
    {
        return $this->mani;
    }

    /**
     * @param int $mani
     */
    public function setMani($mani)
    {
        $this->mani = $mani;
    }

    /**
     * @return int
     */
    public function getDances()
    {
        return $this->dances;
    }

    /**
     * @param int $dances
     */
    public function setDances($dances)
    {
        $this->dances = $dances;
    }

    /**
     * @return UserProfileUserThingRelation|ArrayCollection
     */
    public function getUserProfileUserThingRelations()
    {
        return $this->userProfileUserThingRelations;
    }

    /**
     * @param UserProfileUserThingRelation|ArrayCollection $userProfileUserThingRelations
     */
    public function setUserProfileUserThingRelations($userProfileUserThingRelations)
    {
        $this->userProfileUserThingRelations = $userProfileUserThingRelations;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * @param \DateTime $birthDate
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * @param string $sex
     */
    public function setSex($sex)
    {
        $this->sex = $sex;
    }

    /**
     * @return int
     */
    public function getExp()
    {
        return $this->exp;
    }

    /**
     * @param int $exp
     */
    public function setExp($exp)
    {
        $this->exp = $exp;
    }

    /**
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param string $avatar
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }

    /**
     * @return UserLevel
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param UserLevel $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }

    /**
     * @return UserLevel
     */
    public function getNextLevel()
    {
        return $this->nextLevel;
    }

    /**
     * @param UserLevel $nextLevel
     */
    public function setNextLevel($nextLevel)
    {
        $this->nextLevel = $nextLevel;
    }

    /**
     * @return int
     */
    public function getActing()
    {
        return $this->acting;
    }

    /**
     * @param int $acting
     */
    public function setActing($acting)
    {
        $this->acting = $acting;
    }

    /**
     * @return int
     */
    public function getSportsTraining()
    {
        return $this->sportsTraining;
    }

    /**
     * @param int $sportsTraining
     */
    public function setSportsTraining($sportsTraining)
    {
        $this->sportsTraining = $sportsTraining;
    }

    /**
     * @return int
     */
    public function getPopularity()
    {
        return $this->popularity;
    }

    /**
     * @param int $popularity
     */
    public function setPopularity($popularity)
    {
        $this->popularity = $popularity;
    }

    /**
     * @return int
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param int $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }

    /**
     * @return \DateTime
     */
    public function getEnergyUpdateDate()
    {
        return $this->energyUpdateDate;
    }

    /**
     * @param \DateTime $energyUpdateDate
     */
    public function setEnergyUpdateDate($energyUpdateDate)
    {
        $this->energyUpdateDate = $energyUpdateDate;
    }

    /**
     * @return int
     */
    public function getEnergy()
    {
        return $this->energy;
    }

    /**
     * @param int $energy
     */
    public function setEnergy($energy)
    {
        $this->energy = $energy;
    }

    /**
     * @return int
     */
    public function getEnergyMax()
    {
        return $this->energyMax;
    }

    /**
     * @param int $energyMax
     */
    public function setEnergyMax($energyMax)
    {
        $this->energyMax = $energyMax;
    }

    /**
     * @return int
     */
    public function getEnergyPeriodSeconds()
    {
        return $this->energyPeriodSeconds;
    }

    /**
     * @param int $energyPeriodSeconds
     */
    public function setEnergyPeriodSeconds($energyPeriodSeconds)
    {
        $this->energyPeriodSeconds = $energyPeriodSeconds;
    }
}
