<?php

namespace AppBundle\Service\Listener;

use AppBundle\Entity\Achievement\AchievementType;
use AppBundle\Service\Event\RewardEvent;
use AppBundle\StoreEvents;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class ParseRewardForAchievementsListener
{
    /**
     * @param RewardEvent $event
     * @param string $eventName
     * @param EventDispatcherInterface $dispatcher
     */
    public function onParse(RewardEvent $event, $eventName, EventDispatcherInterface $dispatcher)
    {
        foreach ($event->getReward() as $entityClass => $params) {
            foreach ($params as $paramName => $value) {
                switch ($paramName) {
                    case 'exp':
                        $this->dispatchExpEvents($event, $dispatcher, $value);
                        break;
                    case 'dances':
                        $this->dispatchDancesEvents($event, $dispatcher, $value);
                        break;
                    case 'mani':
                        $this->dispatchManiEvents($event, $dispatcher, $value);
                        break;
                }
            }
        }
    }

    /**
     * @param RewardEvent $event
     * @param EventDispatcherInterface $dispatcher
     * @param int $value
     */
    private function dispatchExpEvents(RewardEvent $event, EventDispatcherInterface $dispatcher, $value)
    {
        $event->setIncreaseOn($value);
        $event->setAchievementParticleCode(AchievementType::PARAM_EXP_1);
        $dispatcher->dispatch(StoreEvents::USER_REWARD_EXP, $event);
    }

    /**
     * @param RewardEvent $event
     * @param EventDispatcherInterface $dispatcher
     * @param int $value
     */
    private function dispatchDancesEvents(RewardEvent $event, EventDispatcherInterface $dispatcher, $value)
    {
        $event->setIncreaseOn($value);
        $event->setAchievementParticleCode(AchievementType::PARAM_DANCES_1);
        $dispatcher->dispatch(StoreEvents::USER_REWARD_DANCES, $event);
    }

    /**
     * @param RewardEvent $event
     * @param EventDispatcherInterface $dispatcher
     * @param int $value
     */
    private function dispatchManiEvents(RewardEvent $event, EventDispatcherInterface $dispatcher, $value)
    {
        $event->setIncreaseOn($value);
        $event->setAchievementParticleCode(AchievementType::PARAM_MANI_1);
        $dispatcher->dispatch(StoreEvents::USER_REWARD_MANI, $event);
    }
}