<?php

namespace AppBundle\Service\BonusSystem\Source\User;

use AppBundle\Service\BonusSystem\Source\AbstractSource;
use AppBundle\Service\BonusSystem\Source\SourceInterface;

class EventsSource extends AbstractSource implements SourceInterface
{
    /**
     * @param mixed $object
     * @return array
     */
    public function extractBonuses($object):array
    {
        return [
            'bonus4' => ['expireAt' => time() + 30],
            'bonus5' => ['expireAt' => time() + 20],
            'bonus6' => ['expireAt' => time() + 10],
        ];
    }
}
