<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="user_things")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserThingRepository")
 */
class UserThing
{
    const TOP = 'top';
    const HAT = 'hat';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var UserProfileUserThingRelation[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\UserProfileUserThingRelation", mappedBy="thing")
     */
    private $userProfileUserThingRelations;

    /**
     * @var int
     *
     * @ORM\Column(name="layer_level", type="integer")
     */
    private $layerLevel;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string")
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="sex", type="string", length=1, nullable=true, columnDefinition="VARCHAR(1) CHECK (sex IN ('f','m'))")
     */
    private $sex;

    /**
     * @var array
     *
     * @ORM\Column(name="cost", type="json_array", options={"default": "[]"})
     */
    private $cost = [];

    /**
     * @var string
     *
     * @ORM\Column(name="img", type="string", unique=true)
     */
    private $img;

    /**
     * @var UserThingCategory
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\UserThingCategory", inversedBy="things")
     */
    private $category;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getLayerLevel()
    {
        return $this->layerLevel;
    }

    /**
     * @param int $layerLevel
     */
    public function setLayerLevel($layerLevel)
    {
        $this->layerLevel = $layerLevel;
    }

    /**
     * @return UserProfileUserThingRelation[]|ArrayCollection
     */
    public function getUserProfileUserThingRelations()
    {
        return $this->userProfileUserThingRelations;
    }

    /**
     * @param UserProfileUserThingRelation[]|ArrayCollection $userProfileUserThingRelations
     */
    public function setUserProfileUserThingRelations($userProfileUserThingRelations)
    {
        $this->userProfileUserThingRelations = $userProfileUserThingRelations;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * @param string $sex
     */
    public function setSex($sex)
    {
        $this->sex = $sex;
    }

    /**
     * @return string
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * @param string $img
     */
    public function setImg($img)
    {
        $this->img = $img;
    }

    /**
     * @return array
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @param array $cost
     */
    public function setCost($cost)
    {
        $this->cost = $cost;
    }

    /**
     * @return UserThingCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param UserThingCategory $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }
}
