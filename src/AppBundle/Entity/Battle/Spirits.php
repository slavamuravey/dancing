<?php

namespace AppBundle\Entity\Battle;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="battle_spirits")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Battle\SpiritsRepository")
 */
class Spirits
{
    const CODE_BAD = 'bad';
    const CODE_NORMAL = 'normal';
    const CODE_GOOD = 'good';

    const VALUE_BAD = 130;
    const VALUE_NORMAL = 100;
    const VALUE_GOOD = 70;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", unique=true)
     */
    private $code;

    /**
     * @var int
     *
     * @ORM\Column(name="value", type="integer")
     */
    private $value;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param int $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
}