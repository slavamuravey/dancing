<?php

namespace AppBundle\Entity\Autograph;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="autograph_cities")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Autograph\CityRepository")
 */
class City
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="number", type="integer", unique=true)
     */
    private $number;

    /**
     * @var int
     *
     * @ORM\Column(name="steps", type="integer")
     */
    private $steps;

    /**
     * @var ArrayCollection|CityCrowdPersonRelation[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Autograph\CityCrowdPersonRelation", mappedBy="city")
     */
    private $cityCrowdPersonRelations;

    /**
     * @var ArrayCollection|CrowdPersonProducerUserRelation[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Autograph\CrowdPersonProducerUserRelation", mappedBy="city")
     */
    private $crowdPersonProducerUserRelations;

    public function __construct()
    {
        $this->cityCrowdPersonRelations = new ArrayCollection();
        $this->crowdPersonProducerUserRelations = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param int $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return int
     */
    public function getSteps()
    {
        return $this->steps;
    }

    /**
     * @param int $steps
     */
    public function setSteps($steps)
    {
        $this->steps = $steps;
    }

    /**
     * @return CityCrowdPersonRelation[]|ArrayCollection
     */
    public function getCityCrowdPersonRelations()
    {
        return $this->cityCrowdPersonRelations;
    }

    /**
     * @param CityCrowdPersonRelation[]|ArrayCollection $cityCrowdPersonRelations
     */
    public function setCityCrowdPersonRelations($cityCrowdPersonRelations)
    {
        $this->cityCrowdPersonRelations = $cityCrowdPersonRelations;
    }

    /**
     * @return CrowdPersonProducerUserRelation[]|ArrayCollection
     */
    public function getCrowdPersonProducerUserRelations()
    {
        return $this->crowdPersonProducerUserRelations;
    }

    /**
     * @param CrowdPersonProducerUserRelation[]|ArrayCollection $crowdPersonProducerUserRelations
     */
    public function setCrowdPersonProducerUserRelations($crowdPersonProducerUserRelations)
    {
        $this->crowdPersonProducerUserRelations = $crowdPersonProducerUserRelations;
    }

    /**
     * @return CrowdPerson
     */
    public function getRandomCrowdPerson()
    {
        $cityCrowdPersonRelations = $this->getCityCrowdPersonRelations()->toArray();
        shuffle($cityCrowdPersonRelations);

        /** @var CityCrowdPersonRelation $cityCrowdPersonRelation */
        $cityCrowdPersonRelation = current($cityCrowdPersonRelations);

        return $cityCrowdPersonRelation->getCrowdPerson();
    }

    /**
     * @return CrowdPerson
     */
    public function getRandomCrowdPersonFan()
    {
        $cityCrowdPersonRelations = $this->getCityCrowdPersonRelations()->filter(function ($rel) {
            /** @var CityCrowdPersonRelation $rel */
            return $rel->getCrowdPerson() instanceof CrowdPersonFan;
        })->toArray();
        shuffle($cityCrowdPersonRelations);

        /** @var CityCrowdPersonRelation $cityCrowdPersonRelation */
        $cityCrowdPersonRelation = current($cityCrowdPersonRelations);

        return $cityCrowdPersonRelation->getCrowdPerson();
    }
}