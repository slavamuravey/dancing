<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Achievement\AchievementParticle;
use AppBundle\Entity\Achievement\AchievementQuest;
use AppBundle\Entity\Achievement\AchievementStatus;
use AppBundle\Entity\UserProfile;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadAchievementsData implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $achievementQuestAcquaintance = new AchievementQuest();
        $achievementQuestAcquaintance->setCode('acquaintance');
        $achievementQuestAcquaintance->setReward([
            UserProfile::class => [
                'mani' => 300,
                'exp' => 1000,
            ],
        ]);
        $manager->persist($achievementQuestAcquaintance);

        $achievementQuestFirstSteps = new AchievementQuest();
        $achievementQuestFirstSteps->setCode('first_steps');
        $achievementQuestFirstSteps->setReward([
            UserProfile::class => [
                'mani' => 300,
                'exp' => 1000,
            ],
        ]);
        $manager->persist($achievementQuestFirstSteps);

        $achievementStatusA = new AchievementStatus();
        $achievementStatusA->setCode('a');
        $achievementStatusA->setBuffs([
            UserProfile::class => [
                'mani' => 300,
                'exp' => 1000,
            ],
        ]);
        $manager->persist($achievementStatusA);

        $achievementStatusB = new AchievementStatus();
        $achievementStatusB->setCode('b');
        $achievementStatusB->setBuffs([
            UserProfile::class => [
                'mani' => 300,
                'exp' => 1000,
            ],
        ]);
        $manager->persist($achievementStatusB);

        $achievementParticle = new AchievementParticle();
        $achievementParticle->setCode('client_common_count_1');
        $achievementParticle->setRequiredVolume(20);
        $achievementParticle->setAchievementType($achievementStatusB);
        $manager->persist($achievementParticle);

        $achievementParticle = new AchievementParticle();
        $achievementParticle->setCode('profile_visit_1');
        $achievementParticle->setRequiredVolume(1);
        $achievementParticle->setAchievementType($achievementStatusB);
        $manager->persist($achievementParticle);

        $achievementParticle = new AchievementParticle();
        $achievementParticle->setCode('client_factory_count_1');
        $achievementParticle->setRequiredVolume(5);
        $achievementParticle->setAchievementType($achievementStatusA);
        $manager->persist($achievementParticle);

        $achievementParticle = new AchievementParticle();
        $achievementParticle->setCode('client_club_count_1');
        $achievementParticle->setRequiredVolume(3);
        $achievementParticle->setAchievementType($achievementStatusA);
        $manager->persist($achievementParticle);

        $achievementParticle = new AchievementParticle();
        $achievementParticle->setCode('param_exp_1');
        $achievementParticle->setRequiredVolume(30);
        $achievementParticle->setAchievementType($achievementQuestAcquaintance);
        $manager->persist($achievementParticle);

        $achievementParticle = new AchievementParticle();
        $achievementParticle->setCode('level_up_1');
        $achievementParticle->setRequiredVolume(1);
        $achievementParticle->setAchievementType($achievementQuestAcquaintance);
        $manager->persist($achievementParticle);

        $achievementParticle = new AchievementParticle();
        $achievementParticle->setCode('param_mani_1');
        $achievementParticle->setRequiredVolume(100);
        $achievementParticle->setAchievementType($achievementQuestFirstSteps);
        $manager->persist($achievementParticle);

        $achievementParticle = new AchievementParticle();
        $achievementParticle->setCode('param_dances_1');
        $achievementParticle->setRequiredVolume(10);
        $achievementParticle->setAchievementType($achievementQuestFirstSteps);
        $manager->persist($achievementParticle);

        $achievementParticle = new AchievementParticle();
        $achievementParticle->setCode('client_count_1');
        $achievementParticle->setRequiredVolume(5);
        $manager->persist($achievementParticle);

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 4;
    }
}