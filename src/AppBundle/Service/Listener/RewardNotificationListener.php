<?php

namespace AppBundle\Service\Listener;

use AppBundle\Service\Event\RewardEvent;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class RewardNotificationListener
{
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @param SessionInterface $session
     */
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @param RewardEvent $event
     */
    public function onNotify(RewardEvent $event)
    {
        $this->session->getFlashBag()->add('reward', 'получено ' . json_encode($event->getReward()));
    }
}