<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="user_profile_user_thing_relations", uniqueConstraints={@ORM\UniqueConstraint(name="uniq_profile_id_thing_id", columns={"profile_id", "thing_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserProfileUserThingRelationRepository")
 */
class UserProfileUserThingRelation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var UserProfile
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\UserProfile", inversedBy="userProfileUserThingRelations")
     * @ORM\JoinColumn(name="profile_id", referencedColumnName="id")
     */
    private $profile;

    /**
     * @var UserThing
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\UserThing", inversedBy="userProfileUserThingRelations")
     * @ORM\JoinColumn(name="thing_id", referencedColumnName="id")
     */
    private $thing;

    /**
     * @var bool
     *
     * @ORM\Column(name="wearing", type="boolean", options={"default": false})
     */
    private $wearing = false;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return UserProfile
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @param UserProfile $profile
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;
    }

    /**
     * @return UserThing
     */
    public function getThing()
    {
        return $this->thing;
    }

    /**
     * @param UserThing $thing
     */
    public function setThing($thing)
    {
        $this->thing = $thing;
    }

    /**
     * @return boolean
     */
    public function isWearing()
    {
        return $this->wearing;
    }

    /**
     * @param boolean $wearing
     */
    public function setWearing($wearing)
    {
        $this->wearing = $wearing;
    }
}