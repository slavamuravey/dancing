<?php

namespace AppBundle\Service\Listener;

use AppBundle\Entity\UserEvent;
use AppBundle\Service\Event\AbstractEvent;
use Doctrine\ORM\EntityManager;

class JournalEventListener
{
    /** @var EntityManager */
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function onJournal(AbstractEvent $event, $eventName)
    {
        $userEvent = new UserEvent();
        $userEvent->setUser($event->getUser());
        $userEvent->setEventName($eventName);
        $userEvent->setArguments($event->getJournalParams());
        $this->em->persist($userEvent);
        $this->em->flush($userEvent);
    }
}