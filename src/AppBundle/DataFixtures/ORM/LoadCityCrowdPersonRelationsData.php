<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Autograph\City;
use AppBundle\Entity\Autograph\CityCrowdPersonRelation;
use AppBundle\Entity\Autograph\CrowdPerson;
use AppBundle\Entity\Autograph\CrowdPersonFan;
use AppBundle\Entity\Autograph\CrowdPersonHater;
use AppBundle\Repository\Autograph\CityRepository;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadCityCrowdPersonRelationsData implements FixtureInterface, OrderedFixtureInterface
{
    private static $personsCitiesMap = [
        1 => [
            CrowdPersonFan::FAN_1,
            CrowdPersonFan::FAN_2,
            CrowdPersonFan::FAN_3,
            CrowdPersonFan::FAN_4,
            CrowdPersonHater::HATER_LIGHT_1,
        ],
        2 => [
            CrowdPersonFan::FAN_1,
            CrowdPersonFan::FAN_2,
            CrowdPersonFan::FAN_3,
            CrowdPersonFan::FAN_4,
            CrowdPersonHater::HATER_LIGHT_1,
            CrowdPersonHater::HATER_LIGHT_2,
        ],
        3 => [
            CrowdPersonFan::FAN_1,
            CrowdPersonFan::FAN_2,
            CrowdPersonFan::FAN_3,
            CrowdPersonFan::FAN_4,
            CrowdPersonHater::HATER_LIGHT_1,
            CrowdPersonHater::HATER_LIGHT_2,
            CrowdPersonHater::HATER_NORMAL_1,
        ],
        4 => [
            CrowdPersonFan::FAN_1,
            CrowdPersonFan::FAN_2,
            CrowdPersonFan::FAN_3,
            CrowdPersonFan::FAN_4,
            CrowdPersonHater::HATER_LIGHT_1,
            CrowdPersonHater::HATER_LIGHT_2,
            CrowdPersonHater::HATER_NORMAL_1,
            CrowdPersonHater::HATER_NORMAL_2,
        ],
        5 => [
            CrowdPersonFan::FAN_1,
            CrowdPersonFan::FAN_2,
            CrowdPersonFan::FAN_3,
            CrowdPersonFan::FAN_4,
            CrowdPersonHater::HATER_LIGHT_1,
            CrowdPersonHater::HATER_LIGHT_2,
            CrowdPersonHater::HATER_NORMAL_1,
            CrowdPersonHater::HATER_NORMAL_2,
            CrowdPersonHater::HATER_STRONG_1,
        ],
        6 => [
            CrowdPersonFan::FAN_1,
            CrowdPersonFan::FAN_2,
            CrowdPersonFan::FAN_3,
            CrowdPersonFan::FAN_4,
            CrowdPersonFan::FAN_EXTRA_1,
            CrowdPersonHater::HATER_LIGHT_1,
            CrowdPersonHater::HATER_LIGHT_2,
            CrowdPersonHater::HATER_NORMAL_1,
            CrowdPersonHater::HATER_NORMAL_2,
            CrowdPersonHater::HATER_STRONG_1,
            CrowdPersonHater::HATER_STRONG_2,
        ],
        7 => [
            CrowdPersonFan::FAN_1,
            CrowdPersonFan::FAN_2,
            CrowdPersonFan::FAN_3,
            CrowdPersonFan::FAN_4,
            CrowdPersonFan::FAN_EXTRA_1,
            CrowdPersonFan::FAN_EXTRA_2,
            CrowdPersonHater::HATER_LIGHT_1,
            CrowdPersonHater::HATER_LIGHT_2,
            CrowdPersonHater::HATER_NORMAL_1,
            CrowdPersonHater::HATER_NORMAL_2,
            CrowdPersonHater::HATER_STRONG_1,
            CrowdPersonHater::HATER_STRONG_2,
        ],
        8 => [
            CrowdPersonFan::FAN_1,
            CrowdPersonFan::FAN_2,
            CrowdPersonFan::FAN_3,
            CrowdPersonFan::FAN_4,
            CrowdPersonFan::FAN_EXTRA_1,
            CrowdPersonFan::FAN_EXTRA_2,
            CrowdPersonFan::FAN_EXTRA_3,
            CrowdPersonHater::HATER_LIGHT_1,
            CrowdPersonHater::HATER_LIGHT_2,
            CrowdPersonHater::HATER_NORMAL_1,
            CrowdPersonHater::HATER_NORMAL_2,
            CrowdPersonHater::HATER_STRONG_1,
            CrowdPersonHater::HATER_STRONG_2,
        ],
        9 => [
            CrowdPersonFan::FAN_1,
            CrowdPersonFan::FAN_2,
            CrowdPersonFan::FAN_3,
            CrowdPersonFan::FAN_4,
            CrowdPersonFan::FAN_EXTRA_1,
            CrowdPersonFan::FAN_EXTRA_2,
            CrowdPersonFan::FAN_EXTRA_3,
            CrowdPersonHater::HATER_LIGHT_1,
            CrowdPersonHater::HATER_LIGHT_2,
            CrowdPersonHater::HATER_NORMAL_1,
            CrowdPersonHater::HATER_NORMAL_1,
            CrowdPersonHater::HATER_NORMAL_2,
            CrowdPersonHater::HATER_NORMAL_2,
            CrowdPersonHater::HATER_STRONG_1,
            CrowdPersonHater::HATER_STRONG_1,
            CrowdPersonHater::HATER_STRONG_2,
            CrowdPersonHater::HATER_STRONG_2,
        ],
        10 => [
            CrowdPersonFan::FAN_1,
            CrowdPersonFan::FAN_2,
            CrowdPersonFan::FAN_3,
            CrowdPersonFan::FAN_4,
            CrowdPersonFan::FAN_EXTRA_1,
            CrowdPersonFan::FAN_EXTRA_2,
            CrowdPersonFan::FAN_EXTRA_3,
            CrowdPersonHater::HATER_LIGHT_1,
            CrowdPersonHater::HATER_LIGHT_2,
            CrowdPersonHater::HATER_NORMAL_1,
            CrowdPersonHater::HATER_NORMAL_1,
            CrowdPersonHater::HATER_NORMAL_1,
            CrowdPersonHater::HATER_NORMAL_2,
            CrowdPersonHater::HATER_NORMAL_2,
            CrowdPersonHater::HATER_NORMAL_2,
            CrowdPersonHater::HATER_STRONG_1,
            CrowdPersonHater::HATER_STRONG_1,
            CrowdPersonHater::HATER_STRONG_1,
            CrowdPersonHater::HATER_STRONG_2,
            CrowdPersonHater::HATER_STRONG_2,
            CrowdPersonHater::HATER_STRONG_2,
        ],
    ];

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        /** @var CityRepository $cityRepository */
        $cityRepository = $manager->getRepository(City::class);

        $personRepository = $manager->getRepository(CrowdPerson::class);

        for ($i = 1; $i <= 30; $i++) {
            $city = $cityRepository->getCityByNumber($i);
            $key = array_key_exists($i, self::$personsCitiesMap) ? $i : 10;

            foreach (self::$personsCitiesMap[$key] as $personCode) {
                /** @var CrowdPerson $person */
                $person = $personRepository->findOneBy(['code' => $personCode]);
                $cityCrowdPersonRelation = new CityCrowdPersonRelation();
                $cityCrowdPersonRelation->setCity($city);
                $cityCrowdPersonRelation->setCrowdPerson($person);
                $manager->persist($cityCrowdPersonRelation);
            }
        }

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 10;
    }
}