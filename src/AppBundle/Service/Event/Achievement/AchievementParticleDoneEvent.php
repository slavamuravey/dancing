<?php

namespace AppBundle\Service\Event\Achievement;

use AppBundle\Entity\Achievement\AchievementParticleUserRelation;
use AppBundle\Service\Event\AbstractEvent;

class AchievementParticleDoneEvent extends AbstractEvent
{
    /** @var AchievementParticleUserRelation */
    private $achievementParticleUserRelation;

    /**
     * @return AchievementParticleUserRelation
     */
    public function getAchievementParticleUserRelation()
    {
        return $this->achievementParticleUserRelation;
    }

    /**
     * @param AchievementParticleUserRelation $achievementParticleUserRelation
     */
    public function setAchievementParticleUserRelation(AchievementParticleUserRelation $achievementParticleUserRelation)
    {
        $this->achievementParticleUserRelation = $achievementParticleUserRelation;
    }
}