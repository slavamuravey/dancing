<?php

namespace AppBundle\Service\BonusSystem;

use AppBundle\Service\BonusSystem\Modifier\ModifierInterface;

class Effect
{
    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $type;

    /**
     * @var mixed
     */
    private $value;

    /**
     * @var Bonus
     */
    private $bonus;

    /**
     * @var int
     */
    private $priority;

    /**
     * @var ModifierInterface
     */
    private $modifier;

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return Bonus
     */
    public function getBonus():Bonus
    {
        return $this->bonus;
    }

    /**
     * @param Bonus $bonus
     */
    public function setBonus(Bonus $bonus)
    {
        $this->bonus = $bonus;
    }

    /**
     * @return int
     */
    public function getPriority():int
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     */
    public function setPriority(int $priority)
    {
        $this->priority = $priority;
    }

    /**
     * @return ModifierInterface
     */
    public function getModifier()
    {
        return $this->modifier;
    }

    /**
     * @param ModifierInterface $modifier
     */
    public function setModifier($modifier)
    {
        $this->modifier = $modifier;
    }
}