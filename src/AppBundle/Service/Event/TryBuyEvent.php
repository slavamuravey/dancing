<?php

namespace AppBundle\Service\Event;

class TryBuyEvent extends AbstractEvent
{
    /** @var array */
    private $cost = [];

    /**
     * @return array
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @param array $cost
     */
    public function setCost(array $cost)
    {
        $this->cost = $cost;
    }
}