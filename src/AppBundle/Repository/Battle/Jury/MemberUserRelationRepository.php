<?php

namespace AppBundle\Repository\Battle\Jury;

use AppBundle\Entity\Battle\Jury\MemberUserRelation;
use AppBundle\Entity\Battle\Stage;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

abstract class MemberUserRelationRepository extends EntityRepository
{
    /**
     * @param User $user
     * @param Stage $stage
     * @return null|MemberUserRelation
     */
    public function findRelationByUserStage(User $user, Stage $stage)
    {
        return $this->findOneBy(['user' => $user, 'stage' => $stage,]);
    }

    /**
     * @param User $user
     * @param Stage $stage
     * @return MemberUserRelation|null
     */
    public function findOrCreateByUserStage(User $user, Stage $stage)
    {
        if (!($relation = $this->findRelationByUserStage($user, $stage))) {
            $entityName = $this->getEntityName();
            /** @var MemberUserRelation $relation */
            $relation = new $entityName;
            $relation->setUser($user);
            $relation->setStage($stage);
            $em = $this->getEntityManager();
            $em->persist($relation);
            $em->flush($relation);
        }

        return $relation;
    }
}
