<?php

namespace AppBundle\Service\Event;

use AppBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

abstract class AbstractEvent extends Event
{
    /** @var User */
    private $user;

    /** @var array */
    private $journalParams;

    /**
     * @param User $user
     * @param array $journalParams
     */
    public function __construct(User $user, array $journalParams = [])
    {
        $this->user = $user;
        $this->journalParams = $journalParams;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return array
     */
    public function getJournalParams()
    {
        return $this->journalParams;
    }

    /**
     * @param array $journalParams
     */
    public function setJournalParams(array $journalParams)
    {
        $this->journalParams = $journalParams;
    }
}