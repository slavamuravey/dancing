<?php

namespace AppBundle\Repository\Battle;

use Doctrine\ORM\EntityRepository;

class StageRepository extends EntityRepository
{
    public function fillStagesData()
    {
        $connection = $this->getEntityManager()->getConnection();

        $sql = <<<SQL
INSERT INTO battle_stages (code, stage_value) VALUES
('stage1', 20),
('stage2', 70),
('stage3', 210),
('stage4', 340),
('stage5', 550),
('stage6', 700),
('stage7', 900),
('stage8', 1100),
('stage9', 1250),
('stage10', 1450),
('stage11', 1700)
SQL;

        $connection->executeQuery($sql);
    }
}
