<?php

namespace AppBundle\Entity\Achievement;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="achievement_particles")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Achievement\AchievementParticleRepository")
 */
class AchievementParticle extends AchievementType
{
    /**
     * @var int
     *
     * @ORM\Column(name="required_volume", type="bigint", options={"default": 0})
     */
    private $requiredVolume;

    /**
     * @var ArrayCollection|AchievementParticleUserRelation[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Achievement\AchievementParticleUserRelation", mappedBy="achievementParticle")
     */
    private $achievementParticleUserRelations;

    /**
     * @var AchievementType
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Achievement\AchievementType", inversedBy="achievementParticles")
     * @ORM\JoinColumn(name="achievement_type_id", referencedColumnName="id")
     */
    private $achievementType;

    public function __construct()
    {
        $this->achievementParticleUserRelations = new ArrayCollection();
        parent::__construct();
    }

    /**
     * @return int
     */
    public function getRequiredVolume()
    {
        return $this->requiredVolume;
    }

    /**
     * @param int $requiredVolume
     */
    public function setRequiredVolume($requiredVolume)
    {
        $this->requiredVolume = $requiredVolume;
    }

    /**
     * @return AchievementParticleUserRelation[]|ArrayCollection
     */
    public function getAchievementParticleUserRelations()
    {
        return $this->achievementParticleUserRelations;
    }

    /**
     * @param AchievementParticleUserRelation[]|ArrayCollection $achievementParticleUserRelations
     */
    public function setAchievementParticleUserRelations($achievementParticleUserRelations)
    {
        $this->achievementParticleUserRelations = $achievementParticleUserRelations;
    }

    /**
     * @return AchievementType
     */
    public function getAchievementType()
    {
        return $this->achievementType;
    }

    /**
     * @param AchievementType $achievementType
     */
    public function setAchievementType($achievementType)
    {
        $this->achievementType = $achievementType;
    }
}