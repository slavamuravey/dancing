<?php

namespace AppBundle\Entity\Battle\Jury;

use AppBundle\Entity\Battle\Spirits;
use AppBundle\Entity\Battle\Stage;
use AppBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="battle_jury_member_user_relations")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="member", type="string")
 * @ORM\DiscriminatorMap({"fidel" = "FidelUserRelation", "senior_pruzhinin" = "SeniorPruzhininUserRelation"})
 * @ORM\EntityListeners({"AppBundle\Service\Listener\SpiritsSetterListener"})
 */
abstract class MemberUserRelation
{
    const DEFAULT_MIN_DISPERSION_VALUE = 80;
    const DEFAULT_MAX_DISPERSION_VALUE = 120;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    protected $user;

    /**
     * @var Stage
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Battle\Stage")
     * @ORM\JoinColumn(name="stage_id", referencedColumnName="id", nullable=false)
     */
    protected $stage;

    /**
     * @var Spirits
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Battle\Spirits")
     * @ORM\JoinColumn(name="spirits_id", referencedColumnName="id", nullable=false)
     */
    protected $spirits;

    /** @var int */
    protected $minDispersionValue = self::DEFAULT_MIN_DISPERSION_VALUE;

    /** @var int */
    protected $maxDispersionValue = self::DEFAULT_MAX_DISPERSION_VALUE;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return Stage
     */
    public function getStage()
    {
        return $this->stage;
    }

    /**
     * @param Stage $stage
     */
    public function setStage($stage)
    {
        $this->stage = $stage;
    }

    /**
     * @return Spirits
     */
    public function getSpirits()
    {
        return $this->spirits;
    }

    /**
     * @param Spirits $spirits
     */
    public function setSpirits($spirits)
    {
        $this->spirits = $spirits;
    }

    /**
     * @return int
     */
    public function getMinDispersionValue()
    {
        return $this->minDispersionValue;
    }

    /**
     * @param int $minDispersionValue
     */
    public function setMinDispersionValue($minDispersionValue)
    {
        $this->minDispersionValue = $minDispersionValue;
    }

    /**
     * @return int
     */
    public function getMaxDispersionValue()
    {
        return $this->maxDispersionValue;
    }

    /**
     * @param int $maxDispersionValue
     */
    public function setMaxDispersionValue($maxDispersionValue)
    {
        $this->maxDispersionValue = $maxDispersionValue;
    }

    public function getSpiritsText()
    {
        switch ($this->getSpirits()->getCode()) {
            case Spirits::CODE_BAD:
                return 'плохое';
            case Spirits::CODE_NORMAL:
                return 'нормальное';
            default:
                return 'хорошее';
        }
    }
}