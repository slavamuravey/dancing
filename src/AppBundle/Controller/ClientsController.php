<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Achievement\AchievementType;
use AppBundle\Entity\UserClientRelation;
use AppBundle\Entity\Client;
use AppBundle\Service\Event\AddClientEvent;
use AppBundle\Service\Event\TryBuyEvent;
use AppBundle\Service\Event\RewardEvent;
use AppBundle\StoreEvents;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @var Route
 * @var ParamConverter
 */
class ClientsController extends AbstractController
{
    /**
     * @Route("/", name="clients")
     */
    public function indexAction()
    {
        /** @var \AppBundle\Service\BonusSystem\Source\BonusesExtractor $bonusesExtractor */
        $bonusesExtractor = $this->get('app.bonus_system.source.bonuses_extractor');
        $bonuses = $bonusesExtractor->extractBonuses($this->getUser());

        $result = $this->get('app.bonus_system.bonuses_applier')->apply($this->getUser(), 100, 'acts');

        return $this->render('app/clients/index.html.twig', [
            'userClientRelations' => $this->getUser()->getUserClientRelations(),
        ]);
    }

    /**
     * @Route("/clients/add/{client}", name="clients_add")
     * @ParamConverter("client", class="AppBundle:Client")
     *
     * @param Client $client
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addAction(Client $client)
    {
        $user = $this->getUser();
        $event = new TryBuyEvent($user);
        $event->setCost($client->getCost());
        $dispatcher = $this->getEventDispatcher();
        $dispatcher->addListener(StoreEvents::USER_DEBIT, function () use ($user, $client, $dispatcher) {
            $userClientRelation = new UserClientRelation();
            $userClientRelation->setUser($user);
            $userClientRelation->setClient($client);
            $this->getEntityManager()->persist($userClientRelation);
            $this->getEntityManager()->flush($userClientRelation);

            if ($client->getCode() == 'factory') {
                $addClientEvent = new AddClientEvent($user);
                $addClientEvent->setIncreaseOn(1);
                $addClientEvent->setAchievementParticleCode(AchievementType::CLIENT_FACTORY_COUNT_1);
                $dispatcher->dispatch(StoreEvents::USER_ADD_CLIENT_FACTORY, $addClientEvent);
            } else {
                $addClientEvent = new AddClientEvent($user);
                $addClientEvent->setIncreaseOn(1);
                $addClientEvent->setAchievementParticleCode(AchievementType::CLIENT_COMMON_COUNT_1);
                $dispatcher->dispatch(StoreEvents::USER_ADD_CLIENT, $addClientEvent);
            }
        });

        $dispatcher->dispatch(StoreEvents::USER_TRY_BUY, $event);

        return $this->redirectToRoute('clients');
    }

    /**
     * @Route("/clients/select", name="clients_select")
     */
    public function selectAction()
    {
        $clients = $this->getRepository(Client::class)->findAll();

        return $this->render('app/clients/select.html.twig', [
            'clients' => $clients,
        ]);
    }

    /**
     * @Route("/clients/get-reward/{userClientRelation}", name="clients_get_reward")
     * @ParamConverter("userClientRelation", class="AppBundle:UserClientRelation")
     *
     * @param UserClientRelation $userClientRelation
     * @throws AccessDeniedHttpException
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getRewardAction(UserClientRelation $userClientRelation)
    {
        if ($userClientRelation->getUser()->getId() !== $this->getUser()->getId()
            || !$userClientRelation->isTimeAwards()
        ) {
            throw new AccessDeniedHttpException();
        }

        $dispatcher = $this->getEventDispatcher();

        $reward = $userClientRelation->getClient()->getReward();
        $event = new RewardEvent($this->getUser(), $reward);
        $event->setReward($reward);

        $dispatcher->dispatch(StoreEvents::USER_REWARD, $event);

        $userClientRelation->setRewardTime(null);

        $this->getEntityManager()->flush($userClientRelation);

        return $this->redirectToRoute('clients');
    }

    /**
     * @Route("/clients/come-out/{userClientRelation}", name="clients_come_out")
     * @ParamConverter("userClientRelation", class="AppBundle:UserClientRelation")
     *
     * @param UserClientRelation $userClientRelation
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function comeOutAction(UserClientRelation $userClientRelation)
    {
        if ($userClientRelation->getUser()->getId() !== $this->getUser()->getId()) {
            throw new AccessDeniedHttpException();
        }

        $rewardPeriodSeconds = $userClientRelation->getClient()->getRewardPeriodSeconds();

        $userClientRelation->setRewardTime((new \DateTime())->modify("+$rewardPeriodSeconds second"));
        $this->getEntityManager()->flush($userClientRelation);

        return $this->redirectToRoute('clients');
    }
}
