<?php

namespace AppBundle\Form\Model;

use AppBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

class Registration
{
    /**
     * @var User
     * @Assert\Type(type="AppBundle\Entity\User")
     * @Assert\Valid()
     */
    protected $user;

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
}