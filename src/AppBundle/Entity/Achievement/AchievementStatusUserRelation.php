<?php

namespace AppBundle\Entity\Achievement;

use AppBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="achievement_status_user_relations", uniqueConstraints={@ORM\UniqueConstraint(name="uniq_user_id_achievement_status_id", columns={"user_id", "achievement_status_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Achievement\AchievementStatusUserRelationRepository")
 */
class AchievementStatusUserRelation extends AchievementTypeUserRelation
{
    /**
     * @var AchievementStatus
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Achievement\AchievementStatus", inversedBy="achievementStatusUserRelations")
     * @ORM\JoinColumn(name="achievement_status_id", referencedColumnName="id", nullable=false)
     */
    private $achievementStatus;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="achievementStatusUserRelations")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @return AchievementStatus
     */
    public function getAchievementStatus()
    {
        return $this->achievementStatus;
    }

    /**
     * @param AchievementStatus $achievementStatus
     */
    public function setAchievementStatus($achievementStatus)
    {
        $this->achievementStatus = $achievementStatus;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }
}
