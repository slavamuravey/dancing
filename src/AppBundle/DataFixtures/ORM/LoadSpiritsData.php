<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Battle\Spirits;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadSpiritsData implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $spirits = new Spirits();
        $spirits->setCode(Spirits::CODE_BAD);
        $spirits->setValue(Spirits::VALUE_BAD);

        $manager->persist($spirits);

        $spirits = new Spirits();
        $spirits->setCode(Spirits::CODE_NORMAL);
        $spirits->setValue(Spirits::VALUE_NORMAL);

        $manager->persist($spirits);

        $spirits = new Spirits();
        $spirits->setCode(Spirits::CODE_GOOD);
        $spirits->setValue(Spirits::VALUE_GOOD);

        $manager->persist($spirits);

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 7;
    }
}