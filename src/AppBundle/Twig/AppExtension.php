<?php

namespace AppBundle\Twig;

class AppExtension extends \Twig_Extension
{
    public static $units = [
        'y' => 'г',
        'm' => 'мес',
        'd' => 'д',
        'h' => 'ч',
        'i' => 'мин',
        's' => 'с',
    ];

    /**
     * Returns a list of filters.
     *
     * @return array
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('time_diff', [$this, 'diff'], [
                'needs_environment' => true,
            ]),
        ];
    }

    /**
     * Name of this extension.
     *
     * @return string
     */
    public function getName()
    {
        return 'date';
    }

    /**
     * Filter for converting dates to a time ago string like Facebook and Twitter has.
     *
     * @param \Twig_Environment $env  A Twig_Environment instance.
     * @param string|\DateTime  $date A string or DateTime object to convert.
     * @param string|\DateTime  $now  A string or DateTime object to compare with. If none given, the current time will be used.
     *
     * @return string The converted time.
     */
    public function diff(\Twig_Environment $env, $date, $now = null)
    {
        // Convert both dates to DateTime instances.
        $date = twig_date_converter($env, $date);
        $now = twig_date_converter($env, $now);

        // Get the difference between the two DateTime objects.
        $diff = $date->diff($now);

        // Check for each interval if it appears in the $diff object.
        foreach (self::$units as $attribute => $unit) {
            $count = $diff->$attribute;

            if (0 !== $count) {
                return $diff->invert ? "$count $unit" : "-$count $unit";
            }
        }

        return '';
    }
}
