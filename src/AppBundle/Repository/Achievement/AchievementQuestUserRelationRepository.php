<?php

namespace AppBundle\Repository\Achievement;

use AppBundle\Entity\Achievement\AchievementQuest;
use AppBundle\Entity\Achievement\AchievementQuestUserRelation;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class AchievementQuestUserRelationRepository extends EntityRepository
{
    /**
     * @param User $user
     * @param AchievementQuest $achievementQuest
     * @return AchievementQuestUserRelation
     */
    public function findOrCreateOneByUserAchievementQuest(User $user, AchievementQuest $achievementQuest)
    {
        /** @var AchievementQuestUserRelation $achievementQuestUserRelation */
        if (!($achievementQuestUserRelation = $this->findOneBy(['user' => $user, 'achievementQuest' => $achievementQuest]))) {
            $achievementQuestUserRelation = new AchievementQuestUserRelation();
            $achievementQuestUserRelation->setUser($user);
            $achievementQuestUserRelation->setAchievementQuest($achievementQuest);

            $this->getEntityManager()->persist($achievementQuestUserRelation);
            $this->getEntityManager()->flush();
        }

        return $achievementQuestUserRelation;
    }

    /**
     * @param User $user
     * @param AchievementQuest $achievementQuest
     * @throws \Exception
     */
    public function subscribeUserToQuest(User $user, AchievementQuest $achievementQuest)
    {
        $em = $this->getEntityManager();
        $connection = $em->getConnection();

        $sql = <<<SQL
UPDATE achievement_quest_user_relations
SET current = false, updated_at = current_timestamp
WHERE user_id = :user_id
SQL;
        try {
            $em->beginTransaction();
            $connection->executeUpdate($sql, ['user_id' => $user->getId()]);
            $relation = $this->findOrCreateOneByUserAchievementQuest($user, $achievementQuest);
            $relation->setCurrent(true);
            $em->flush();
            $em->commit();
        } catch (\Exception $e) {
            $em->rollback();
            throw $e;
        }
    }
}
