<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Achievement\AchievementParticleUserRelation;
use AppBundle\Entity\Achievement\AchievementQuestUserRelation;
use AppBundle\Entity\Achievement\AchievementStatusUserRelation;
use AppBundle\Entity\Autograph\CrowdPersonProducerUserRelation;
use AppBundle\Entity\BonusSystem\UserBonusRelation;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @UniqueEntity(fields="username", message="Пользователь с таким логином уже существует")
 */
class User implements AdvancedUserInterface, \Serializable
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="username", type="string", length=25, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(name="password", type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Role", inversedBy="users", cascade={"persist"})
     * @ORM\JoinTable(name="user_role_relations",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", unique=true)},
     *      inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
     * )
     */
    private $roles;

    /**
     * @var ArrayCollection|UserClientRelation[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\UserClientRelation", mappedBy="user")
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $userClientRelations;

    /**
     * @var ArrayCollection|UserEvent[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\UserEvent", mappedBy="user")
     */
    private $events;

    /**
     * @var ArrayCollection|AchievementQuestUserRelation[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Achievement\AchievementQuestUserRelation", mappedBy="user")
     */
    private $achievementQuestUserRelations;

    /**
     * @var ArrayCollection|AchievementStatusUserRelation[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Achievement\AchievementStatusUserRelation", mappedBy="user")
     */
    private $achievementStatusUserRelations;

    /**
     * @var ArrayCollection|AchievementParticleUserRelation[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Achievement\AchievementParticleUserRelation", mappedBy="user")
     */
    private $achievementParticleUserRelations;

    /**
     * @var UserProfile
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\UserProfile", mappedBy="user", cascade={"persist"})
     */
    private $profile;

    /**
     * @var ArrayCollection|CrowdPersonProducerUserRelation[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Autograph\CrowdPersonProducerUserRelation", mappedBy="user")
     */
    private $crowdPersonProducerUserRelations;

    public function __construct()
    {
        $this->active = true;
        $this->roles = new ArrayCollection();
        $this->userClientRelations = new ArrayCollection();
        $this->events = new ArrayCollection();
        $this->achievementQuestUserRelations = new ArrayCollection();
        $this->achievementStatusUserRelations = new ArrayCollection();
        $this->achievementParticleUserRelations = new ArrayCollection();
        $this->crowdPersonProducerUserRelations = new ArrayCollection();
        // may not be needed, see section on salt below
        // $this->salt = md5(uniqid(null, true));
    }

    /**
     * @inheritDoc
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    /**
     * @inheritDoc
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @inheritDoc
     */
    public function getRoles()
    {
        return $this->roles->toArray();
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = trim(mb_strtolower($username, 'UTF-8'));

        return $this;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return User
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return $this->active;
    }

    /**
     * Add roles
     *
     * @param Role $roles
     * @return User
     */
    public function addRole(Role $roles)
    {
        $this->roles[] = $roles;

        return $this;
    }

    /**
     * Remove roles
     *
     * @param Role $roles
     */
    public function removeRole(Role $roles)
    {
        $this->roles->removeElement($roles);
    }

    /**
     * @return UserClientRelation[]|ArrayCollection
     */
    public function getUserClientRelations()
    {
        return $this->userClientRelations;
    }

    /**
     * @param UserClientRelation[]|ArrayCollection $userClientRelations
     */
    public function setUserClientRelations($userClientRelations)
    {
        $this->userClientRelations = $userClientRelations;
    }

    /**
     * @return UserEvent[]|ArrayCollection
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * @param UserEvent[]|ArrayCollection $events
     */
    public function setEvents($events)
    {
        $this->events = $events;
    }

    /**
     * @return Achievement\AchievementQuestUserRelation[]|ArrayCollection
     */
    public function getAchievementQuestUserRelations()
    {
        return $this->achievementQuestUserRelations;
    }

    /**
     * @param Achievement\AchievementQuestUserRelation[]|ArrayCollection $achievementQuestUserRelations
     */
    public function setAchievementQuestUserRelations($achievementQuestUserRelations)
    {
        $this->achievementQuestUserRelations = $achievementQuestUserRelations;
    }

    /**
     * @return Achievement\AchievementStatusUserRelation[]|ArrayCollection
     */
    public function getAchievementStatusUserRelations()
    {
        return $this->achievementStatusUserRelations;
    }

    /**
     * @param Achievement\AchievementStatusUserRelation[]|ArrayCollection $achievementStatusUserRelations
     */
    public function setAchievementStatusUserRelations($achievementStatusUserRelations)
    {
        $this->achievementStatusUserRelations = $achievementStatusUserRelations;
    }

    /**
     * @return Achievement\AchievementParticleUserRelation[]|ArrayCollection
     */
    public function getAchievementParticleUserRelations()
    {
        return $this->achievementParticleUserRelations;
    }

    /**
     * @param Achievement\AchievementParticleUserRelation[]|ArrayCollection $achievementParticleUserRelations
     */
    public function setAchievementParticleUserRelations($achievementParticleUserRelations)
    {
        $this->achievementParticleUserRelations = $achievementParticleUserRelations;
    }

    /**
     * @return UserProfile
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @param UserProfile $profile
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;
    }

    /**
     * @return ArrayCollection|CrowdPersonProducerUserRelation[]
     */
    public function getCrowdPersonProducerUserRelations()
    {
        return $this->crowdPersonProducerUserRelations;
    }

    /**
     * @param ArrayCollection|CrowdPersonProducerUserRelation[] $crowdPersonProducerUserRelations
     */
    public function setCrowdPersonProducerUserRelations($crowdPersonProducerUserRelations)
    {
        $this->crowdPersonProducerUserRelations = $crowdPersonProducerUserRelations;
    }
}
