<?php

namespace AppBundle\Entity\Achievement;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="achievement_statuses")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Achievement\AchievementStatusRepository")
 */
class AchievementStatus extends AchievementType
{
    /**
     * @var array
     *
     * @ORM\Column(name="buffs", type="json_array", options={"default": "[]"})
     */
    private $buffs = [];

    /**
     * @var ArrayCollection|AchievementStatusUserRelation[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Achievement\AchievementStatusUserRelation", mappedBy="achievementStatus")
     */
    private $achievementStatusUserRelations;

    public function __construct()
    {
        $this->achievementStatusUserRelations = new ArrayCollection();
        parent::__construct();
    }

    /**
     * @return array
     */
    public function getBuffs()
    {
        return $this->buffs;
    }

    /**
     * @param array $buffs
     */
    public function setBuffs($buffs)
    {
        $this->buffs = $buffs;
    }

    /**
     * @return AchievementStatusUserRelation[]|ArrayCollection
     */
    public function getAchievementStatusUserRelations()
    {
        return $this->achievementStatusUserRelations;
    }

    /**
     * @param AchievementStatusUserRelation[]|ArrayCollection $achievementStatusUserRelations
     */
    public function setAchievementStatusUserRelations($achievementStatusUserRelations)
    {
        $this->achievementStatusUserRelations = $achievementStatusUserRelations;
    }
}
