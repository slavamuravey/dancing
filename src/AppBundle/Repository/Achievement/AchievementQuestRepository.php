<?php

namespace AppBundle\Repository\Achievement;

use AppBundle\Entity\Achievement\AchievementQuest;
use AppBundle\Entity\Achievement\AchievementQuestUserRelation;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class AchievementQuestRepository extends EntityRepository
{
    public function getQuestsWithRelationsForUser(User $user)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('q.id', 'q.code', 'q.reward', 'r.id rel_id', 'r.current', 'r.done')
            ->from(AchievementQuest::class, 'q')
            ->leftJoin(AchievementQuestUserRelation::class, 'r', 'WITH', 'q.id = r.achievementQuest AND r.user = :user')
            ->setParameter('user', $user);

        $q = $qb->getQuery();

        return $q->getResult();
    }
}
