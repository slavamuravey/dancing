<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="clients")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClientRepository")
 */
class Client
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="reward_period_seconds", type="integer")
     */
    private $rewardPeriodSeconds = 0;

    /**
     * @var array
     *
     * @ORM\Column(name="reward", type="json_array", options={"default": "[]"})
     */
    private $reward = [];

    /**
     * @var ArrayCollection|UserClientRelation[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\UserClientRelation", mappedBy="client")
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $userClientRelations;

    /**
     * @var array
     *
     * @ORM\Column(name="cost", type="json_array", options={"default": "[]"})
     */
    private $cost = [];

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", unique=true)
     */
    private $code;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getRewardPeriodSeconds()
    {
        return $this->rewardPeriodSeconds;
    }

    /**
     * @param int $rewardPeriodSeconds
     */
    public function setRewardPeriodSeconds($rewardPeriodSeconds)
    {
        $this->rewardPeriodSeconds = $rewardPeriodSeconds;
    }

    /**
     * @return array
     */
    public function getReward()
    {
        return $this->reward;
    }

    /**
     * @param array $reward
     */
    public function setReward($reward)
    {
        $this->reward = $reward;
    }

    /**
     * @return UserClientRelation[]|ArrayCollection
     */
    public function getUserClientRelations()
    {
        return $this->userClientRelations;
    }

    /**
     * @param UserClientRelation[]|ArrayCollection $userClientRelations
     */
    public function setUserClientRelations($userClientRelations)
    {
        $this->userClientRelations = $userClientRelations;
    }

    /**
     * @return array
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @param array $cost
     */
    public function setCost($cost)
    {
        $this->cost = $cost;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }
}
