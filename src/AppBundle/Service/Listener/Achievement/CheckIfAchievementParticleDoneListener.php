<?php

namespace AppBundle\Service\Listener\Achievement;

use AppBundle\Entity\Achievement\AchievementParticleUserRelation;
use AppBundle\Service\Event\Achievement\AchievementParticleDoneEvent;
use AppBundle\StoreEvents;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\EventDispatcher\ContainerAwareEventDispatcher;

class CheckIfAchievementParticleDoneListener
{
    /** @var ContainerAwareEventDispatcher */
    private $dispatcher;

    /**
     * @param ContainerAwareEventDispatcher $dispatcher
     */
    public function __construct($dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param PreUpdateEventArgs $eventArgs
     */
    public function preUpdate(PreUpdateEventArgs $eventArgs)
    {
        $entity = $eventArgs->getObject();

        if (!($entity instanceof AchievementParticleUserRelation)) {
            return;
        }

        if (!$eventArgs->hasChangedField('currentValue')) {
            return;
        }

        if (!$entity->isDone() && $entity->getCurrentValue() >= $entity->getAchievementParticle()->getRequiredVolume()) {
            $entity->setDone(true);
            $event = new AchievementParticleDoneEvent($entity->getUser(), [
                'code' => $entity->getAchievementParticle()->getCode(),
            ]);
            $event->setAchievementParticleUserRelation($entity);
            $em = $eventArgs->getEntityManager();
            $em->getUnitOfWork()->recomputeSingleEntityChangeSet($em->getClassMetadata(get_class($entity)), $entity);
            $this->dispatcher->dispatch(StoreEvents::ACHIEVEMENT_PARTICLE_DONE, $event);
        }
    }
}