<?php

namespace AppBundle\Entity\Autograph;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="autograph_crowd_persons")
 * @ORM\Entity()
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({
 *     "fan" = "CrowdPersonFan",
 *     "hater" = "CrowdPersonHater",
 *     "producer" = "CrowdPersonProducer"
 * })
 */
abstract class CrowdPerson
{
    const HATER_TYPE = 'hater';
    const FAN_TYPE = 'fan';
    const PRODUCER_TYPE = 'producer';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", unique=true)
     */
    private $code;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    abstract public function getType();
}