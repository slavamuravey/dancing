<?php

namespace AppBundle\Service\Event;

use AppBundle\Service\Event\Achievement\AchievementParticleDataAwareInterface;
use AppBundle\Service\Event\Achievement\AchievementParticleDataAwareTrait;

class UserVisitProfileEvent extends AbstractEvent implements AchievementParticleDataAwareInterface
{
    use AchievementParticleDataAwareTrait;
}