<?php

namespace AppBundle\Service\Listener\Achievement;

use AppBundle\Service\Event\Achievement\AchievementStatusDoneEvent;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class AchievementStatusDoneNotificationListener
{
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @param SessionInterface $session
     */
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function onNotify(AchievementStatusDoneEvent $event)
    {
        $this->session->getFlashBag()->add('achievement_status_done_notice', 'Статус ' . $event->getAchievementStatusUserRelation()->getAchievementStatus()->getCode() . ' выполнен');
    }
}