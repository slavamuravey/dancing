<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @var Route
 */
class TutorialController extends AbstractController
{
    /**
     * @Route("/tutorial/step{step}", name="tutorial")
     *
     * @param $step
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction($step)
    {
        return $this->redirectToRoute('clients');
        return $this->render('app/tutorial/step{$step}.html.twig');
    }
}