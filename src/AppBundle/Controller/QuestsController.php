<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Achievement\AchievementParticle;
use AppBundle\Entity\Achievement\AchievementQuest;
use AppBundle\Entity\Achievement\AchievementQuestUserRelation;
use AppBundle\Entity\UserProfile;
use AppBundle\Repository\Achievement\AchievementParticleRepository;
use AppBundle\Repository\Achievement\AchievementQuestRepository;
use AppBundle\Repository\Achievement\AchievementQuestUserRelationRepository;
use AppBundle\Service\Event\RewardEvent;
use AppBundle\StoreEvents;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @var Route
 * @var ParamConverter
 */
class QuestsController extends AbstractController
{
    /**
     * @Route("/quests/{profile}", name="quests")
     * @ParamConverter("profile", class="AppBundle:UserProfile")
     *
     * @param UserProfile $profile
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(UserProfile $profile = null)
    {
        if (null === $profile) {
            $profile = $this->getUser()->getProfile();
        }

        /** @var AchievementParticleRepository $achievementParticleRepository */
        $achievementParticleRepository = $this->getRepository(AchievementParticle::class);

        /** @var AchievementQuestRepository $achievementQuestRepository */
        $achievementQuestRepository = $this->getRepository(AchievementQuest::class);

        /** @var AchievementQuest[] $achievementQuests */
        $achievementQuests = $achievementQuestRepository->findAll();

        /** @var AchievementQuestUserRelationRepository $achievementQuestUserRelationRepository */
        $achievementQuestUserRelationRepository = $this->getRepository(AchievementQuestUserRelation::class);

        $achievementsData = [];

        foreach ($achievementQuests as $achievementQuest) {
            $user = $profile->getUser();
            $achievementsData[$achievementQuest->getCode()] = [
                'achievementParticleData' => $achievementParticleRepository->getAchievementParticleRelationsForAchievementType($user, $achievementQuest),
                'achievementQuestUserRelation' => $achievementQuestUserRelationRepository->findOrCreateOneByUserAchievementQuest($user, $achievementQuest),
            ];
        }

        return $this->render('app/quests/index.html.twig', [
            'achievementsData' => $achievementsData,
        ]);
    }

    /**
     * @Route("/quests/get-reward/{achievementQuestUserRelation}", name="quests_get_reward")
     * @ParamConverter("achievementQuestUserRelation", class="AppBundle:UserQuestRelation")
     *
     * @param UserQuestRelation $userQuestRelation
     * @throws AccessDeniedHttpException
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getRewardAction(UserQuestRelation $userQuestRelation)
    {
        if ($userQuestRelation->getUser()->getId() !== $this->getUser()->getId()
            || !$userQuestRelation->isDone()
        ) {
            throw new AccessDeniedHttpException();
        }

        $dispatcher = $this->getEventDispatcher();

        $reward = $userQuestRelation->getQuest()->getReward();
        $event = new RewardEvent($this->getUser(), $reward);
        $event->setReward($reward);

        $dispatcher->dispatch(StoreEvents::USER_REWARD, $event);

        return $this->redirectToRoute('quests');
    }

    /**
     * @Route("/quests/subscribe/{achievementQuest}", name="quests_subscribe")
     * @ParamConverter("achievementQuest", class="AppBundle:Achievement\AchievementQuest")
     *
     * @param AchievementQuest $achievementQuest
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function subscribeAction(AchievementQuest $achievementQuest)
    {
        /** @var AchievementQuestUserRelationRepository $achievementQuestUserRelationRepository */
        $achievementQuestUserRelationRepository = $this->getRepository(AchievementQuestUserRelation::class);
        $achievementQuestUserRelationRepository->subscribeUserToQuest($this->getUser(), $achievementQuest);

        return $this->redirectToRoute('quests');
    }
}
