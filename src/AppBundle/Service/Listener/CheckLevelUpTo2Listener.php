<?php

namespace AppBundle\Service\Listener;

use AppBundle\Entity\Achievement\AchievementType;
use AppBundle\Service\Event\LevelUpEvent;
use AppBundle\Service\Event\UserLevelUpTo2Event;
use AppBundle\StoreEvents;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CheckLevelUpTo2Listener
{
    /**
     * @param LevelUpEvent $event
     * @param string $eventName
     * @param EventDispatcherInterface $dispatcher
     */
    public function onLevelUpTo2(LevelUpEvent $event, $eventName, EventDispatcherInterface $dispatcher)
    {
        if ($event->getToLevel()->getLevel() == 2) {
            $event = new UserLevelUpTo2Event($event->getUser());
            $event->setIncreaseOn(1);
            $event->setAchievementParticleCode(AchievementType::LEVEL_UP_1);
            $dispatcher->dispatch(StoreEvents::USER_LEVEL_UP_TO_2, $event);
        }
    }
}