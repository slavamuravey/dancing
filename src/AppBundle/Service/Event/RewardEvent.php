<?php

namespace AppBundle\Service\Event;

use AppBundle\Service\Event\Achievement\AchievementParticleDataAwareInterface;
use AppBundle\Service\Event\Achievement\AchievementParticleDataAwareTrait;

class RewardEvent extends AbstractEvent implements AchievementParticleDataAwareInterface
{
    use AchievementParticleDataAwareTrait;

    /** @var array */
    private $reward;

    /**
     * @return array
     */
    public function getReward()
    {
        return $this->reward;
    }

    /**
     * @param array $reward
     */
    public function setReward(array $reward)
    {
        $this->reward = $reward;
    }
}