<?php

namespace AppBundle\Service\Listener;

use AppBundle\Entity\UserProfile;
use AppBundle\Repository\UserLevelRepository;
use AppBundle\Service\Event\LevelUpEvent;
use AppBundle\StoreEvents;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\EventDispatcher\ContainerAwareEventDispatcher;

class CheckIfUserLevelUpListener
{
    /** @var ContainerAwareEventDispatcher */
    private $dispatcher;

    /**
     * @param ContainerAwareEventDispatcher $dispatcher
     */
    public function __construct($dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param PreUpdateEventArgs $event
     */
    public function preUpdate(PreUpdateEventArgs $event)
    {
        $entity = $event->getObject();

        if (!($entity instanceof UserProfile)) {
            return;
        }

        if (!$event->hasChangedField('exp')) {
            return;
        }

        if ($entity->getNextLevel()
            && $entity->getExp() >= $entity->getNextLevel()->getExpNeeded()
        ) {
            /** @var UserLevelRepository $userLevelRepository */
            $levelUpEvent = new LevelUpEvent($entity->getUser(), [
                'from_level' => $entity->getLevel()->getLevel(),
                'to_level' => $entity->getNextLevel()->getLevel(),
            ]);
            $levelUpEvent->setFromLevel($entity->getLevel());
            $levelUpEvent->setToLevel($entity->getNextLevel());
            $em = $event->getEntityManager();
            $entity->setLevel($entity->getNextLevel());
            $em->getUnitOfWork()->recomputeSingleEntityChangeSet($em->getClassMetadata(get_class($entity)), $entity);
            $em->flush($entity);

            $this->dispatcher->dispatch(StoreEvents::USER_LEVEL_UP, $levelUpEvent);
        }
    }
}