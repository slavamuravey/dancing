<?php

namespace AppBundle\Service\BonusSystem;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BonusHydrator
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param array $params
     * @param string $class
     * @return object
     */
    public function hydrate(array $params, string $class)
    {
        $object = new $class;

        foreach ($params as $key => $value) {
            if ('effects' == $key) {
                $effects = [];
                foreach ($value as $internalKey => $internalValue) {
                    /** @var Effect $effect */
                    $effect = $this->hydrate($internalValue, Effect::class);
                    $effect->setBonus($object);
                    $effects[] = $effect;
                }
                $value = new ArrayCollection($effects);
            }
            if ('modifier' == $key) {
                $value = $this->container->get($value);
            }
            $object->{'set' . $key}($value);
        }

        return $object;
    }
}
