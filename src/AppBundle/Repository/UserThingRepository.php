<?php

namespace AppBundle\Repository;

use AppBundle\Entity\UserProfile;
use AppBundle\Entity\UserProfileUserThingRelation;
use AppBundle\Entity\UserThing;
use AppBundle\Entity\UserThingCategory;
use Doctrine\ORM\EntityRepository;

class UserThingRepository extends EntityRepository
{
    public function getThingsWithRelationsForUserProfile(UserProfile $profile, UserThingCategory $category = null)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('t.id', 't.code', 'r.id rel_id', 'r.wearing')
            ->from(UserThing::class, 't')
            ->leftJoin(UserProfileUserThingRelation::class, 'r', 'WITH', 't.id = r.thing AND r.profile = :profile')
            ->setParameter('profile', $profile);

        if ($category) {
            $qb->andWhere($qb->expr()->eq('t.category', $category->getId()));
        }

        $q = $qb->getQuery();

        return $q->getResult();
    }
}
