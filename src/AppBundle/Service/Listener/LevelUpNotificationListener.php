<?php

namespace AppBundle\Service\Listener;

use AppBundle\Service\Event\LevelUpEvent;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class LevelUpNotificationListener
{
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @param SessionInterface $session
     */
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @param LevelUpEvent $event
     */
    public function onNotify(LevelUpEvent $event)
    {
        $this->session->getFlashBag()->add('level_up', 'Вы прокачались до ' . $event->getToLevel()->getLevel() . ' уровня!');
    }
}