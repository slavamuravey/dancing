<?php

namespace AppBundle\Service\Listener\Achievement;

use AppBundle\Entity\Achievement\AchievementParticle;
use AppBundle\Entity\Achievement\AchievementStatus;
use AppBundle\Entity\Achievement\AchievementStatusUserRelation;
use AppBundle\Repository\Achievement\AchievementParticleRepository;
use AppBundle\Repository\Achievement\AchievementStatusUserRelationRepository;
use AppBundle\Service\Event\Achievement\AchievementParticleDoneEvent;
use AppBundle\Service\Event\Achievement\AchievementStatusDoneEvent;
use AppBundle\StoreEvents;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\ContainerAwareEventDispatcher;

class CheckIfAchievementStatusDoneListener
{
    /** @var EntityManager */
    private $em;

    /** @var ContainerAwareEventDispatcher */
    private $dispatcher;

    /**
     * @param EntityManager $em
     * @param ContainerAwareEventDispatcher $dispatcher
     */
    public function __construct(EntityManager $em, $dispatcher)
    {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param AchievementParticleDoneEvent $event
     */
    public function onCheck(AchievementParticleDoneEvent $event)
    {
        $achievementParticleUserRelation = $event->getAchievementParticleUserRelation();
        $achievementParticle = $achievementParticleUserRelation->getAchievementParticle();
        $achievementType = $achievementParticle->getAchievementType();
        if (!(is_object($achievementType) && $achievementType instanceof AchievementStatus)) {
            return;
        }

        /** @var AchievementStatusUserRelationRepository $achievementStatusUserRelationRepository */
        $achievementStatusUserRelationRepository = $this->em->getRepository(AchievementStatusUserRelation::class);

        $user = $event->getUser();
        $achievementStatusUserRelation = $achievementStatusUserRelationRepository->findOrCreateOneByUserAchievementStatus($user, $achievementType);

        if ($achievementStatusUserRelation->isDone()) {
            return;
        }

        /** @var AchievementParticleRepository $achievementParticleRepository */
        $achievementParticleRepository = $this->em->getRepository(AchievementParticle::class);
        $achievementParticleRelations = $achievementParticleRepository->getAchievementParticleRelationsForAchievementType($user, $achievementType);

        foreach ($achievementParticleRelations as $achievementParticleRelation) {
            if ($achievementParticleRelation['rel_id'] != $achievementParticleUserRelation->getId()
                && !$achievementParticleRelation['done']) {
                return;
            }
        }

        $achievementStatusUserRelation->setDone(true);
        $event = new AchievementStatusDoneEvent($user, [
            'code' => $achievementType->getCode(),
        ]);
        $event->setAchievementStatusUserRelation($achievementStatusUserRelation);
        $this->em->flush($achievementStatusUserRelation);
        $this->dispatcher->dispatch(StoreEvents::ACHIEVEMENT_STATUS_DONE, $event);
    }
}