<?php

namespace AppBundle\Repository\Battle;

use AppBundle\Entity\Battle\Jury\MemberUserRelation;
use AppBundle\Entity\Battle\Spirits;
use Doctrine\ORM\EntityRepository;

class SpiritsRepository extends EntityRepository
{
    /**
     * @param MemberUserRelation $relation
     */
    public function spiritsUp(MemberUserRelation $relation)
    {
        $em = $this->getEntityManager();

        switch ($relation->getSpirits()->getCode()) {
            case Spirits::CODE_BAD:
                /** @var Spirits $normalSpirits */
                $normalSpirits = $this->findOneBy(['code' => Spirits::CODE_NORMAL]);
                $relation->setSpirits($normalSpirits);
                $em->flush($relation);
                break;
            case Spirits::CODE_NORMAL:
                /** @var Spirits $goodSpirits */
                $goodSpirits = $this->findOneBy(['code' => Spirits::CODE_GOOD]);
                $relation->setSpirits($goodSpirits);
                $em->flush($relation);
                break;
        }
    }
}
