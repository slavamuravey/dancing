<?php

namespace AppBundle\Repository\Achievement;

use AppBundle\Entity\Achievement;
use AppBundle\Entity\Achievement\AchievementParticle;
use AppBundle\Entity\Achievement\AchievementParticleUserRelation;
use AppBundle\Entity\Achievement\AchievementType;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class AchievementParticleRepository extends EntityRepository
{
    /**
     * @param User $user
     * @param AchievementType $achievementType
     * @return array
     */
    public function getAchievementParticleRelationsForAchievementType(User $user, AchievementType $achievementType)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('a.id', 'a.code', 'a.requiredVolume', 'r.id rel_id', 'r.currentValue', 'r.done')
            ->from(AchievementParticle::class, 'a')
            ->leftJoin(AchievementParticleUserRelation::class, 'r', 'WITH', 'a.id = r.achievementParticle AND r.user = :user')
            ->where('a.achievementType = :type')
            ->setParameter('type', $achievementType)
            ->setParameter('user', $user);

        $q = $qb->getQuery();

        return $q->getResult();
    }
}
