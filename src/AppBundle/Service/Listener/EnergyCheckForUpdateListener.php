<?php

namespace AppBundle\Service\Listener;

use AppBundle\Entity\UserProfile;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;

class EnergyCheckForUpdateListener
{
    /**
     * @ORM\PostLoad
     *
     * @param UserProfile $profile
     * @param LifecycleEventArgs $event
     */
    public function updateEnergy(UserProfile $profile, $event)
    {
        $energy = $profile->getEnergy();
        $energyMax = $profile->getEnergyMax();
        if ($energy >= $energyMax
            || $profile->getEnergyUpdateDate() >= new \DateTime()
        ) {
            return;
        }

        $energyUpdateDate = $profile->getEnergyUpdateDate();
        $currentDate = new \DateTime();
        $interval = $currentDate->getTimestamp() - $energyUpdateDate->getTimestamp();
        $energyPointsForAdd = floor($interval / $profile->getEnergyPeriodSeconds());

        if ($energyPointsForAdd < 1) {
            return;
        }

        $em = $event->getEntityManager();
        $profile->setEnergy(min($energy + $energyPointsForAdd, $energyMax));
        $em->flush($profile);
    }
}