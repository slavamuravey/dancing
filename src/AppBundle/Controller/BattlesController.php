<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Battle\Jury\FidelUserRelation;
use AppBundle\Entity\Battle\Jury\MemberUserRelation;
use AppBundle\Entity\Battle\Jury\SeniorPruzhininUserRelation;
use AppBundle\Entity\Battle\Spirits;
use AppBundle\Entity\Battle\Stage;
use AppBundle\Entity\UserProfile;
use AppBundle\Repository\Battle\Jury\FidelUserRelationRepository;
use AppBundle\Repository\Battle\Jury\SeniorPruzhininUserRelationRepository;
use AppBundle\Repository\Battle\SpiritsRepository;
use AppBundle\Repository\Battle\StageRepository;
use AppBundle\Repository\UserProfileRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @var Route
 */
class BattlesController extends AbstractController
{
    const DANCE_ENERGY_COST = 2;

    /**
     * @Route("/battles", name="battles")
     */
    public function indexAction()
    {
        /** @var StageRepository $stageRepository */
        $stageRepository = $this->getRepository(Stage::class);
        $stages = $stageRepository->findAll();

        return $this->render('app/battles/index.html.twig', [
            'stages' => $stages
        ]);
    }

    /**
     * @Route("/battles/spirits-up/{memberUserRelation}", name="battles_spirits_up")
     * @ParamConverter("memberUserRelation", class="AppBundle:Battle\Jury\MemberUserRelation")
     *
     * @param MemberUserRelation $memberUserRelation
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function spiritsUpAction(MemberUserRelation $memberUserRelation)
    {
        if ($this->getUser()->getId() != $memberUserRelation->getUser()->getId()) {
            throw new AccessDeniedHttpException();
        }

        /** @var SpiritsRepository $spiritsRepository */
        $spiritsRepository = $this->getRepository(Spirits::class);

        $spiritsRepository->spiritsUp($memberUserRelation);

        return $this->redirectToRoute('battles_stage', ['stage' => $memberUserRelation->getStage()->getId()]);
    }

    /**
     * @Route("/battles/stage/{stage}", name="battles_stage")
     * @ParamConverter("stage", class="AppBundle:Battle\Stage")
     *
     * @param Request $request
     * @param Stage $stage
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function stageAction(Request $request, Stage $stage)
    {
        /** @var FidelUserRelationRepository $fidelUserRelationRepository */
        $fidelUserRelationRepository = $this->getRepository(FidelUserRelation::class);

        /** @var SeniorPruzhininUserRelationRepository $seniorPruzhininUserRelationRepository */
        $seniorPruzhininUserRelationRepository = $this->getRepository(SeniorPruzhininUserRelation::class);

        $user = $this->getUser();

        /** @var FidelUserRelation $fidelUserRelation */
        $fidelUserRelation = $fidelUserRelationRepository->findOrCreateByUserStage($user, $stage);

        /** @var SeniorPruzhininUserRelation $seniorPruzhininUserRelation */
        $seniorPruzhininUserRelation = $seniorPruzhininUserRelationRepository->findOrCreateByUserStage($user, $stage);

        $userFidelResult = $request->query->get('userFidelResult');
        $userSeniorPruzhininResult = $request->query->get('userSeniorPruzhininResult');

        if ($request->query->get('dance') === null) {
            return $this->render('app/battles/stage.html.twig', [
                'stage' => $stage,
                'fidelUserRelation' => $fidelUserRelation,
                'seniorPruzhininUserRelation' => $seniorPruzhininUserRelation,
                'userFidelResult' => $userFidelResult,
                'userSeniorPruzhininResult' => $userSeniorPruzhininResult,
            ]);
        }

        $profile = $user->getProfile();

        if ($profile->getEnergy() >= self::DANCE_ENERGY_COST) {
            $profile->setEnergy($profile->getEnergy() - self::DANCE_ENERGY_COST);
            /** @var UserProfileRepository $profileRepository */
            $profileRepository = $this->getRepository(UserProfile::class);

            $result = $profileRepository->calculateDanceResult($profile);
            $fidelUserRelation->applyUserParamsBuff();
            $userFidelResult = $result - $fidelUserRelation->calculate();
            $seniorPruzhininUserRelation->applyUserParamsBuff();
            $userSeniorPruzhininResult = $result - $seniorPruzhininUserRelation->calculate();
            $em = $this->getEntityManager();
            $profile->setRating($profile->getRating() + $userFidelResult + $userSeniorPruzhininResult);
            $em->remove($fidelUserRelation);
            $em->remove($seniorPruzhininUserRelation);
            $em->flush();
        } else {
            $this->get('session')->getFlashBag()->add('more_energy_required', 'Не хватает энергии');
        }

        return $this->redirectToRoute('battles_stage', [
            'stage' => $stage->getId(),
            'userFidelResult' => $userFidelResult,
            'userSeniorPruzhininResult' => $userSeniorPruzhininResult,
        ]);
    }
}
