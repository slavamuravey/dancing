<?php

namespace AppBundle\Service\Event\Achievement;

trait AchievementParticleDataAwareTrait
{
    /** @var int */
    private $increaseOn;

    /** @var string */
    private $achievementParticleCode;

    /** @var AchievementParticleDataAwareInterface[] */
    private $dependencies = [];

    /**
     * @return int
     */
    public function getIncreaseOn()
    {
        return $this->increaseOn;
    }

    /**
     * @param int $increaseOn
     */
    public function setIncreaseOn($increaseOn)
    {
        $this->increaseOn = (int)$increaseOn;
    }

    /**
     * @return string
     */
    public function getAchievementParticleCode()
    {
        return $this->achievementParticleCode;
    }

    /**
     * @param string $achievementParticleCode
     */
    public function setAchievementParticleCode($achievementParticleCode)
    {
        $this->achievementParticleCode = (string)$achievementParticleCode;
    }
}