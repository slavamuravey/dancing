<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="user_levels")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserLevelRepository")
 */
class UserLevel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="level", type="integer", unique=true)
     */
    private $level;

    /**
     * @var int
     *
     * @ORM\Column(name="exp_needed", type="integer")
     */
    private $expNeeded;

    /**
     * @var UserProfile[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\UserProfile", mappedBy="level")
     */
    private $profiles;

    public function __construct()
    {
        $this->profiles = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param int $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }

    /**
     * @return int
     */
    public function getExpNeeded()
    {
        return $this->expNeeded;
    }

    /**
     * @param int $expNeeded
     */
    public function setExpNeeded($expNeeded)
    {
        $this->expNeeded = $expNeeded;
    }

    /**
     * @return UserProfile[]
     */
    public function getProfiles()
    {
        return $this->profiles;
    }

    /**
     * @param UserProfile[] $profiles
     */
    public function setProfiles($profiles)
    {
        $this->profiles = $profiles;
    }
}