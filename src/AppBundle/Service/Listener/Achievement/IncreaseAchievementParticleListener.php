<?php

namespace AppBundle\Service\Listener\Achievement;

use AppBundle\Entity\Achievement\AchievementParticle;
use AppBundle\Entity\Achievement\AchievementParticleUserRelation;
use AppBundle\Entity\Achievement\AchievementQuest;
use AppBundle\Entity\Achievement\AchievementQuestUserRelation;
use AppBundle\Repository\Achievement\AchievementParticleRepository;
use AppBundle\Repository\Achievement\AchievementParticleUserRelationRepository;
use AppBundle\Repository\Achievement\AchievementQuestUserRelationRepository;
use AppBundle\Service\Event\AbstractEvent;
use AppBundle\Service\Event\Achievement\AchievementParticleDataAwareInterface;
use Doctrine\ORM\EntityManager;

class IncreaseAchievementParticleListener
{
    /** @var EntityManager */
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function onIncrease(AchievementParticleDataAwareInterface $event)
    {
        if (!($event instanceof AbstractEvent)) {
            throw new \RuntimeException('event must be instance of ' . AbstractEvent::class);
        }

        /** @var AchievementParticleRepository $achievementParticleRepository */
        $achievementParticleRepository = $this->em->getRepository(AchievementParticle::class);

        /** @var AchievementParticle $achievementParticle */
        $achievementParticleCode = $event->getAchievementParticleCode();
        if (!($achievementParticle = $achievementParticleRepository->findOneBy(['code' => $achievementParticleCode]))) {
            throw new \RuntimeException(sprintf('achievement particle "%s" not found', $achievementParticleCode));
        }

        /** @var AchievementParticleUserRelationRepository $achievementParticleUserRelationRepository */
        $achievementParticleUserRelationRepository = $this->em->getRepository(AchievementParticleUserRelation::class);

        $user = $event->getUser();
        $achievementParticleUserRelation = $achievementParticleUserRelationRepository->findOrCreateOneByUserAchievementParticle($user, $achievementParticle);
        if ($achievementParticleUserRelation->isDone()) {
            return;
        }
        $achievementType = $achievementParticleUserRelation->getAchievementParticle()->getAchievementType();
        /** @var AchievementQuestUserRelationRepository $achievementQuestUserRelationRepository */
        $achievementQuestUserRelationRepository = $this->em->getRepository(AchievementQuestUserRelation::class);
        if (!(is_object($achievementType)
            && $achievementType instanceof AchievementQuest
            && !$achievementQuestUserRelationRepository->findOrCreateOneByUserAchievementQuest($user, $achievementType)->isCurrent())
        ) {
            $achievementParticleUserRelation->setCurrentValue($achievementParticleUserRelation->getCurrentValue() + $event->getIncreaseOn());
            $this->em->flush($achievementParticleUserRelation);
        }
    }
}