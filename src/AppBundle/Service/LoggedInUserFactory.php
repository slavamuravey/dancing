<?php

namespace AppBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class LoggedInUserFactory
{
    public static function create(ContainerInterface $container)
    {
        /** @var TokenInterface $token */
        $token = $container->get('security.token_storage')->getToken();

        return (!$token || $token instanceof AnonymousToken) ? null : $token->getUser();
    }
}