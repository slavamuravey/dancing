<?php

namespace AppBundle\Repository\Achievement;

use AppBundle\Entity\Achievement\AchievementStatus;
use AppBundle\Entity\Achievement\AchievementStatusUserRelation;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class AchievementStatusUserRelationRepository extends EntityRepository
{
    /**
     * @param User $user
     * @param AchievementStatus $achievementStatus
     * @return AchievementStatusUserRelation
     */
    public function findOrCreateOneByUserAchievementStatus(User $user, AchievementStatus $achievementStatus)
    {
        /** @var AchievementStatusUserRelation $achievementStatusUserRelation */
        if (!($achievementStatusUserRelation = $this->findOneBy(['user' => $user, 'achievementStatus' => $achievementStatus]))) {
            $achievementStatusUserRelation = new AchievementStatusUserRelation();
            $achievementStatusUserRelation->setUser($user);
            $achievementStatusUserRelation->setAchievementStatus($achievementStatus);

            $this->getEntityManager()->persist($achievementStatusUserRelation);
            $this->getEntityManager()->flush();
        }

        return $achievementStatusUserRelation;
    }
}
