<?php

namespace AppBundle\Controller;

use AppBundle\Entity\UserProfile;
use AppBundle\Entity\UserProfileUserThingRelation;
use AppBundle\Repository\UserProfileUserThingRelationRepository;
use AppBundle\Service\ThingsImagesCompositor;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * @var Route
 * @var ParamConverter
 */
class AvatarController extends AbstractController
{
    /**
     * @Route("/avatar/{profile}/{avatar}", name="avatar", defaults={"avatar" = ""})
     *
     * @ParamConverter("profile", class="AppBundle:UserProfile")
     *
     * @param UserProfile $profile
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAvatarAction(UserProfile $profile)
    {
        /** @var UserProfileUserThingRelationRepository $userProfileUserThingRelationRepository */
        $userProfileUserThingRelationRepository = $this->getRepository(UserProfileUserThingRelation::class);
        $images = $userProfileUserThingRelationRepository->getImagePathsForWearingThingsByProfile($profile->getId());

        /** @var ThingsImagesCompositor $thingsImagesCompositor */
        $thingsImagesCompositor = $this->get('app.things_images_compositor');

        $avatarPath = $thingsImagesCompositor->generate($images);

        $profile->setAvatar(basename($avatarPath));
        $this->getEntityManager()->flush();

        return new Response(file_get_contents($avatarPath), Response::HTTP_OK, ['Content-Type' => 'image/png']);
    }
}
