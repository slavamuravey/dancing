<?php

namespace AppBundle\Service\BonusSystem;

use Doctrine\Common\Collections\ArrayCollection;

class Bonus
{
    /**
     * @var int
     */
    private $expireAt;

    /**
     * @var mixed
     */
    private $power;

    /**
     * @var string
     */
    private $description;

    /**
     * @var int
     */
    private $expiration;

    /**
     * @var ArrayCollection|Effect[]
     */
    private $effects;

    public function __construct()
    {
        $this->effects = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getExpireAt()
    {
        return $this->expireAt;
    }

    /**
     * @param int $expireAt
     */
    public function setExpireAt($expireAt)
    {
        $this->expireAt = $expireAt;
    }

    /**
     * @return mixed
     */
    public function getPower()
    {
        return $this->power;
    }

    /**
     * @param mixed $power
     */
    public function setPower($power)
    {
        $this->power = $power;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getExpiration()
    {
        return $this->expiration;
    }

    /**
     * @param int $expiration
     */
    public function setExpiration($expiration)
    {
        $this->expiration = $expiration;
    }

    /**
     * @return ArrayCollection|Effect[]
     */
    public function getEffects()
    {
        return $this->effects;
    }

    /**
     * @param ArrayCollection|Effect[] $effects
     */
    public function setEffects($effects)
    {
        $this->effects = $effects;
    }
}
