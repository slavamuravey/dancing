<?php

namespace AppBundle\Entity\Achievement;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="achievement_types")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({
 *     "quest" = "AchievementQuest",
 *     "status" = "AchievementStatus",
 *     "particle" = "AchievementParticle"
 * })
 */
abstract class AchievementType
{
    const PROFILE_VISIT_1 = 'profile_visit_1';
    const CLIENT_COMMON_COUNT_1 = 'client_common_count_1';
    const CLIENT_FACTORY_COUNT_1 = 'client_factory_count_1';
    const LEVEL_UP_1 = 'level_up_1';
    const PARAM_EXP_1 = 'param_exp_1';
    const PARAM_MANI_1 = 'param_mani_1';
    const PARAM_DANCES_1 = 'param_dances_1';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", unique=true)
     */
    private $code;

    /**
     * @var ArrayCollection|AchievementParticle[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Achievement\AchievementParticle", mappedBy="achievementType")
     */
    private $achievementParticles;

    public function __construct()
    {
        $this->achievementParticles = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return AchievementParticle[]|ArrayCollection
     */
    public function getAchievementParticles()
    {
        return $this->achievementParticles;
    }

    /**
     * @param AchievementParticle[]|ArrayCollection $achievementParticles
     */
    public function setAchievementParticles($achievementParticles)
    {
        $this->achievementParticles = $achievementParticles;
    }
}