<?php

namespace AppBundle\Service\BonusSystem;

class DefaultEffectsMixer implements EffectsMixerInterface
{
    /**
     * @param mixed $baseValue
     * @param array|Effect[] $effects
     * @return mixed
     */
    public function mix($baseValue, array $effects)
    {
        usort($effects, function ($a, $b) {
            /** @var Effect $a */
            /** @var Effect $b */
            return $a->getPriority() <=> $b->getPriority();
        });

        return array_reduce($effects, function ($carry, Effect $item) {
            return $item->getModifier()->calculate($carry, $item);
        }, $baseValue);
    }
}
