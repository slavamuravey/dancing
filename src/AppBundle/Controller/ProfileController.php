<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Achievement\AchievementParticle;
use AppBundle\Entity\Achievement\AchievementStatus;
use AppBundle\Entity\Achievement\AchievementStatusUserRelation;
use AppBundle\Entity\Achievement\AchievementType;
use AppBundle\Entity\UserProfile;
use AppBundle\Form\Type\UserProfileEditType;
use AppBundle\Repository\Achievement\AchievementParticleRepository;
use AppBundle\Repository\Achievement\AchievementStatusRepository;
use AppBundle\Repository\Achievement\AchievementStatusUserRelationRepository;
use AppBundle\Service\Event\UserVisitProfileEvent;
use AppBundle\StoreEvents;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @var Route
 * @var ParamConverter
 */
class ProfileController extends AbstractController
{
    /**
     * @Route("/profile/view/{profile}", name="profile")
     * @ParamConverter("profile", class="AppBundle:UserProfile")
     *
     * @param UserProfile $profile
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction(UserProfile $profile = null)
    {
        $this->userVisitProfile();
        if (null === $profile) {
            $profile = $this->getUser()->getProfile();
        }

        /** @var AchievementStatusRepository $achievementStatusRepository */
        $achievementStatusRepository = $this->getRepository(AchievementStatus::class);

        return $this->render('app/profile/index.html.twig', [
            'profile' => $profile,
            'statusesData' => $achievementStatusRepository->getAchievementStatusWithRelationsForUser($profile->getUser()),
        ]);
    }

    /**
     * @Route("/profile/achievements/{profile}", name="profile_achievements")
     * @ParamConverter("profile", class="AppBundle:UserProfile")
     *
     * @param UserProfile $profile
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function achievementsAction(UserProfile $profile = null)
    {
        $this->userVisitProfile();
        if (null === $profile) {
            $profile = $this->getUser()->getProfile();
        }

        /** @var AchievementParticleRepository $achievementParticleRepository */
        $achievementParticleRepository = $this->getRepository(AchievementParticle::class);

        /** @var AchievementStatusRepository $achievementStatusRepository */
        $achievementStatusRepository = $this->getRepository(AchievementStatus::class);

        /** @var AchievementStatus[] $achievementStatuses */
        $achievementStatuses = $achievementStatusRepository->findAll();

        /** @var AchievementStatusUserRelationRepository $achievementStatusUserRelationRepository */
        $achievementStatusUserRelationRepository = $this->getRepository(AchievementStatusUserRelation::class);

        $achievementsData = [];

        foreach ($achievementStatuses as $achievementStatus) {
            $user = $profile->getUser();
            $achievementsData[$achievementStatus->getCode()] = [
                'achievementParticleData' => $achievementParticleRepository->getAchievementParticleRelationsForAchievementType($user, $achievementStatus),
                'achievementStatusUserRelation' => $achievementStatusUserRelationRepository->findOrCreateOneByUserAchievementStatus($user, $achievementStatus),
            ];
        }

        return $this->render('app/profile/achievements.html.twig', [
            'profile' => $profile,
            'achievementsData' => $achievementsData,
        ]);
    }

    /**
     * @Route("/profile/edit/{profile}", name="profile_edit")
     * @ParamConverter("profile", class="AppBundle:UserProfile")
     *
     * @param Request $request
     * @param UserProfile $profile
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, UserProfile $profile = null)
    {
        $this->userVisitProfile();
        if (null === $profile) {
            $profile = $this->getUser()->getProfile();
        }

        if ($profile != $this->getUser()->getProfile()) {
            throw new AccessDeniedHttpException();
        }

        $form = $this->createForm(UserProfileEditType::class, $profile);

        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->render('app/profile/edit.html.twig', [
                'form' => $form->createView(),
            ]);
        }

        $this->getEntityManager()->flush();

        return $this->redirect($this->generateUrl('profile_edit', ['profile' => $profile->getId()]));
    }

    private function userVisitProfile()
    {
        $event = new UserVisitProfileEvent($this->getUser());
        $event->setIncreaseOn(1);
        $event->setAchievementParticleCode(AchievementType::PROFILE_VISIT_1);
        $this->getEventDispatcher()->dispatch(StoreEvents::USER_VISIT_PROFILE, $event);
    }
}
