<?php

namespace AppBundle\Service\Event\Achievement;

interface AchievementParticleDataAwareInterface
{
    /**
     * @return int
     */
    public function getIncreaseOn();

    /**
     * @param int $increaseOn
     */
    public function setIncreaseOn($increaseOn);

    /**
     * @return string
     */
    public function getAchievementParticleCode();

    /**
     * @param string $achievementParticleCode
     */
    public function setAchievementParticleCode($achievementParticleCode);
}