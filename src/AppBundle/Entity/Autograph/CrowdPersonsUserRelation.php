<?php

namespace AppBundle\Entity\Autograph;

use AppBundle\Entity\User;
use AppBundle\Entity\UserProfile;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="autograph_crowd_persons_user_relations")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Autograph\CrowdPersonsUserRelationRepository")
 */
class CrowdPersonsUserRelation
{
    const SECURITY_ENERGY_REWARD = 5;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @var int
     *
     * @ORM\Column(name="steps1", type="integer", options={"default": "0"})
     */
    private $steps1 = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="steps2", type="integer", options={"default": "0"})
     */
    private $steps2 = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="steps3", type="integer", options={"default": "0"})
     */
    private $steps3 = 0;

    /**
     * @var City
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Autograph\City")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id" )
     */
    private $city;

    /**
     * @var CrowdPerson
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Autograph\CrowdPerson")
     * @ORM\JoinColumn(name="crowd_person_1", referencedColumnName="id" )
     */
    private $crowdPerson1;

    /**
     * @var CrowdPerson
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Autograph\CrowdPerson")
     * @ORM\JoinColumn(name="crowd_person_2", referencedColumnName="id" )
     */
    private $crowdPerson2;

    /**
     * @var CrowdPerson
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Autograph\CrowdPerson")
     * @ORM\JoinColumn(name="crowd_person_3", referencedColumnName="id" )
     */
    private $crowdPerson3;

    /**
     * @var bool
     *
     * @ORM\Column(name="disabled_1", type="boolean", options={"default": false})
     */
    private $disabled1 = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="disabled_2", type="boolean", options={"default": false})
     */
    private $disabled2 = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="disabled_3", type="boolean", options={"default": false})
     */
    private $disabled3 = false;

    /**
     * @var int
     *
     * @ORM\Column(name="security_energy", type="bigint", options={"default": CrowdPersonHater::SECURITY_ENERGY_5})
     */
    private $securityEnergy = CrowdPersonHater::SECURITY_ENERGY_5;

    /**
     * @var Bag
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Autograph\Bag", mappedBy="crowdPersonsUserRelation", cascade={"persist", "remove"})
     */
    private $bag;

    /**
     * @var bool
     *
     * @ORM\Column(name="disabled_all", type="boolean", options={"default": false})
     */
    private $disabledAll = false;

    /**
     * @var array
     *
     * @ORM\Column(name="cost", type="json_array", options={"default": "[]"})
     */
    private $cost = [];

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getSteps1()
    {
        return $this->steps1;
    }

    /**
     * @param int $steps1
     */
    public function setSteps1($steps1)
    {
        $this->steps1 = $steps1;
    }

    /**
     * @return int
     */
    public function getSteps2()
    {
        return $this->steps2;
    }

    /**
     * @param int $steps2
     */
    public function setSteps2($steps2)
    {
        $this->steps2 = $steps2;
    }

    /**
     * @return int
     */
    public function getSteps3()
    {
        return $this->steps3;
    }

    /**
     * @param int $steps3
     */
    public function setSteps3($steps3)
    {
        $this->steps3 = $steps3;
    }

    /**
     * @return City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param City $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return CrowdPerson
     */
    public function getCrowdPerson1()
    {
        return $this->crowdPerson1;
    }

    /**
     * @param CrowdPerson $crowdPerson1
     */
    public function setCrowdPerson1($crowdPerson1)
    {
        $this->crowdPerson1 = $crowdPerson1;
    }

    /**
     * @return CrowdPerson
     */
    public function getCrowdPerson2()
    {
        return $this->crowdPerson2;
    }

    /**
     * @param CrowdPerson $crowdPerson2
     */
    public function setCrowdPerson2($crowdPerson2)
    {
        $this->crowdPerson2 = $crowdPerson2;
    }

    /**
     * @return CrowdPerson
     */
    public function getCrowdPerson3()
    {
        return $this->crowdPerson3;
    }

    /**
     * @param CrowdPerson $crowdPerson3
     */
    public function setCrowdPerson3($crowdPerson3)
    {
        $this->crowdPerson3 = $crowdPerson3;
    }

    /**
     * @param int $place
     * @return CrowdPerson
     */
    public function getCrowdPerson($place)
    {
        switch ($place) {
            case 1:
                return $this->getCrowdPerson1();
            case 2:
                return $this->getCrowdPerson2();
            case 3:
                return $this->getCrowdPerson3();
            default:
                throw new \LogicException('Invalid place');
        }
    }

    /**
     * @param int $place
     * @param CrowdPerson|null $crowdPerson
     */
    public function setCrowdPerson($place, $crowdPerson)
    {
        switch ($place) {
            case 1:
                $this->setCrowdPerson1($crowdPerson);
                break;
            case 2:
                $this->setCrowdPerson2($crowdPerson);
                break;
            case 3:
                $this->setCrowdPerson3($crowdPerson);
                break;
            default:
                throw new \LogicException('Invalid place');
        }
    }

    public function setExitInPlace($place)
    {
        $this->setCrowdPerson($place, null);
    }

    public function increaseSteps($place)
    {
        $this->setSteps($place, $this->getSteps($place) + 1);
    }

    /**
     * @param int $place
     * @return int
     */
    public function getSteps($place)
    {
        switch ($place) {
            case 1:
                return $this->getSteps1();
            case 2:
                return $this->getSteps2();
            case 3:
                return $this->getSteps3();
            default:
                throw new \LogicException('Invalid place');
        }
    }

    /**
     * @param int $place
     * @param int $steps
     */
    public function setSteps($place, $steps)
    {
        switch ($place) {
            case 1:
                $this->setSteps1($steps);
                break;
            case 2:
                $this->setSteps2($steps);
                break;
            case 3:
                $this->setSteps3($steps);
                break;
            default:
                throw new \LogicException('Invalid place');
        }
    }

    /**
     * @return boolean
     */
    public function isDisabled1()
    {
        return $this->disabled1;
    }

    /**
     * @param boolean $disabled1
     */
    public function setDisabled1($disabled1)
    {
        $this->disabled1 = $disabled1;
    }

    /**
     * @return boolean
     */
    public function isDisabled2()
    {
        return $this->disabled2;
    }

    /**
     * @param boolean $disabled2
     */
    public function setDisabled2($disabled2)
    {
        $this->disabled2 = $disabled2;
    }

    /**
     * @return boolean
     */
    public function isDisabled3()
    {
        return $this->disabled3;
    }

    /**
     * @param boolean $disabled3
     */
    public function setDisabled3($disabled3)
    {
        $this->disabled3 = $disabled3;
    }

    /**
     * @param int $place
     * @return bool
     */
    public function isDisabled($place)
    {
        switch ($place) {
            case 1:
                return $this->isDisabled1();
            case 2:
                return $this->isDisabled2();
            case 3:
                return $this->isDisabled3();
            default:
                throw new \LogicException('Invalid place');
        }
    }

    /**
     * @param int $place
     * @param bool $disabled
     */
    public function setDisabled($place, $disabled)
    {
        switch ($place) {
            case 1:
                $this->setDisabled1($disabled);
                break;
            case 2:
                $this->setDisabled2($disabled);
                break;
            case 3:
                $this->setDisabled3($disabled);
                break;
            default:
                throw new \LogicException('Invalid place');
        }
    }

    public function makeStepToPlace($place)
    {
        if ($this->getCrowdPerson($place) instanceof CrowdPersonHater) {
            $this->goToHater($place);
        } else {
            $this->goToFan($place);
        }
    }

    /**
     * @param int $place
     * @return CrowdPerson
     */
    private function setRandomCrowdPersonInPlace($place)
    {
        $crowdPerson = $this->getCity()->getRandomCrowdPerson();
        $this->setCrowdPerson($place, $crowdPerson);

        return $crowdPerson;
    }

    /**
     * @param int $place
     */
    private function setRandomCrowdPersonFanInPlace($place)
    {
        $this->setCrowdPerson($place, $this->getCity()->getRandomCrowdPersonFan());
    }

    public function hasAtLeastOneHater()
    {
        return ($this->getCrowdPerson1() instanceof CrowdPersonHater)
        || ($this->getCrowdPerson2() instanceof CrowdPersonHater)
        || ($this->getCrowdPerson3() instanceof CrowdPersonHater);
    }

    public function hasAtLeastOneUnlockedFan()
    {
        return (($this->getCrowdPerson1() === null ||$this->getCrowdPerson1() instanceof CrowdPersonFan) && !$this->isDisabled1())
        || (($this->getCrowdPerson2() === null || $this->getCrowdPerson2() instanceof CrowdPersonFan) && !$this->isDisabled2())
        || (($this->getCrowdPerson3() === null || $this->getCrowdPerson3() instanceof CrowdPersonFan) && !$this->isDisabled3());
    }

    public function unlockPlaces()
    {
        $this->setDisabled1(false);
        $this->setDisabled2(false);
        $this->setDisabled3(false);
    }

    private function lockPlace($place)
    {
        $disabledPlacesMap = [
            1 => 2,
            2 => [1, 3][mt_rand(0, 1)],
            3 => 2,
        ];
        $this->setDisabled($disabledPlacesMap[$place], true);
    }

    private function moveLockToPlace2()
    {
        $this->unlockPlaces();
        $this->setDisabled2(true);
    }

    /**
     * @param int $place
     */
    public function goToHater($place)
    {
        $this->increaseSteps($place);
        if ($this->getCity()->getSteps() == $this->getSteps($place)) {
            $this->setExitInPlace($place);
        } else {
            if ($this->trySetProducer($place)) {
                return;
            }
            $this->setRandomCrowdPersonFanInPlace($place);
        }
        if ($this->hasAtLeastOneHater()) {
            if ($this->getCrowdPerson2() instanceof CrowdPersonFan) {
                $this->moveLockToPlace2();
            }
        } else {
            $this->unlockPlaces();
        }
    }

    /**
     * @param int $place
     */
    public function goToFan($place)
    {
        $this->increaseSteps($place);
        if ($this->getCity()->getSteps() == $this->getSteps($place)) {
            $this->setExitInPlace($place);
            return;
        }

        $hasAtLeastOneHater = $this->hasAtLeastOneHater();

        if ($this->trySetProducer($place)) {
            return;
        }

        $crowdPerson = $this->setRandomCrowdPersonInPlace($place);

        if ($crowdPerson instanceof CrowdPersonHater && !$hasAtLeastOneHater) {
            $this->lockPlace($place);
        }

        $min = $this->minSecurityEnergyNeeded();
        $this->setDisabledAll(!$this->hasAtLeastOneUnlockedFan() && $this->getSecurityEnergy() < $min);
    }

    /**
     * @param int $place
     */
    public function goToProducer($place)
    {
        $this->goToFan($place);
    }

    /**
     * @param City $city
     */
    public function goToCity(City $city)
    {
        $this->setCity($city);
        $this->setCrowdPerson1($this->getCity()->getRandomCrowdPersonFan());
        $this->setCrowdPerson2($this->getCity()->getRandomCrowdPersonFan());
        $this->setCrowdPerson3($this->getCity()->getRandomCrowdPersonFan());
        $this->setSteps1(0);
        $this->setSteps2(0);
        $this->setSteps3(0);
        $this->setDisabled1(false);
        $this->setDisabled2(false);
        $this->setDisabled3(false);
    }

    /**
     * @return int
     */
    public function getSecurityEnergy()
    {
        return $this->securityEnergy;
    }

    /**
     * @param int $securityEnergy
     */
    public function setSecurityEnergy($securityEnergy)
    {
        $this->securityEnergy = $securityEnergy;
    }

    /**
     * @return Bag
     */
    public function getBag()
    {
        return $this->bag;
    }

    /**
     * @param Bag $bag
     */
    public function setBag($bag)
    {
        $this->bag = $bag;
    }

    /**
     * @return boolean
     */
    public function isDisabledAll()
    {
        return $this->disabledAll;
    }

    /**
     * @param boolean $disabledAll
     */
    public function setDisabledAll($disabledAll)
    {
        $this->disabledAll = $disabledAll;
    }

    /**
     * @return array
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @param array $cost
     */
    public function setCost($cost)
    {
        $this->cost = $cost;
    }

    /**
     * @return int|mixed
     */
    public function minSecurityEnergyNeeded()
    {
        $hatersCosts = [];
        $places = self::getPlaces();
        foreach ($places as $place) {
            $crowdPerson = $this->getCrowdPerson($place);
            if ($crowdPerson instanceof CrowdPersonHater) {
                /** @var CrowdPersonHater $crowdPerson */
                $hatersCosts[] = $crowdPerson->getCost()[CrowdPersonsUserRelation::class]['securityEnergy'];
            }
        }

        $min = !empty($hatersCosts) ? min($hatersCosts) : 0;

        return $min;
    }

    public static function getPlaces()
    {
        return [1, 2, 3];
    }

    /**
     * @return array
     */
    public function getReward()
    {
        return [
            self::class => [
                'securityEnergy' => self::SECURITY_ENERGY_REWARD,
            ],
        ];
    }

    public function getRewardSecurityEnergy()
    {
        if (array_key_exists(self::class, $this->getReward()) && array_key_exists('securityEnergy', $this->getReward()[self::class])) {
            return $this->getReward()[self::class]['securityEnergy'];
        }

        return 0;
    }

    public function getCostDances()
    {
        if (array_key_exists(UserProfile::class, $this->getCost()) && array_key_exists('dances', $this->getCost()[UserProfile::class])) {
            return $this->getCost()[UserProfile::class]['dances'];
        }

        return 0;
    }

    public function appreciate()
    {
        $cost = $this->getCost();

        foreach ($cost as &$entity) {
            foreach ($entity as &$value) {
                $value *= 2;
            }
        }

        $this->setCost($cost);
    }

    public function getMaxSteps()
    {
        return max($this->getSteps1(), $this->getSteps2(), $this->getSteps3());
    }

    /**
     * @param $place
     * @return bool
     */
    private function trySetProducer($place)
    {
        $crowdPersonProducerUserRelations = $this->getCity()->getCrowdPersonProducerUserRelations();

        foreach ($crowdPersonProducerUserRelations as $crowdPersonProducerUserRelation) {
            if ($crowdPersonProducerUserRelation->getPlace() == $place
                && $crowdPersonProducerUserRelation->getSteps() == $this->getSteps($place)
                && $crowdPersonProducerUserRelation->getUser() != $this->getUser()) {
                $this->setCrowdPerson($place, $crowdPersonProducerUserRelation->getCrowdPersonProducer());
                return true;
            }
        }

        return false;
    }
}