<?php

namespace AppBundle\Service\BonusSystem\Source\Gang;

use AppBundle\Service\BonusSystem\Source\SourceInterface;

class ObjectRelationsSource implements SourceInterface
{
    /**
     * @param mixed $object
     * @return array
     */
    public function extractBonuses($object):array
    {
        return [];
    }
}
