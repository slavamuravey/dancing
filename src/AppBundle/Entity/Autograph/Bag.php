<?php

namespace AppBundle\Entity\Autograph;

use AppBundle\Entity\UserProfile;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="autograph_bags")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Autograph\BagRepository")
 */
class Bag
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var CrowdPersonsUserRelation
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Autograph\CrowdPersonsUserRelation", inversedBy="bag")
     * @ORM\JoinColumn(name="crowd_persons_user_relation_id", referencedColumnName="id", nullable=false)
     */
    private $crowdPersonsUserRelation;

    /**
     * @var int
     *
     * @ORM\Column(name="mani", type="bigint", options={"default": 0})
     */
    private $mani = 0;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return CrowdPersonsUserRelation
     */
    public function getCrowdPersonsUserRelation()
    {
        return $this->crowdPersonsUserRelation;
    }

    /**
     * @param CrowdPersonsUserRelation $crowdPersonsUserRelation
     */
    public function setCrowdPersonsUserRelation($crowdPersonsUserRelation)
    {
        $this->crowdPersonsUserRelation = $crowdPersonsUserRelation;
    }

    /**
     * @return int
     */
    public function getMani()
    {
        return $this->mani;
    }

    /**
     * @param int $mani
     */
    public function setMani($mani)
    {
        $this->mani = $mani;
    }

    /**
     * @return array
     */
    public function getReward()
    {
        return [
            UserProfile::class => [
                'mani' => $this->getMani(),
            ],
        ];
    }
}