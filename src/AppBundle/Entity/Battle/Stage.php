<?php

namespace AppBundle\Entity\Battle;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="battle_stages")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Battle\StageRepository")
 */
class Stage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", unique=true)
     */
    private $code;

    /**
     * @var int
     *
     * @ORM\Column(name="stage_value", type="integer")
     */
    private $stageValue;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return int
     */
    public function getStageValue()
    {
        return $this->stageValue;
    }

    /**
     * @param int $stageValue
     */
    public function setStageValue($stageValue)
    {
        $this->stageValue = $stageValue;
    }
}