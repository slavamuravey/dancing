<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Autograph\City;
use AppBundle\Entity\Autograph\CrowdPerson;
use AppBundle\Entity\Autograph\CrowdPersonFan;
use AppBundle\Entity\Autograph\CrowdPersonHater;
use AppBundle\Entity\Autograph\CrowdPersonProducer;
use AppBundle\Entity\Autograph\CrowdPersonsUserRelation;
use AppBundle\Repository\Autograph\CityRepository;
use AppBundle\Repository\Autograph\CrowdPersonsUserRelationRepository;
use AppBundle\Service\Event\RewardEvent;
use AppBundle\Service\Event\TryBuyEvent;
use AppBundle\StoreEvents;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @var Route
 */
class AutographController extends AbstractController
{
    /**
     * @Route("/autograph", name="autograph")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        /** @var CrowdPersonsUserRelationRepository $crowdPersonsUserRelationRepository */
        $crowdPersonsUserRelation = $this->getCrowdPersonsUserRelation();

        return $this->render('app/autograph/index.html.twig', [
            'crowdPersonsUserRelation' => $crowdPersonsUserRelation,
            'c' => [
                'FAN_TYPE' => CrowdPerson::FAN_TYPE,
                'HATER_TYPE' => CrowdPerson::HATER_TYPE,
                'PRODUCER_TYPE' => CrowdPerson::PRODUCER_TYPE,
            ],
        ]);
    }

    /**
     * @Route("/autograph/place/{place}", name="autograph_place")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function placeAction(Request $request)
    {
        $places = CrowdPersonsUserRelation::getPlaces();
        $place = $request->get('place');

        if (!in_array($place, $places)) {
            throw new NotFoundHttpException('Invalid place');
        }

        $crowdPersonsUserRelation = $this->getCrowdPersonsUserRelation();

        if ($crowdPersonsUserRelation->isDisabled($place)) {
            throw new AccessDeniedHttpException();
        }

        $crowdPerson = $crowdPersonsUserRelation->getCrowdPerson($place);
        $dispatcher = $this->getEventDispatcher();
        $user = $this->getUser();

        if ($crowdPerson instanceof CrowdPersonHater) {
            $event = new TryBuyEvent($user);
            /** @var CrowdPersonHater $crowdPerson */
            $event->setCost($crowdPerson->getCost());
            $dispatcher->addListener(StoreEvents::USER_DEBIT, function () use ($user, $crowdPersonsUserRelation, $place) {
                $crowdPersonsUserRelation->goToHater($place);
            });
            $dispatcher->dispatch(StoreEvents::USER_TRY_BUY, $event);
        } elseif ($crowdPerson instanceof CrowdPersonFan) {
            /** @var CrowdPersonFan $crowdPerson */
            $reward = $crowdPerson->getReward();
            $event = new RewardEvent($user, $reward);
            $event->setReward($reward);
            $dispatcher->dispatch(StoreEvents::USER_REWARD, $event);
            $crowdPersonsUserRelation->goToFan($place);
        } elseif ($crowdPerson instanceof CrowdPersonProducer) {
            $crowdPersonsUserRelation->goToProducer($place);
            $crowdPersonProducerUserRelation = $crowdPerson->getCrowdPersonProducerUserRelation();
            $crowdPersonProducerUserRelation->setUser($this->getUser());
            $crowdPersonProducerUserRelation->goToCity($this->getNextCity($crowdPersonsUserRelation));
        } else {
            $crowdPersonsUserRelation->goToCity($this->getNextCity($crowdPersonsUserRelation));
        }

        $this->getEntityManager()->flush();

        return $this->redirectToRoute('autograph');
    }

    /**
     * @Route("/autograph/escape", name="autograph_escape")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function escapeAction()
    {
        $crowdPersonsUserRelation = $this->getCrowdPersonsUserRelation();

        $em = $this->getEntityManager();
        $em->remove($crowdPersonsUserRelation);
        $em->flush($crowdPersonsUserRelation);

        return $this->redirectToRoute('clients');
    }

    /**
     * @Route("/autograph/take-bag", name="autograph_take_bag")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function takeBagAction()
    {
        $crowdPersonsUserRelation = $this->getCrowdPersonsUserRelation();
        $bag = $crowdPersonsUserRelation->getBag();
        $reward = $bag->getReward();

        $event = new RewardEvent($crowdPersonsUserRelation->getUser(), $reward);
        $event->setReward($reward);
        $this->getEventDispatcher()->dispatch(StoreEvents::USER_REWARD, $event);

        $em = $this->getEntityManager();
        $em->remove($crowdPersonsUserRelation);
        $em->flush($crowdPersonsUserRelation);

        return $this->redirectToRoute('clients');
    }

    /**
     * @Route("/autograph/buy-security-energy", name="autograph_buy_security_energy")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function buySecurityEnergyAction()
    {
        $user = $this->getUser();
        $event = new TryBuyEvent($user);
        $crowdPersonsUserRelation = $this->getCrowdPersonsUserRelation();
        $event->setCost($crowdPersonsUserRelation->getCost());
        $dispatcher = $this->getEventDispatcher();
        $dispatcher->addListener(StoreEvents::USER_DEBIT, function () use ($user, $dispatcher, $crowdPersonsUserRelation) {
            $reward = $crowdPersonsUserRelation->getReward();
            $event = new RewardEvent($user);
            $event->setReward($reward);
            $this->getEventDispatcher()->dispatch(StoreEvents::USER_REWARD, $event);
            $crowdPersonsUserRelation->setDisabledAll(false);
            $crowdPersonsUserRelation->appreciate();
            $this->getEntityManager()->flush($crowdPersonsUserRelation);
        });
        $dispatcher->dispatch(StoreEvents::USER_TRY_BUY, $event);

        return $this->redirectToRoute('autograph');
    }

    /**
     * @return CrowdPersonsUserRelation
     */
    private function getCrowdPersonsUserRelation()
    {
        /** @var CrowdPersonsUserRelationRepository $crowdPersonsUserRelationRepository */
        $crowdPersonsUserRelationRepository = $this->getRepository(CrowdPersonsUserRelation::class);
        $crowdPersonsUserRelation = $crowdPersonsUserRelationRepository->findOrCreateOneByUser($this->getUser());

        return $crowdPersonsUserRelation;
    }

    /**
     * @param CrowdPersonsUserRelation $crowdPersonsUserRelation
     * @return City|null
     */
    private function getNextCity(CrowdPersonsUserRelation $crowdPersonsUserRelation)
    {
        /** @var CityRepository $cityRepository */
        $cityRepository = $this->getRepository(City::class);
        $cityNumber = $crowdPersonsUserRelation->getCity()->getNumber();
        $nextCity = $cityRepository->getCityByNumber($cityNumber + 1);

        return $nextCity;
    }
}