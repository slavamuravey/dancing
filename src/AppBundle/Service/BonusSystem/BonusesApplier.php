<?php

namespace AppBundle\Service\BonusSystem;

use AppBundle\Service\BonusSystem\Source\BonusesExtractor;
use Doctrine\Common\Collections\ArrayCollection;

class BonusesApplier
{
    /**
     * @var BonusesExtractor
     */
    private $extractor;

    /**
     * @var EffectsMixerInterface
     */
    private $mixer;

    /**
     * @param BonusesExtractor $extractor
     * @param EffectsMixerInterface $mixer
     */
    public function __construct(BonusesExtractor $extractor, EffectsMixerInterface $mixer)
    {
        $this->extractor = $extractor;
        $this->mixer = $mixer;
    }

    /**
     * @param mixed $object
     * @param mixed $baseValue
     * @param string $type
     * @return mixed
     */
    public function apply($object, $baseValue, string $type)
    {
        $objectBonuses = $this->extractor->extractBonuses($object);

        $effects = $this->getBonusesEffectsByType($type, $objectBonuses);

        return $this->mixer->mix($baseValue, $effects);
    }

    /**
     * @param string $type
     * @param ArrayCollection|Bonus[] $objectBonuses
     * @return array
     */
    private function getBonusesEffectsByType(string $type, ArrayCollection $objectBonuses):array
    {
        $effects = [];

        foreach ($objectBonuses as $bonus) {
            foreach ($bonus->getEffects() as $effect) {
                if ($effect->getType() == $type) {
                    $effects[] = $effect;
                }
            }
        }

        return $effects;
    }
}
