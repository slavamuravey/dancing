<?php

namespace AppBundle\Service\BonusSystem\Source;

use Doctrine\Common\Cache\RedisCache;

class BonusesCacher
{
    const CACHE_PREFIX = 'app.bonuses.';

    /**
     * @var RedisCache
     */
    private $cache;

    /**
     * BonusesCacher constructor.
     * @param RedisCache $cache
     */
    public function __construct(RedisCache $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @param mixed $object
     * @param array $bonuses
     */
    public function cacheObjectBonuses($object, array $bonuses)
    {
        $objectBonusesCacheKey = $this->getObjectBonusesCacheKey($object);
        $expireAt = $this->getExpireAt($bonuses);

        $this->cache->getRedis()->set($objectBonusesCacheKey, $bonuses);

        if ($expireAt) {
            $this->cache->getRedis()->expireAt($objectBonusesCacheKey, $expireAt);
        }
    }

    /**
     * @param mixed $object
     * @return bool
     */
    public function objectCacheExists($object)
    {
        return $this->cache->getRedis()->exists($this->getObjectBonusesCacheKey($object));
    }

    /**
     * @param mixed $object
     * @return false|mixed
     */
    public function fetch($object)
    {
        return $this->cache->getRedis()->get($this->getObjectBonusesCacheKey($object));
    }

    /**
     * @param mixed $object
     * @return string
     */
    public function getObjectBonusesCacheKey($object):string
    {
        return self::CACHE_PREFIX . get_class($object) . $object->getId();
    }

    /**
     * @param array $bonuses
     * @return null|int
     */
    private function getExpireAt(array $bonuses)
    {
        $expireAtBonuses = [];

        array_walk($bonuses, function ($e) use (&$expireAtBonuses) {
            if (isset($e['expireAt']) && is_numeric($e['expireAt'])) {
                $expireAtBonuses[] = $e['expireAt'];
            }
        });

        return !empty($expireAtBonuses) ? min($expireAtBonuses) : null;
    }
}