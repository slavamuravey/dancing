<?php

namespace AppBundle\Service;

use Symfony\Component\Filesystem\Filesystem;

class ThingsImagesCompositor
{
    const WIDTH = 200;
    const HEIGHT = 200;

    /**
     * @var string
     */
    private $destDir;

    /**
     * @var Filesystem
     */
    private $fs;

    /**
     * @param string $destDir
     * @param Filesystem $fs
     */
    public function __construct($destDir, $fs)
    {
        $this->destDir = $destDir;
        $this->fs = $fs;
    }

    /**
     * @param array $images
     * @param int $width
     * @param int $height
     * @return string
     */
    public function generate(array $images, $width = self::WIDTH, $height = self::HEIGHT)
    {
        $destImagePath = $this->getDestImagePath($images);

        $this->fs->mkdir(dirname($destImagePath));

        $destImage = $this->prepareDestImage($width, $height);

        $imagesResources = $this->mergeImages($images, $width, $height, $destImage);

        $this->createDestImageFile($destImage, $destImagePath);

        $this->destroyImages($imagesResources);

        return $destImagePath;
    }

    /**
     * @param array $imagesResources
     */
    private function destroyImages(array $imagesResources)
    {
        foreach ($imagesResources as $imageResource) {
            imagedestroy($imageResource);
        }
    }

    /**
     * @param array $images
     * @return string
     */
    public function getDestImagePath(array $images)
    {
        return $this->destDir . '/' . md5(implode('', $images)) . '.png';
    }

    /**
     * @param $width
     * @param $height
     * @return resource
     */
    private function prepareDestImage($width, $height)
    {
        $destImage = imagecreatetruecolor($width, $height);
        imagesavealpha($destImage, true);
        $transBackground = imagecolorallocatealpha($destImage, 0, 0, 0, 127);
        imagefill($destImage, 0, 0, $transBackground);

        return $destImage;
    }

    /**
     * @param array $images
     * @param int $width
     * @param int $height
     * @param resource $destImage
     * @return array|resource[]
     */
    private function mergeImages(array $images, $width, $height, $destImage)
    {
        $imagesResources = [$destImage];

        foreach ($images as $imageUrl) {
            $imageResource = imagecreatefrompng($imageUrl);
            $imagesResources[] = $imageResource;
            imagecopy($destImage, $imageResource, 0, 0, 0, 0, $width, $height);
        }

        return $imagesResources;
    }

    /**
     * @param resource $destImage
     * @param string $destImagePath
     * @return bool
     */
    private function createDestImageFile($destImage, $destImagePath)
    {
        return imagepng($destImage, $destImagePath);
    }
}