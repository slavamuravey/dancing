<?php

namespace AppBundle\Repository\Autograph;

use AppBundle\Entity\Autograph\Bag;
use AppBundle\Entity\Autograph\City;
use AppBundle\Entity\Autograph\CrowdPersonsUserRelation;
use AppBundle\Entity\User;
use AppBundle\Entity\UserProfile;
use AppBundle\Repository\SearcherByUserInterface;
use Doctrine\ORM\EntityRepository;

class CrowdPersonsUserRelationRepository extends EntityRepository implements SearcherByUserInterface
{
    const START_DANCES_COST = 5;

    public function findOrCreateOneByUser(User $user)
    {
        /** @var CrowdPersonsUserRelation $crowdPersonsUserRelation */
        if (!($crowdPersonsUserRelation = $this->findOneBy(['user' => $user]))) {
            /** @var CityRepository $cityRepository */
            $cityRepository = $this->getEntityManager()->getRepository(City::class);
            $crowdPersonsUserRelation = new CrowdPersonsUserRelation();
            $crowdPersonsUserRelation->setUser($user);
            $crowdPersonsUserRelation->setCity($cityRepository->getCityByNumber(1));
            $crowdPersonsUserRelation->setCrowdPerson1($crowdPersonsUserRelation->getCity()->getRandomCrowdPersonFan());
            $crowdPersonsUserRelation->setCrowdPerson2($crowdPersonsUserRelation->getCity()->getRandomCrowdPersonFan());
            $crowdPersonsUserRelation->setCrowdPerson3($crowdPersonsUserRelation->getCity()->getRandomCrowdPersonFan());
            $crowdPersonsUserRelation->setCost([
                UserProfile::class => [
                    'dances' => self::START_DANCES_COST,
                ],
            ]);
            $bag = new Bag();
            $bag->setCrowdPersonsUserRelation($crowdPersonsUserRelation);
            $crowdPersonsUserRelation->setBag($bag);
            $this->getEntityManager()->persist($crowdPersonsUserRelation);
            $this->getEntityManager()->flush();
        }

        return $crowdPersonsUserRelation;
    }

    /**
     * @param User $user
     * @return null|CrowdPersonsUserRelation
     */
    public function findOneByUser(User $user)
    {
        return $this->findOneBy(['user' => $user]);
    }
}
