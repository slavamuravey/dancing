<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Battle\Stage;
use AppBundle\Repository\Battle\StageRepository;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadStagesData implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        /** @var StageRepository $stageRepository */
        $stageRepository = $manager->getRepository(Stage::class);
        $stageRepository->fillStagesData();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 6;
    }
}