<?php

namespace AppBundle\Service\Listener\Achievement;

use AppBundle\Service\Event\Achievement\AchievementQuestDoneEvent;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class AchievementQuestDoneNotificationListener
{
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @param SessionInterface $session
     */
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function onNotify(AchievementQuestDoneEvent $event)
    {
        $this->session->getFlashBag()->add('achievement_quest_done_notice', 'Испытание ' . $event->getAchievementQuestUserRelation()->getAchievementQuest()->getCode() . ' выполнено');
    }
}