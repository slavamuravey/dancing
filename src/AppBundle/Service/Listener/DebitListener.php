<?php

namespace AppBundle\Service\Listener;

use AppBundle\Repository\SearcherByUserInterface;
use AppBundle\Service\Event\DebitEvent;
use Doctrine\ORM\EntityManager;

class DebitListener
{
    /** @var EntityManager */
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function onDebit(DebitEvent $event)
    {
        foreach ($event->getCost() as $entityClass => $params) {
            /** @var SearcherByUserInterface $repository */
            $repository = $this->em->getRepository($entityClass);
            $entity = $repository->findOneByUser($event->getUser());
            foreach ($params as $paramName => $value) {
                call_user_func([$entity, 'set' . $paramName], call_user_func([$entity, 'get' . $paramName]) - $value);
            }
            $this->em->flush($entity);
        }
    }
}