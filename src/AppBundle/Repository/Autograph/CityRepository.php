<?php

namespace AppBundle\Repository\Autograph;

use AppBundle\Entity\Autograph\City;
use Doctrine\ORM\EntityRepository;

class CityRepository extends EntityRepository
{
    public function fillCitiesData()
    {
        $connection = $this->getEntityManager()->getConnection();

        $sql = <<<SQL
INSERT INTO autograph_cities (number, steps) VALUES
(1, 5),
(2, 7),
(3, 9),
(4, 12),
(5, 16),
(6, 21),
(7, 28),
(8, 37),
(9, 49),
(10, 65),
(11, 87),
(12, 115),
(13, 153),
(14, 204),
(15, 271),
(16, 360),
(17, 479),
(18, 637),
(19, 848),
(20, 1128),
(21, 1500),
(22, 1995),
(23, 2653),
(24, 3528),
(25, 4693),
(26, 6241),
(27, 8301),
(28, 11040),
(29, 14683),
(30, 19528)
SQL;

        $connection->executeQuery($sql);
    }

    /**
     * @param int $number
     * @return null|City
     */
    public function getCityByNumber($number)
    {
        return $this->findOneBy(['number' => (int)$number]);
    }
}
