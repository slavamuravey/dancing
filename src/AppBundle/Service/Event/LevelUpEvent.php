<?php

namespace AppBundle\Service\Event;

use AppBundle\Entity\UserLevel;

class LevelUpEvent extends AbstractEvent
{
    /** @var UserLevel */
    private $fromLevel;

    /** @var UserLevel */
    private $toLevel;

    /**
     * @return UserLevel
     */
    public function getFromLevel()
    {
        return $this->fromLevel;
    }

    /**
     * @param UserLevel $fromLevel
     */
    public function setFromLevel($fromLevel)
    {
        $this->fromLevel = $fromLevel;
    }

    /**
     * @return UserLevel
     */
    public function getToLevel()
    {
        return $this->toLevel;
    }

    /**
     * @param UserLevel $toLevel
     */
    public function setToLevel($toLevel)
    {
        $this->toLevel = $toLevel;
    }
}