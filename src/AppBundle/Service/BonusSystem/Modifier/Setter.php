<?php

namespace AppBundle\Service\BonusSystem\Modifier;

use AppBundle\Service\BonusSystem\Effect;

class Setter implements ModifierInterface
{
    /**
     * @param mixed $modifiedValue
     * @param Effect $effect
     * @return mixed
     */
    public function calculate($modifiedValue, Effect $effect)
    {
        return $effect->getValue();
    }
}