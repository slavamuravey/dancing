<?php

namespace AppBundle\Service\Listener;

use AppBundle\Entity\UserProfile;
use Doctrine\ORM\Event\PreUpdateEventArgs;

class EnergyUpdateListener
{
    /**
     * @param PreUpdateEventArgs $eventArgs
     */
    public function preUpdate(PreUpdateEventArgs $eventArgs)
    {
        $entity = $eventArgs->getObject();

        if (!($entity instanceof UserProfile)) {
            return;
        }

        if (!$eventArgs->hasChangedField('energy')) {
            return;
        }

        $entity->setEnergyUpdateDate(new \DateTime());
    }
}