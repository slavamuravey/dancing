<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\UserLevel;
use AppBundle\Repository\UserLevelRepository;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadLevelsData implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        /** @var UserLevelRepository $userLevelRepository */
        $userLevelRepository = $manager->getRepository(UserLevel::class);
        $userLevelRepository->fillLevelsData();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }
}