<?php

namespace AppBundle\Repository\Achievement;

use AppBundle\Entity\Achievement\AchievementStatus;
use AppBundle\Entity\Achievement\AchievementStatusUserRelation;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class AchievementStatusRepository extends EntityRepository
{
    public function getAchievementStatusWithRelationsForUser(User $user)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('s.id', 's.code', 'r.id rel_id', 'r.done')
            ->from(AchievementStatus::class, 's')
            ->leftJoin(AchievementStatusUserRelation::class, 'r', 'WITH', 's.id = r.achievementStatus AND r.user = :user')
            ->setParameter('user', $user);

        $q = $qb->getQuery();

        return $q->getResult();
    }
}
