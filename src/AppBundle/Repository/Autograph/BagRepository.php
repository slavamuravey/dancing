<?php

namespace AppBundle\Repository\Autograph;

use AppBundle\Entity\Autograph\Bag;
use AppBundle\Entity\Autograph\CrowdPersonsUserRelation;
use AppBundle\Entity\User;
use AppBundle\Repository\SearcherByUserInterface;
use Doctrine\ORM\EntityRepository;

class BagRepository extends EntityRepository implements SearcherByUserInterface
{
    /**
     * @param User $user
     * @return null|Bag
     */
    public function findOneByUser(User $user)
    {
        $qb = $this->createQueryBuilder('b');
        $qb->innerJoin(CrowdPersonsUserRelation::class, 'pur', 'WITH', 'b.crowdPersonsUserRelation = pur.id')
           ->where($qb->expr()->eq('pur.user', $user->getId()));

        $query = $qb->getQuery();

        return $query->getOneOrNullResult();
    }
}
