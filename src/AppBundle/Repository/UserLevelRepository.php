<?php

namespace AppBundle\Repository;

use AppBundle\Entity\UserLevel;
use Doctrine\ORM\EntityRepository;

class UserLevelRepository extends EntityRepository
{
    public function fillLevelsData()
    {
        $connection = $this->getEntityManager()->getConnection();

        $sql = <<<SQL
INSERT INTO user_levels (level, exp_needed) VALUES
(1, 0),
(2,	432),
(3,	518),
(4,	622),
(5,	746),
(6,	895),
(7,	1074),
(8,	1289),
(9,	1547),
(10, 1856),
(11, 2227),
(12, 2672),
(13, 3206),
(14, 3847),
(15, 4616),
(16, 5539),
(17, 6647),
(18, 7976),
(19, 9571),
(20, 11485),
(21, 13782),
(22, 16538),
(23, 19846),
(24, 23815),
(25, 28578),
(26, 34294),
(27, 41153),
(28, 49384),
(29, 59261),
(30, 71113),
(31, 85336)
SQL;

        $connection->executeQuery($sql);
    }

    /**
     * @return null|UserLevel
     */
    public function getLevelOne()
    {
        return $this->findOneBy(['level' => 1]);
    }

    /**
     * @param int $level
     * @return null|UserLevel
     */
    public function getNextLevelAfter($level)
    {
        return $this->findOneBy(['level' => (int)$level + 1]);
    }
}
