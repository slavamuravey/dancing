<?php

namespace AppBundle\Service\BonusSystem\Source\User;

use AppBundle\Service\BonusSystem\Source\SourceInterface;

class ObjectRelationsSource implements SourceInterface
{
    /**
     * @param mixed $object
     * @return array
     */
    public function extractBonuses($object):array
    {
        return ['bonus2' => [], 'bonus3' => []];
    }
}
