<?php

namespace AppBundle\Repository;

use AppBundle\Entity\User;

interface SearcherByUserInterface
{
    /**
     * @param User $user
     * @return null|object
     */
    public function findOneByUser(User $user);
}