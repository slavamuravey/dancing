<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Client;
use AppBundle\Entity\UserProfile;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadClientsData implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $client = new Client();
        $client->setRewardPeriodSeconds(30);
        $client->setCode('club');
        $client->setCost([
            UserProfile::class => [
                'mani' => 500,
            ],
        ]);
        $client->setReward([
            UserProfile::class => [
                'mani' => 100,
                'exp' => 10,
            ],
        ]);
        $manager->persist($client);

        $client = new Client();
        $client->setRewardPeriodSeconds(100);
        $client->setCode('factory');
        $client->setCost([
            UserProfile::class => [
                'mani' => 700,
            ],
        ]);
        $client->setReward([
            UserProfile::class => [
                'mani' => 150,
                'dances' => 5,
                'exp' => 50,
            ],
        ]);
        $manager->persist($client);

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 3;
    }
}