<?php

namespace AppBundle\Service\BonusSystem\Source;

use AppBundle\Service\BonusSystem\Bonus;
use AppBundle\Service\BonusSystem\BonusHydrator;
use Doctrine\Common\Collections\ArrayCollection;

class BonusesExtractor
{
    /**
     * @var array
     */
    private $bonusesConfig;

    /**
     * @var BonusesCacher
     */
    private $bonusesCacher;

    /**
     * @var BonusHydrator
     */
    private $hydrator;

    /**
     * @var array
     */
    private $sourcesConfig = [];

    /**
     * @param array $bonusesConfig
     * @param BonusesCacher $bonusesCacher
     * @param BonusHydrator $hydrator
     * @param array $sourcesConfig
     */
    public function __construct(
        array $bonusesConfig,
        BonusesCacher $bonusesCacher,
        BonusHydrator $hydrator,
        array $sourcesConfig
    )
    {
        $this->bonusesConfig = $bonusesConfig;
        $this->bonusesCacher = $bonusesCacher;
        $this->hydrator = $hydrator;
        $this->sourcesConfig = $sourcesConfig;
    }

    /**
     * @param mixed $object
     * @return ArrayCollection|Bonus[]
     */
    public function extractBonuses($object):ArrayCollection
    {
        $bonuses = [];
        $objectClass = get_class($object);

        if (!array_key_exists($objectClass, $this->sourcesConfig)) {
            throw new \RuntimeException(sprintf('No sources config for class "%s"', $objectClass));
        }

        if ($this->bonusesCacher->objectCacheExists($object)) {
            return $this->getHydratedBonuses($this->bonusesCacher->fetch($object));
        }

        $sources = $this->sourcesConfig[$objectClass];

        foreach ($sources as $source) {
            /** @var $source SourceInterface */
            $bonuses = array_merge($bonuses, $source->extractBonuses($object));
        }

        $this->bonusesCacher->cacheObjectBonuses($object, $bonuses);

        return $this->getHydratedBonuses($bonuses);
    }

    /**
     * @param array $bonuses
     * @return ArrayCollection
     */
    private function getHydratedBonuses(array $bonuses)
    {
        $hydratedBonuses = [];

        foreach ($bonuses as $bonusName => $bonusParams) {
            $hydratedBonuses[$bonusName] = $this->hydrator->hydrate(
                array_merge($this->bonusesConfig[$bonusName], $bonusParams),
                Bonus::class
            );
        }

        return new ArrayCollection($hydratedBonuses);
    }
}
