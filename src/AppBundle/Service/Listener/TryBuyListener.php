<?php

namespace AppBundle\Service\Listener;

use AppBundle\Repository\SearcherByUserInterface;
use AppBundle\Service\Event\DebitEvent;
use AppBundle\Service\Event\TryBuyEvent;
use AppBundle\StoreEvents;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class TryBuyListener
{
    /** @var EntityManager */
    private $em;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @param EntityManager $em
     * @param SessionInterface $session
     */
    public function __construct(EntityManager $em, SessionInterface $session)
    {
        $this->em = $em;
        $this->session = $session;
    }

    /**
     * @param TryBuyEvent $tryBuyEvent
     * @param string $eventName
     * @param EventDispatcherInterface $dispatcher
     */
    public function onTryBuy(TryBuyEvent $tryBuyEvent, $eventName, EventDispatcherInterface $dispatcher)
    {
        $flashBag = $this->session->getFlashBag();
        if ($this->checkUserResources($tryBuyEvent) === false) {
            $flashBag->add('insufficient_funds', 'Недостаточно средств!');
        } else {
            $cost = $tryBuyEvent->getCost();
            $debitEvent = new DebitEvent($tryBuyEvent->getUser(), $cost);
            $debitEvent->setCost($cost);
            $dispatcher->dispatch(StoreEvents::USER_DEBIT, $debitEvent);
            $flashBag->add('debit', 'списано ' . json_encode($cost));
        }
    }

    /**
     * @param TryBuyEvent $event
     * @return bool|array
     */
    private function checkUserResources(TryBuyEvent $event)
    {
        $cost = $event->getCost();

        foreach ($cost as $entityClass => $params) {
            /** @var SearcherByUserInterface $repository */
            $repository = $this->em->getRepository($entityClass);
            $entity = $repository->findOneByUser($event->getUser());
            foreach ($params as $paramName => $value) {
                if (call_user_func([$entity, 'get' . $paramName]) < $value) {
                    return false;
                }
            }
            $this->em->flush($entity);
        }

        return $cost;
    }
}