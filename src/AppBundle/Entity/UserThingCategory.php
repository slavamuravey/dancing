<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="user_thing_categories")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserThingCategoryRepository")
 */
class UserThingCategory
{
    const HEAD = 'head';
    const BODY = 'body';
    const SHOES = 'shoes';
    const JEWELRY = 'jewelry';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string")
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @var UserThing[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\UserThing", mappedBy="category")
     */
    private $things;

    public function __construct()
    {
        $this->things = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return UserThing[]|ArrayCollection
     */
    public function getThings()
    {
        return $this->things;
    }

    /**
     * @param UserThing[]|ArrayCollection $things
     */
    public function setThings($things)
    {
        $this->things = $things;
    }
}
