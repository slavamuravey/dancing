<?php

namespace AppBundle\Entity\Achievement;

use AppBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="achievement_particle_user_relations", uniqueConstraints={@ORM\UniqueConstraint(name="uniq_user_id_achievement_particle_id", columns={"user_id", "achievement_particle_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Achievement\AchievementParticleUserRelationRepository")
 */
class AchievementParticleUserRelation extends AchievementTypeUserRelation
{
    /**
     * @var int
     *
     * @ORM\Column(name="current_value", type="bigint", options={"default": 0})
     */
    private $currentValue = 0;

    /**
     * @var AchievementParticle
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Achievement\AchievementParticle", inversedBy="achievementParticleUserRelations")
     * @ORM\JoinColumn(name="achievement_particle_id", referencedColumnName="id", nullable=false)
     */
    private $achievementParticle;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="achievementParticleUserRelations")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @return int
     */
    public function getCurrentValue()
    {
        return $this->currentValue;
    }

    /**
     * @param int $currentValue
     */
    public function setCurrentValue($currentValue)
    {
        $this->currentValue = min($currentValue, $this->getAchievementParticle()->getRequiredVolume());
    }

    /**
     * @return AchievementParticle
     */
    public function getAchievementParticle()
    {
        return $this->achievementParticle;
    }

    /**
     * @param AchievementParticle $achievementParticle
     */
    public function setAchievementParticle($achievementParticle)
    {
        $this->achievementParticle = $achievementParticle;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    public function checkDone()
    {
        return $this->getCurrentValue() >= $this->getAchievementParticle()->getRequiredVolume();
    }
}