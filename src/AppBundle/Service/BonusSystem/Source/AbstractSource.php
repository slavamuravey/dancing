<?php

namespace AppBundle\Service\BonusSystem\Source;

abstract class AbstractSource
{
    /**
     * @var array
     */
    private $bonusesConfig;

    /**
     * @param array $bonusesConfig
     */
    public function __construct(array $bonusesConfig)
    {
        $this->bonusesConfig = $bonusesConfig;
    }

    /**
     * @return array
     */
    protected function getBonusesConfig()
    {
        return $this->bonusesConfig;
    }
}
