<?php

namespace AppBundle\Controller;

use AppBundle\Entity\UserProfileUserThingRelation;
use AppBundle\Entity\UserThing;
use AppBundle\Entity\UserThingCategory;
use AppBundle\Repository\UserProfileUserThingRelationRepository;
use AppBundle\Repository\UserThingCategoryRepository;
use AppBundle\Repository\UserThingRepository;
use AppBundle\Service\Event\TryBuyEvent;
use AppBundle\Service\ThingsImagesCompositor;
use AppBundle\StoreEvents;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @var Route
 * @var ParamConverter
 */
class ThingsController extends AbstractController
{
    /**
     * @Route("/things", name="things")
     */
    public function indexAction()
    {
        $profile = $this->getUser()->getProfile();

        /** @var UserThingCategoryRepository $categoriesRepository */
        $categoriesRepository = $this->getRepository(UserThingCategory::class);
        $categories = $categoriesRepository->findAll();

        return $this->render('app/things/index.html.twig', [
            'profile' => $profile,
            'categories' => $categories,
        ]);
    }

    /**
     * @Route("/things/category/{category}", name="things_category")
     * @ParamConverter("category", class="AppBundle:UserThingCategory")
     *
     * @param UserThingCategory $category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function categoryAction(UserThingCategory $category)
    {
        $profile = $this->getUser()->getProfile();

        /** @var UserThingRepository $userThingRepository */
        $userThingRepository = $this->getRepository(UserThing::class);

        return $this->render('app/things/category.html.twig', [
            'thingsData' => $userThingRepository->getThingsWithRelationsForUserProfile($profile, $category),
            'category' => $category,
            'profile' => $profile,
        ]);
    }

    /**
     * @Route("/things/buy/{thing}", name="things_buy")
     * @ParamConverter("thing", class="AppBundle:UserThing")
     *
     * @param UserThing $thing
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function buyAction(UserThing $thing)
    {
        $user = $this->getUser();
        $profile = $user->getProfile();

        /** @var UserProfileUserThingRelationRepository $userProfileUserThingRelationRepository */
        $userProfileUserThingRelationRepository = $this->getRepository(UserProfileUserThingRelation::class);

        if ($userProfileUserThingRelationRepository->hasUserProfileUserThing($profile, $thing)) {
            return $this->redirectToRoute('things');
        }

        $event = new TryBuyEvent($user);
        $event->setCost($thing->getCost());

        $dispatcher = $this->getEventDispatcher();
        $dispatcher->addListener(StoreEvents::USER_DEBIT, function () use ($profile, $thing) {
            $userProfileUserThingRelation = new UserProfileUserThingRelation();
            $userProfileUserThingRelation->setProfile($profile);
            $userProfileUserThingRelation->setThing($thing);
            $userProfileUserThingRelations = $profile->getUserProfileUserThingRelations();
            $userProfileUserThingRelations->add($userProfileUserThingRelation);

            $em = $this->getEntityManager();
            $em->persist($userProfileUserThingRelation);
            $em->flush();
        });

        $dispatcher->dispatch(StoreEvents::USER_TRY_BUY, $event);

        return $this->redirectToRoute('things_category', [
            'category' => $thing->getCategory()->getId(),
        ]);
    }

    /**
     * @Route("/things/wear/{userProfileUserThingRelation}", name="things_wear")
     * @ParamConverter("userProfileUserThingRelation", class="AppBundle:UserProfileUserThingRelation")
     *
     * @param UserProfileUserThingRelation $userProfileUserThingRelation
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function wearAction(UserProfileUserThingRelation $userProfileUserThingRelation)
    {
        $userProfileUserThingRelation->setWearing(true);
        $em = $this->getEntityManager();
        $em->flush();
        $this->getUser()->getProfile()->setAvatar($this->getAvatar());
        $em->flush();

        return $this->redirectToRoute('things_category', [
            'category' => $userProfileUserThingRelation->getThing()->getCategory()->getId(),
        ]);
    }

    /**
     * @Route("/things/take-off/{userProfileUserThingRelation}", name="things_take_off")
     * @ParamConverter("userProfileUserThingRelation", class="AppBundle:UserProfileUserThingRelation")
     *
     * @param UserProfileUserThingRelation $userProfileUserThingRelation
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function takeOffAction(UserProfileUserThingRelation $userProfileUserThingRelation)
    {
        $userProfileUserThingRelation->setWearing(false);
        $em = $this->getEntityManager();
        $em->flush();
        $this->getUser()->getProfile()->setAvatar($this->getAvatar());
        $em->flush();

        return $this->redirectToRoute('things_category', [
            'category' => $userProfileUserThingRelation->getThing()->getCategory()->getId(),
        ]);
    }

    private function getAvatar()
    {
        /** @var ThingsImagesCompositor $thingsImagesCompositor */
        $thingsImagesCompositor = $this->get('app.things_images_compositor');
        /** @var UserProfileUserThingRelationRepository $userProfileUserThingRelationRepository */
        $userProfileUserThingRelationRepository = $this->getRepository(UserProfileUserThingRelation::class);
        $images = $userProfileUserThingRelationRepository->getImagePathsForWearingThingsByProfile($this->getUser()->getProfile()->getId());

        return basename($thingsImagesCompositor->getDestImagePath($images));
    }
}
