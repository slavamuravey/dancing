<?php

namespace AppBundle\Entity\Autograph;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="autograph_city_crowd_person_relations")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Autograph\CityCrowdPersonRelationRepository")
 */
class CityCrowdPersonRelation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var City
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Autograph\City", inversedBy="cityCrowdPersonRelations")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id", nullable=false)
     */
    private $city;

    /**
     * @var CrowdPerson
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Autograph\CrowdPerson")
     * @ORM\JoinColumn(name="crowd_person_id", referencedColumnName="id", nullable=false)
     */
    private $crowdPerson;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param City $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return CrowdPerson
     */
    public function getCrowdPerson()
    {
        return $this->crowdPerson;
    }

    /**
     * @param CrowdPerson $crowdPerson
     */
    public function setCrowdPerson($crowdPerson)
    {
        $this->crowdPerson = $crowdPerson;
    }
}