<?php

namespace AppBundle\Service\Listener;

use AppBundle\Entity\Achievement\AchievementType;
use AppBundle\Service\Event\AddClientEvent;
use AppBundle\StoreEvents;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class AddClientFactoryAddClientListener
{
    /**
     * @param AddClientEvent $event
     * @param string $eventName
     * @param EventDispatcherInterface $dispatcher
     */
    public function onDispatch(AddClientEvent $event, $eventName, EventDispatcherInterface $dispatcher)
    {
        $event->setAchievementParticleCode(AchievementType::CLIENT_COMMON_COUNT_1);
        $dispatcher->dispatch(StoreEvents::USER_ADD_CLIENT, $event);
    }
}