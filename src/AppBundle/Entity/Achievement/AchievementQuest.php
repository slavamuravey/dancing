<?php

namespace AppBundle\Entity\Achievement;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="achievement_quests")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Achievement\AchievementQuestRepository")
 */
class AchievementQuest extends AchievementType
{
    /**
     * @var array
     *
     * @ORM\Column(name="reward", type="json_array", options={"default": "[]"})
     */
    private $reward = [];

    /**
     * @var ArrayCollection|AchievementQuestUserRelation[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Achievement\AchievementQuestUserRelation", mappedBy="achievementQuest")
     */
    private $achievementQuestUserRelations;

    public function __construct()
    {
        $this->achievementQuestUserRelations = new ArrayCollection();
        parent::__construct();
    }

    /**
     * @return AchievementQuestUserRelation[]|ArrayCollection
     */
    public function getAchievementQuestUserRelations()
    {
        return $this->achievementQuestUserRelations;
    }

    /**
     * @param AchievementQuestUserRelation[]|ArrayCollection $achievementQuestUserRelations
     */
    public function setAchievementQuestUserRelations($achievementQuestUserRelations)
    {
        $this->achievementQuestUserRelations = $achievementQuestUserRelations;
    }

    /**
     * @return array
     */
    public function getReward()
    {
        return $this->reward;
    }

    /**
     * @param array $reward
     */
    public function setReward($reward)
    {
        $this->reward = $reward;
    }
}
