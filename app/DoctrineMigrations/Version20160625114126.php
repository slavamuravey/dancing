<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160625114126 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE battle_jury_member_user_relations (id SERIAL NOT NULL, user_id INT NOT NULL, stage_id INT NOT NULL, spirits_id INT NOT NULL, member VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B7BAFC1CA76ED395 ON battle_jury_member_user_relations (user_id)');
        $this->addSql('CREATE INDEX IDX_B7BAFC1C2298D193 ON battle_jury_member_user_relations (stage_id)');
        $this->addSql('CREATE INDEX IDX_B7BAFC1C7927E467 ON battle_jury_member_user_relations (spirits_id)');
        $this->addSql('CREATE TABLE battle_stages (id SERIAL NOT NULL, code VARCHAR(255) NOT NULL, stage_value INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_AD4839577153098 ON battle_stages (code)');
        $this->addSql('CREATE TABLE battle_spirits (id SERIAL NOT NULL, code VARCHAR(255) NOT NULL, value INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_111CBD9A77153098 ON battle_spirits (code)');
        $this->addSql('CREATE TABLE autograph_crowd_person_producer_user_relations (id SERIAL NOT NULL, user_id INT DEFAULT NULL, city_id INT DEFAULT NULL, crowd_person_producer INT DEFAULT NULL, place SMALLINT CHECK (place IN (1, 2, 3)), steps INT DEFAULT 1 NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_972C99FA76ED395 ON autograph_crowd_person_producer_user_relations (user_id)');
        $this->addSql('CREATE INDEX IDX_972C99F8BAC62AF ON autograph_crowd_person_producer_user_relations (city_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_972C99FCEC5A0B8 ON autograph_crowd_person_producer_user_relations (crowd_person_producer)');
        $this->addSql('CREATE TABLE autograph_crowd_persons (id SERIAL NOT NULL, code VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3B7629377153098 ON autograph_crowd_persons (code)');
        $this->addSql('CREATE TABLE autograph_crowd_person_fans (id INT NOT NULL, reward JSON DEFAULT \'[]\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE autograph_cities (id SERIAL NOT NULL, number INT NOT NULL, steps INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1E07F0E796901F54 ON autograph_cities (number)');
        $this->addSql('CREATE TABLE autograph_city_crowd_person_relations (id SERIAL NOT NULL, city_id INT NOT NULL, crowd_person_id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_15B451848BAC62AF ON autograph_city_crowd_person_relations (city_id)');
        $this->addSql('CREATE INDEX IDX_15B4518499A0AB13 ON autograph_city_crowd_person_relations (crowd_person_id)');
        $this->addSql('CREATE TABLE autograph_crowd_persons_user_relations (id SERIAL NOT NULL, user_id INT NOT NULL, city_id INT DEFAULT NULL, crowd_person_1 INT DEFAULT NULL, crowd_person_2 INT DEFAULT NULL, crowd_person_3 INT DEFAULT NULL, steps1 INT DEFAULT 0 NOT NULL, steps2 INT DEFAULT 0 NOT NULL, steps3 INT DEFAULT 0 NOT NULL, disabled_1 BOOLEAN DEFAULT \'false\' NOT NULL, disabled_2 BOOLEAN DEFAULT \'false\' NOT NULL, disabled_3 BOOLEAN DEFAULT \'false\' NOT NULL, security_energy BIGINT DEFAULT 5 NOT NULL, disabled_all BOOLEAN DEFAULT \'false\' NOT NULL, cost JSON DEFAULT \'[]\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_13203D8AA76ED395 ON autograph_crowd_persons_user_relations (user_id)');
        $this->addSql('CREATE INDEX IDX_13203D8A8BAC62AF ON autograph_crowd_persons_user_relations (city_id)');
        $this->addSql('CREATE INDEX IDX_13203D8AC3200087 ON autograph_crowd_persons_user_relations (crowd_person_1)');
        $this->addSql('CREATE INDEX IDX_13203D8A5A29513D ON autograph_crowd_persons_user_relations (crowd_person_2)');
        $this->addSql('CREATE INDEX IDX_13203D8A2D2E61AB ON autograph_crowd_persons_user_relations (crowd_person_3)');
        $this->addSql('CREATE TABLE autograph_bags (id SERIAL NOT NULL, crowd_persons_user_relation_id INT NOT NULL, mani BIGINT DEFAULT 0 NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FBFB127A247E8347 ON autograph_bags (crowd_persons_user_relation_id)');
        $this->addSql('CREATE TABLE autograph_crowd_person_haters (id INT NOT NULL, cost JSON DEFAULT \'[]\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE autograph_crowd_person_producers (id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE user_things (id SERIAL NOT NULL, category_id INT DEFAULT NULL, layer_level INT NOT NULL, code VARCHAR(255) NOT NULL, sex VARCHAR(1) CHECK (sex IN (\'f\',\'m\')), cost JSON DEFAULT \'[]\' NOT NULL, img VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_AB64A80BBC2C8AC ON user_things (img)');
        $this->addSql('CREATE INDEX IDX_AB64A8012469DE2 ON user_things (category_id)');
        $this->addSql('CREATE TABLE user_client_relations (id SERIAL NOT NULL, user_id INT NOT NULL, client_id INT NOT NULL, reward_time TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_82592DAAA76ED395 ON user_client_relations (user_id)');
        $this->addSql('CREATE INDEX IDX_82592DAA19EB6921 ON user_client_relations (client_id)');
        $this->addSql('CREATE TABLE users (id SERIAL NOT NULL, username VARCHAR(25) NOT NULL, password VARCHAR(64) NOT NULL, active BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E9F85E0677 ON users (username)');
        $this->addSql('CREATE TABLE user_role_relations (user_id INT NOT NULL, role_id INT NOT NULL, PRIMARY KEY(user_id, role_id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2E1DB3FDA76ED395 ON user_role_relations (user_id)');
        $this->addSql('CREATE INDEX IDX_2E1DB3FDD60322AC ON user_role_relations (role_id)');
        $this->addSql('CREATE TABLE achievement_types (id SERIAL NOT NULL, code VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F650133E77153098 ON achievement_types (code)');
        $this->addSql('CREATE TABLE achievement_quests (id INT NOT NULL, reward JSON DEFAULT \'[]\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE achievement_type_user_relations (id SERIAL NOT NULL, done BOOLEAN DEFAULT \'false\' NOT NULL, rel VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE achievement_status_user_relations (id INT NOT NULL, achievement_status_id INT NOT NULL, user_id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_61154DB158389F76 ON achievement_status_user_relations (achievement_status_id)');
        $this->addSql('CREATE INDEX IDX_61154DB1A76ED395 ON achievement_status_user_relations (user_id)');
        $this->addSql('CREATE UNIQUE INDEX uniq_user_id_achievement_status_id ON achievement_status_user_relations (user_id, achievement_status_id)');
        $this->addSql('CREATE TABLE achievement_particles (id INT NOT NULL, achievement_type_id INT DEFAULT NULL, required_volume BIGINT DEFAULT 0 NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_FF9AA6B14B157427 ON achievement_particles (achievement_type_id)');
        $this->addSql('CREATE TABLE achievement_quest_user_relations (id INT NOT NULL, achievement_quest_id INT NOT NULL, user_id INT NOT NULL, current BOOLEAN DEFAULT \'false\' NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_1D4E9802EC1CB099 ON achievement_quest_user_relations (achievement_quest_id)');
        $this->addSql('CREATE INDEX IDX_1D4E9802A76ED395 ON achievement_quest_user_relations (user_id)');
        $this->addSql('CREATE UNIQUE INDEX uniq_user_id_achievement_quest_id ON achievement_quest_user_relations (user_id, achievement_quest_id)');
        $this->addSql('CREATE TABLE achievement_particle_user_relations (id INT NOT NULL, achievement_particle_id INT NOT NULL, user_id INT NOT NULL, current_value BIGINT DEFAULT 0 NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_4334A72E4C05BE6A ON achievement_particle_user_relations (achievement_particle_id)');
        $this->addSql('CREATE INDEX IDX_4334A72EA76ED395 ON achievement_particle_user_relations (user_id)');
        $this->addSql('CREATE UNIQUE INDEX uniq_user_id_achievement_particle_id ON achievement_particle_user_relations (user_id, achievement_particle_id)');
        $this->addSql('CREATE TABLE achievement_statuses (id INT NOT NULL, buffs JSON DEFAULT \'[]\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE roles (id SERIAL NOT NULL, name VARCHAR(30) NOT NULL, role VARCHAR(20) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE user_profiles (id SERIAL NOT NULL, user_id INT NOT NULL, level_id INT NOT NULL, mani BIGINT DEFAULT 2000 NOT NULL, dances BIGINT DEFAULT 0 NOT NULL, acting BIGINT DEFAULT 5 NOT NULL, sports_training BIGINT DEFAULT 5 NOT NULL, popularity BIGINT DEFAULT 5 NOT NULL, name VARCHAR(255) DEFAULT NULL, birth_date DATE DEFAULT NULL, phone VARCHAR(255) DEFAULT NULL, email VARCHAR(60) DEFAULT NULL, sex VARCHAR(1) NOT NULL CHECK (sex IN (\'f\',\'m\')), exp BIGINT DEFAULT 0 NOT NULL, rating BIGINT DEFAULT 0 NOT NULL, avatar VARCHAR(255) DEFAULT NULL, energy_update_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, energy BIGINT DEFAULT 50 NOT NULL, energy_max BIGINT DEFAULT 50 NOT NULL, energy_period_seconds INT DEFAULT 30 NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6BBD6130E7927C74 ON user_profiles (email)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6BBD6130A76ED395 ON user_profiles (user_id)');
        $this->addSql('CREATE INDEX IDX_6BBD61305FB14BA7 ON user_profiles (level_id)');
        $this->addSql('CREATE TABLE user_levels (id SERIAL NOT NULL, level INT NOT NULL, exp_needed INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FA787F249AEACC13 ON user_levels (level)');
        $this->addSql('CREATE TABLE user_thing_categories (id SERIAL NOT NULL, code VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE bonus_system_operations (id SERIAL NOT NULL, service_name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BF1DB9BC9060CAEA ON bonus_system_operations (service_name)');
        $this->addSql('CREATE TABLE bonus_system_types (id SERIAL NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D2A5B7515E237E06 ON bonus_system_types (name)');
        $this->addSql('CREATE TABLE bonus_system_bonuses (id SERIAL NOT NULL, operation_id INT NOT NULL, type_id INT NOT NULL, code VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, applying_value DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_38B2030A77153098 ON bonus_system_bonuses (code)');
        $this->addSql('CREATE INDEX IDX_38B2030A44AC3583 ON bonus_system_bonuses (operation_id)');
        $this->addSql('CREATE INDEX IDX_38B2030AC54C8C93 ON bonus_system_bonuses (type_id)');
        $this->addSql('CREATE TABLE bonus_system_user_bonus_relations (id SERIAL NOT NULL, user_id INT NOT NULL, bonus_id INT NOT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E327B621A76ED395 ON bonus_system_user_bonus_relations (user_id)');
        $this->addSql('CREATE INDEX IDX_E327B62169545666 ON bonus_system_user_bonus_relations (bonus_id)');
        $this->addSql('CREATE TABLE user_events (id SERIAL NOT NULL, user_id INT NOT NULL, event_name VARCHAR(255) NOT NULL, arguments JSON DEFAULT \'[]\' NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_36D54C77A76ED395 ON user_events (user_id)');
        $this->addSql('CREATE TABLE user_profile_user_thing_relations (id SERIAL NOT NULL, profile_id INT DEFAULT NULL, thing_id INT DEFAULT NULL, wearing BOOLEAN DEFAULT \'false\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_73E69CB9CCFA12B8 ON user_profile_user_thing_relations (profile_id)');
        $this->addSql('CREATE INDEX IDX_73E69CB9C36906A7 ON user_profile_user_thing_relations (thing_id)');
        $this->addSql('CREATE UNIQUE INDEX uniq_profile_id_thing_id ON user_profile_user_thing_relations (profile_id, thing_id)');
        $this->addSql('CREATE TABLE clients (id SERIAL NOT NULL, reward_period_seconds INT NOT NULL, reward JSON DEFAULT \'[]\' NOT NULL, cost JSON DEFAULT \'[]\' NOT NULL, code VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C82E7477153098 ON clients (code)');
        $this->addSql('ALTER TABLE battle_jury_member_user_relations ADD CONSTRAINT FK_B7BAFC1CA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE battle_jury_member_user_relations ADD CONSTRAINT FK_B7BAFC1C2298D193 FOREIGN KEY (stage_id) REFERENCES battle_stages (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE battle_jury_member_user_relations ADD CONSTRAINT FK_B7BAFC1C7927E467 FOREIGN KEY (spirits_id) REFERENCES battle_spirits (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE autograph_crowd_person_producer_user_relations ADD CONSTRAINT FK_972C99FA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE autograph_crowd_person_producer_user_relations ADD CONSTRAINT FK_972C99F8BAC62AF FOREIGN KEY (city_id) REFERENCES autograph_cities (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE autograph_crowd_person_producer_user_relations ADD CONSTRAINT FK_972C99FCEC5A0B8 FOREIGN KEY (crowd_person_producer) REFERENCES autograph_crowd_person_producers (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE autograph_crowd_person_fans ADD CONSTRAINT FK_96744ABFBF396750 FOREIGN KEY (id) REFERENCES autograph_crowd_persons (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE autograph_city_crowd_person_relations ADD CONSTRAINT FK_15B451848BAC62AF FOREIGN KEY (city_id) REFERENCES autograph_cities (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE autograph_city_crowd_person_relations ADD CONSTRAINT FK_15B4518499A0AB13 FOREIGN KEY (crowd_person_id) REFERENCES autograph_crowd_persons (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE autograph_crowd_persons_user_relations ADD CONSTRAINT FK_13203D8AA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE autograph_crowd_persons_user_relations ADD CONSTRAINT FK_13203D8A8BAC62AF FOREIGN KEY (city_id) REFERENCES autograph_cities (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE autograph_crowd_persons_user_relations ADD CONSTRAINT FK_13203D8AC3200087 FOREIGN KEY (crowd_person_1) REFERENCES autograph_crowd_persons (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE autograph_crowd_persons_user_relations ADD CONSTRAINT FK_13203D8A5A29513D FOREIGN KEY (crowd_person_2) REFERENCES autograph_crowd_persons (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE autograph_crowd_persons_user_relations ADD CONSTRAINT FK_13203D8A2D2E61AB FOREIGN KEY (crowd_person_3) REFERENCES autograph_crowd_persons (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE autograph_bags ADD CONSTRAINT FK_FBFB127A247E8347 FOREIGN KEY (crowd_persons_user_relation_id) REFERENCES autograph_crowd_persons_user_relations (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE autograph_crowd_person_haters ADD CONSTRAINT FK_7D8DACC9BF396750 FOREIGN KEY (id) REFERENCES autograph_crowd_persons (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE autograph_crowd_person_producers ADD CONSTRAINT FK_C30D0C5EBF396750 FOREIGN KEY (id) REFERENCES autograph_crowd_persons (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_things ADD CONSTRAINT FK_AB64A8012469DE2 FOREIGN KEY (category_id) REFERENCES user_thing_categories (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_client_relations ADD CONSTRAINT FK_82592DAAA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_client_relations ADD CONSTRAINT FK_82592DAA19EB6921 FOREIGN KEY (client_id) REFERENCES clients (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_role_relations ADD CONSTRAINT FK_2E1DB3FDA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_role_relations ADD CONSTRAINT FK_2E1DB3FDD60322AC FOREIGN KEY (role_id) REFERENCES roles (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE achievement_quests ADD CONSTRAINT FK_7F8910A9BF396750 FOREIGN KEY (id) REFERENCES achievement_types (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE achievement_status_user_relations ADD CONSTRAINT FK_61154DB158389F76 FOREIGN KEY (achievement_status_id) REFERENCES achievement_statuses (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE achievement_status_user_relations ADD CONSTRAINT FK_61154DB1A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE achievement_status_user_relations ADD CONSTRAINT FK_61154DB1BF396750 FOREIGN KEY (id) REFERENCES achievement_type_user_relations (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE achievement_particles ADD CONSTRAINT FK_FF9AA6B14B157427 FOREIGN KEY (achievement_type_id) REFERENCES achievement_types (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE achievement_particles ADD CONSTRAINT FK_FF9AA6B1BF396750 FOREIGN KEY (id) REFERENCES achievement_types (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE achievement_quest_user_relations ADD CONSTRAINT FK_1D4E9802EC1CB099 FOREIGN KEY (achievement_quest_id) REFERENCES achievement_quests (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE achievement_quest_user_relations ADD CONSTRAINT FK_1D4E9802A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE achievement_quest_user_relations ADD CONSTRAINT FK_1D4E9802BF396750 FOREIGN KEY (id) REFERENCES achievement_type_user_relations (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE achievement_particle_user_relations ADD CONSTRAINT FK_4334A72E4C05BE6A FOREIGN KEY (achievement_particle_id) REFERENCES achievement_particles (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE achievement_particle_user_relations ADD CONSTRAINT FK_4334A72EA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE achievement_particle_user_relations ADD CONSTRAINT FK_4334A72EBF396750 FOREIGN KEY (id) REFERENCES achievement_type_user_relations (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE achievement_statuses ADD CONSTRAINT FK_8772307CBF396750 FOREIGN KEY (id) REFERENCES achievement_types (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_profiles ADD CONSTRAINT FK_6BBD6130A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_profiles ADD CONSTRAINT FK_6BBD61305FB14BA7 FOREIGN KEY (level_id) REFERENCES user_levels (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bonus_system_bonuses ADD CONSTRAINT FK_38B2030A44AC3583 FOREIGN KEY (operation_id) REFERENCES bonus_system_operations (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bonus_system_bonuses ADD CONSTRAINT FK_38B2030AC54C8C93 FOREIGN KEY (type_id) REFERENCES bonus_system_types (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bonus_system_user_bonus_relations ADD CONSTRAINT FK_E327B621A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bonus_system_user_bonus_relations ADD CONSTRAINT FK_E327B62169545666 FOREIGN KEY (bonus_id) REFERENCES bonus_system_bonuses (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_events ADD CONSTRAINT FK_36D54C77A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_profile_user_thing_relations ADD CONSTRAINT FK_73E69CB9CCFA12B8 FOREIGN KEY (profile_id) REFERENCES user_profiles (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_profile_user_thing_relations ADD CONSTRAINT FK_73E69CB9C36906A7 FOREIGN KEY (thing_id) REFERENCES user_things (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE battle_jury_member_user_relations DROP CONSTRAINT FK_B7BAFC1C2298D193');
        $this->addSql('ALTER TABLE battle_jury_member_user_relations DROP CONSTRAINT FK_B7BAFC1C7927E467');
        $this->addSql('ALTER TABLE autograph_crowd_person_fans DROP CONSTRAINT FK_96744ABFBF396750');
        $this->addSql('ALTER TABLE autograph_city_crowd_person_relations DROP CONSTRAINT FK_15B4518499A0AB13');
        $this->addSql('ALTER TABLE autograph_crowd_persons_user_relations DROP CONSTRAINT FK_13203D8AC3200087');
        $this->addSql('ALTER TABLE autograph_crowd_persons_user_relations DROP CONSTRAINT FK_13203D8A5A29513D');
        $this->addSql('ALTER TABLE autograph_crowd_persons_user_relations DROP CONSTRAINT FK_13203D8A2D2E61AB');
        $this->addSql('ALTER TABLE autograph_crowd_person_haters DROP CONSTRAINT FK_7D8DACC9BF396750');
        $this->addSql('ALTER TABLE autograph_crowd_person_producers DROP CONSTRAINT FK_C30D0C5EBF396750');
        $this->addSql('ALTER TABLE autograph_crowd_person_producer_user_relations DROP CONSTRAINT FK_972C99F8BAC62AF');
        $this->addSql('ALTER TABLE autograph_city_crowd_person_relations DROP CONSTRAINT FK_15B451848BAC62AF');
        $this->addSql('ALTER TABLE autograph_crowd_persons_user_relations DROP CONSTRAINT FK_13203D8A8BAC62AF');
        $this->addSql('ALTER TABLE autograph_bags DROP CONSTRAINT FK_FBFB127A247E8347');
        $this->addSql('ALTER TABLE autograph_crowd_person_producer_user_relations DROP CONSTRAINT FK_972C99FCEC5A0B8');
        $this->addSql('ALTER TABLE user_profile_user_thing_relations DROP CONSTRAINT FK_73E69CB9C36906A7');
        $this->addSql('ALTER TABLE battle_jury_member_user_relations DROP CONSTRAINT FK_B7BAFC1CA76ED395');
        $this->addSql('ALTER TABLE autograph_crowd_person_producer_user_relations DROP CONSTRAINT FK_972C99FA76ED395');
        $this->addSql('ALTER TABLE autograph_crowd_persons_user_relations DROP CONSTRAINT FK_13203D8AA76ED395');
        $this->addSql('ALTER TABLE user_client_relations DROP CONSTRAINT FK_82592DAAA76ED395');
        $this->addSql('ALTER TABLE user_role_relations DROP CONSTRAINT FK_2E1DB3FDA76ED395');
        $this->addSql('ALTER TABLE achievement_status_user_relations DROP CONSTRAINT FK_61154DB1A76ED395');
        $this->addSql('ALTER TABLE achievement_quest_user_relations DROP CONSTRAINT FK_1D4E9802A76ED395');
        $this->addSql('ALTER TABLE achievement_particle_user_relations DROP CONSTRAINT FK_4334A72EA76ED395');
        $this->addSql('ALTER TABLE user_profiles DROP CONSTRAINT FK_6BBD6130A76ED395');
        $this->addSql('ALTER TABLE bonus_system_user_bonus_relations DROP CONSTRAINT FK_E327B621A76ED395');
        $this->addSql('ALTER TABLE user_events DROP CONSTRAINT FK_36D54C77A76ED395');
        $this->addSql('ALTER TABLE achievement_quests DROP CONSTRAINT FK_7F8910A9BF396750');
        $this->addSql('ALTER TABLE achievement_particles DROP CONSTRAINT FK_FF9AA6B14B157427');
        $this->addSql('ALTER TABLE achievement_particles DROP CONSTRAINT FK_FF9AA6B1BF396750');
        $this->addSql('ALTER TABLE achievement_statuses DROP CONSTRAINT FK_8772307CBF396750');
        $this->addSql('ALTER TABLE achievement_quest_user_relations DROP CONSTRAINT FK_1D4E9802EC1CB099');
        $this->addSql('ALTER TABLE achievement_status_user_relations DROP CONSTRAINT FK_61154DB1BF396750');
        $this->addSql('ALTER TABLE achievement_quest_user_relations DROP CONSTRAINT FK_1D4E9802BF396750');
        $this->addSql('ALTER TABLE achievement_particle_user_relations DROP CONSTRAINT FK_4334A72EBF396750');
        $this->addSql('ALTER TABLE achievement_particle_user_relations DROP CONSTRAINT FK_4334A72E4C05BE6A');
        $this->addSql('ALTER TABLE achievement_status_user_relations DROP CONSTRAINT FK_61154DB158389F76');
        $this->addSql('ALTER TABLE user_role_relations DROP CONSTRAINT FK_2E1DB3FDD60322AC');
        $this->addSql('ALTER TABLE user_profile_user_thing_relations DROP CONSTRAINT FK_73E69CB9CCFA12B8');
        $this->addSql('ALTER TABLE user_profiles DROP CONSTRAINT FK_6BBD61305FB14BA7');
        $this->addSql('ALTER TABLE user_things DROP CONSTRAINT FK_AB64A8012469DE2');
        $this->addSql('ALTER TABLE bonus_system_bonuses DROP CONSTRAINT FK_38B2030A44AC3583');
        $this->addSql('ALTER TABLE bonus_system_bonuses DROP CONSTRAINT FK_38B2030AC54C8C93');
        $this->addSql('ALTER TABLE bonus_system_user_bonus_relations DROP CONSTRAINT FK_E327B62169545666');
        $this->addSql('ALTER TABLE user_client_relations DROP CONSTRAINT FK_82592DAA19EB6921');
        $this->addSql('DROP TABLE battle_jury_member_user_relations');
        $this->addSql('DROP TABLE battle_stages');
        $this->addSql('DROP TABLE battle_spirits');
        $this->addSql('DROP TABLE autograph_crowd_person_producer_user_relations');
        $this->addSql('DROP TABLE autograph_crowd_persons');
        $this->addSql('DROP TABLE autograph_crowd_person_fans');
        $this->addSql('DROP TABLE autograph_cities');
        $this->addSql('DROP TABLE autograph_city_crowd_person_relations');
        $this->addSql('DROP TABLE autograph_crowd_persons_user_relations');
        $this->addSql('DROP TABLE autograph_bags');
        $this->addSql('DROP TABLE autograph_crowd_person_haters');
        $this->addSql('DROP TABLE autograph_crowd_person_producers');
        $this->addSql('DROP TABLE user_things');
        $this->addSql('DROP TABLE user_client_relations');
        $this->addSql('DROP TABLE users');
        $this->addSql('DROP TABLE user_role_relations');
        $this->addSql('DROP TABLE achievement_types');
        $this->addSql('DROP TABLE achievement_quests');
        $this->addSql('DROP TABLE achievement_type_user_relations');
        $this->addSql('DROP TABLE achievement_status_user_relations');
        $this->addSql('DROP TABLE achievement_particles');
        $this->addSql('DROP TABLE achievement_quest_user_relations');
        $this->addSql('DROP TABLE achievement_particle_user_relations');
        $this->addSql('DROP TABLE achievement_statuses');
        $this->addSql('DROP TABLE roles');
        $this->addSql('DROP TABLE user_profiles');
        $this->addSql('DROP TABLE user_levels');
        $this->addSql('DROP TABLE user_thing_categories');
        $this->addSql('DROP TABLE bonus_system_operations');
        $this->addSql('DROP TABLE bonus_system_types');
        $this->addSql('DROP TABLE bonus_system_bonuses');
        $this->addSql('DROP TABLE bonus_system_user_bonus_relations');
        $this->addSql('DROP TABLE user_events');
        $this->addSql('DROP TABLE user_profile_user_thing_relations');
        $this->addSql('DROP TABLE clients');
    }
}
