<?php

namespace AppBundle\Repository\Achievement;

use AppBundle\Entity\Achievement;
use AppBundle\Entity\Achievement\AchievementParticle;
use AppBundle\Entity\Achievement\AchievementParticleUserRelation;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class AchievementParticleUserRelationRepository extends EntityRepository
{
    public function findOrCreateOneByUserAchievementParticle(User $user, AchievementParticle $achievementParticle)
    {
        /** @var AchievementParticleUserRelation $achievementParticleUserRelation */
        if (!($achievementParticleUserRelation = $this->findOneBy(['user' => $user, 'achievementParticle' => $achievementParticle]))) {
            $achievementParticleUserRelation = new AchievementParticleUserRelation();
            $achievementParticleUserRelation->setUser($user);
            $achievementParticleUserRelation->setAchievementParticle($achievementParticle);

            $this->getEntityManager()->persist($achievementParticleUserRelation);
            $this->getEntityManager()->flush();
        }

        return $achievementParticleUserRelation;
    }
}
