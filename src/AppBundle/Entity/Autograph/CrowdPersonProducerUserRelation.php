<?php

namespace AppBundle\Entity\Autograph;

use AppBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="autograph_crowd_person_producer_user_relations")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Autograph\CrowdPersonProducerUserRelationRepository")
 */
class CrowdPersonProducerUserRelation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="crowdPersonProducerUserRelations")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var int
     *
     * @ORM\Column(name="place", type="smallint", columnDefinition="SMALLINT CHECK (place IN (1, 2, 3))")
     */
    private $place;

    /**
     * @var City
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Autograph\City", inversedBy="crowdPersonProducerUserRelations")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id" )
     */
    private $city;

    /**
     * @var int
     *
     * @ORM\Column(name="steps", type="integer", options={"default": "1"})
     */
    private $steps = 1;

    /**
     * @var CrowdPersonProducer
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Autograph\CrowdPersonProducer", inversedBy="crowdPersonProducerUserRelation")
     * @ORM\JoinColumn(name="crowd_person_producer", referencedColumnName="id" )
     */
    private $crowdPersonProducer;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * @param int $place
     */
    public function setPlace($place)
    {
        $this->place = $place;
    }

    /**
     * @return City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param City $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return int
     */
    public function getSteps()
    {
        return $this->steps;
    }

    /**
     * @param int $steps
     */
    public function setSteps($steps)
    {
        $this->steps = $steps;
    }

    /**
     * @return CrowdPersonProducer
     */
    public function getCrowdPersonProducer()
    {
        return $this->crowdPersonProducer;
    }

    /**
     * @param CrowdPersonProducer $crowdPersonProducer
     */
    public function setCrowdPersonProducer($crowdPersonProducer)
    {
        $this->crowdPersonProducer = $crowdPersonProducer;
    }

    public function goToCity(City $city)
    {
        $this->setCity($city);
        $this->setPlace(mt_rand(1, 3));
        $this->setSteps(mt_rand(1, $city->getSteps() - 1));
    }
}