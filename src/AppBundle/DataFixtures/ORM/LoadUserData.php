<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Role;
use AppBundle\Entity\UserLevel;
use AppBundle\Entity\UserProfile;
use AppBundle\Repository\UserLevelRepository;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

class LoadUserData implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    const ADMIN_USER_NAME = 'admin';
    const ADMIN_USER_PASSWORD = 'slavamuraveydancingslavamuravey';
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ADMIN_USER_EMAIL = 'slavamuravey@mail.ru';

    use ContainerAwareTrait;

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setUsername(self::ADMIN_USER_NAME);

        $role = new Role();
        $role->setName(self::ADMIN_USER_NAME);
        $role->setRole(self::ROLE_ADMIN);
        $user->addRole($role);

        /** @var UserLevelRepository $userLevelRepository */
        $userLevelRepository = $manager->getRepository(UserLevel::class);
        $level = $userLevelRepository->getLevelOne();

        $profile = new UserProfile();
        $profile->setSex('m');
        $profile->setEmail(self::ADMIN_USER_EMAIL);
        $profile->setUser($user);
        $profile->setLevel($level);
        $user->setProfile($profile);

        /** @var EncoderFactoryInterface $factory */
        $factory = $this->container->get('security.encoder_factory');
        $encoder = $factory->getEncoder($user);
        $password = $encoder->encodePassword(self::ADMIN_USER_PASSWORD, $user->getSalt());
        $user->setPassword($password);

        $manager->persist($user);
        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 2;
    }
}