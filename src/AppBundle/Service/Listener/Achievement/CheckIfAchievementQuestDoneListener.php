<?php

namespace AppBundle\Service\Listener\Achievement;

use AppBundle\Entity\Achievement\AchievementParticle;
use AppBundle\Entity\Achievement\AchievementQuest;
use AppBundle\Entity\Achievement\AchievementQuestUserRelation;
use AppBundle\Repository\Achievement\AchievementParticleRepository;
use AppBundle\Repository\Achievement\AchievementQuestUserRelationRepository;
use AppBundle\Service\Event\Achievement\AchievementParticleDoneEvent;
use AppBundle\Service\Event\Achievement\AchievementQuestDoneEvent;
use AppBundle\StoreEvents;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\ContainerAwareEventDispatcher;

class CheckIfAchievementQuestDoneListener
{
    /** @var EntityManager */
    private $em;

    /** @var ContainerAwareEventDispatcher */
    private $dispatcher;

    /**
     * @param EntityManager $em
     * @param ContainerAwareEventDispatcher $dispatcher
     */
    public function __construct(EntityManager $em, $dispatcher)
    {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param AchievementParticleDoneEvent $event
     */
    public function onCheck(AchievementParticleDoneEvent $event)
    {
        $achievementParticleUserRelation = $event->getAchievementParticleUserRelation();
        $achievementParticle = $achievementParticleUserRelation->getAchievementParticle();
        $achievementType = $achievementParticle->getAchievementType();
        if (!(is_object($achievementType) && $achievementType instanceof AchievementQuest)) {
            return;
        }

        /** @var AchievementQuestUserRelationRepository $achievementQuestUserRelationRepository */
        $achievementQuestUserRelationRepository = $this->em->getRepository(AchievementQuestUserRelation::class);

        $user = $event->getUser();
        $achievementQuestUserRelation = $achievementQuestUserRelationRepository->findOrCreateOneByUserAchievementQuest($user, $achievementType);

        if ($achievementQuestUserRelation->isDone()) {
            return;
        }

        /** @var AchievementParticleRepository $achievementParticleRepository */
        $achievementParticleRepository = $this->em->getRepository(AchievementParticle::class);
        $achievementParticleRelations = $achievementParticleRepository->getAchievementParticleRelationsForAchievementType($user, $achievementType);

        foreach ($achievementParticleRelations as $achievementParticleRelation) {
            if ($achievementParticleRelation['rel_id'] != $achievementParticleUserRelation->getId()
                && !$achievementParticleRelation['done']) {
                return;
            }
        }

        $achievementQuestUserRelation->setDone(true);
        $event = new AchievementQuestDoneEvent($user, [
            'code' => $achievementType->getCode(),
        ]);
        $event->setAchievementQuestUserRelation($achievementQuestUserRelation);
        $this->em->flush($achievementQuestUserRelation);
        $this->dispatcher->dispatch(StoreEvents::ACHIEVEMENT_QUEST_DONE, $event);
    }
}