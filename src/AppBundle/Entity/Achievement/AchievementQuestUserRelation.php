<?php

namespace AppBundle\Entity\Achievement;

use AppBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="achievement_quest_user_relations", uniqueConstraints={@ORM\UniqueConstraint(name="uniq_user_id_achievement_quest_id", columns={"user_id", "achievement_quest_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Achievement\AchievementQuestUserRelationRepository")
 * @ORM\HasLifecycleCallbacks
 */
class AchievementQuestUserRelation extends AchievementTypeUserRelation
{
    /**
     * @var AchievementQuest
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Achievement\AchievementQuest", inversedBy="achievementQuestUserRelations")
     * @ORM\JoinColumn(name="achievement_quest_id", referencedColumnName="id", nullable=false)
     */
    private $achievementQuest;

    /**
     * @var bool
     *
     * @ORM\Column(name="current", type="boolean", options={"default": false})
     */
    private $current = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="achievementQuestUserRelations")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @return AchievementQuest
     */
    public function getAchievementQuest()
    {
        return $this->achievementQuest;
    }

    /**
     * @param AchievementQuest $achievementQuest
     */
    public function setAchievementQuest($achievementQuest)
    {
        $this->achievementQuest = $achievementQuest;
    }

    /**
     * @return boolean
     */
    public function isCurrent()
    {
        return $this->current;
    }

    /**
     * @param boolean $current
     */
    public function setCurrent($current)
    {
        $this->current = $current;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updateUpdatedAt()
    {
        $this->setUpdatedAt(new \DateTime());
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }
}
