<?php

namespace AppBundle\Service\Event\Achievement;

use AppBundle\Entity\Achievement\AchievementStatusUserRelation;
use AppBundle\Service\Event\AbstractEvent;

class AchievementStatusDoneEvent extends AbstractEvent
{
    /** @var AchievementStatusUserRelation */
    private $achievementStatusUserRelation;

    /**
     * @return AchievementStatusUserRelation
     */
    public function getAchievementStatusUserRelation()
    {
        return $this->achievementStatusUserRelation;
    }

    /**
     * @param AchievementStatusUserRelation $achievementStatusUserRelation
     */
    public function setAchievementStatusUserRelation(AchievementStatusUserRelation $achievementStatusUserRelation)
    {
        $this->achievementStatusUserRelation = $achievementStatusUserRelation;
    }
}