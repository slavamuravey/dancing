<?php

namespace AppBundle\Entity\Autograph;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="autograph_crowd_person_haters")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Autograph\CrowdPersonHaterRepository")
 */
class CrowdPersonHater extends CrowdPerson
{
    const SECURITY_ENERGY_1 = 1;
    const SECURITY_ENERGY_2 = 2;
    const SECURITY_ENERGY_3 = 3;
    const SECURITY_ENERGY_5 = 5;

    const HATER_LIGHT_1 = 'hater_light_1';
    const HATER_LIGHT_2 = 'hater_light_2';
    const HATER_NORMAL_1 = 'hater_normal_1';
    const HATER_NORMAL_2 = 'hater_normal_2';
    const HATER_STRONG_1 = 'hater_strong_1';
    const HATER_STRONG_2 = 'hater_strong_2';

    /**
     * @var array
     *
     * @ORM\Column(name="cost", type="json_array", options={"default": "[]"})
     */
    private $cost = [];

    /**
     * @return array
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @param array $cost
     */
    public function setCost($cost)
    {
        $this->cost = $cost;
    }

    public function getType()
    {
        return self::HATER_TYPE;
    }

    public function getCostSecurityEnergy()
    {
        if (array_key_exists(CrowdPersonsUserRelation::class, $this->getCost()) && array_key_exists('securityEnergy', $this->getCost()[CrowdPersonsUserRelation::class])) {
            return $this->getCost()[CrowdPersonsUserRelation::class]['securityEnergy'];
        }

        return 0;
    }
}