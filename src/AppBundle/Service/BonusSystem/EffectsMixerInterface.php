<?php

namespace AppBundle\Service\BonusSystem;

interface EffectsMixerInterface
{
    /**
     * @param mixed $baseValue
     * @param array|Effect[] $effects
     * @return mixed
     */
    public function mix($baseValue, array $effects);
}
