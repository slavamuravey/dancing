<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Autograph\City;
use AppBundle\Entity\Autograph\CrowdPersonProducer;
use AppBundle\Entity\Autograph\CrowdPersonProducerUserRelation;
use AppBundle\Repository\Autograph\CityRepository;
use AppBundle\Repository\Autograph\CrowdPersonProducerRepository;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadCrowdPersonProducerUserRelationsData implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $crowdPersonProducerUserRelation = new CrowdPersonProducerUserRelation();

        /** @var CrowdPersonProducerRepository $crowdPersonProducerRepository */
        $crowdPersonProducerRepository = $manager->getRepository(CrowdPersonProducer::class);

        /** @var CrowdPersonProducer $producer1 */
        $producer1 = $crowdPersonProducerRepository->findOneBy(['code' => CrowdPersonProducer::PRODUCER_1]);

        /** @var CityRepository $cityRepository */
        $cityRepository = $manager->getRepository(City::class);

        $crowdPersonProducerUserRelation->setCrowdPersonProducer($producer1);
        $city1 = $cityRepository->getCityByNumber(1);
        $crowdPersonProducerUserRelation->goToCity($city1);
        $manager->persist($crowdPersonProducerUserRelation);
        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 11;
    }
}