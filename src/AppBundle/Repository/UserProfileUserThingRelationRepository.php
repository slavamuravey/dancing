<?php

namespace AppBundle\Repository;

use AppBundle\Entity\UserProfile;
use AppBundle\Entity\UserThing;
use Doctrine\ORM\EntityRepository;

class UserProfileUserThingRelationRepository extends EntityRepository
{
    /**
     * @param UserProfile $profile
     * @param UserThing $thing
     * @return bool
     */
    public function hasUserProfileUserThing(UserProfile $profile, UserThing $thing)
    {
        return (bool)$this->findOneBy(['profile' => $profile, 'thing' => $thing,]);
    }

    /**
     * @param int $profileId
     * @return array
     */
    public function getImagePathsForWearingThingsByProfile($profileId)
    {
        $connection = $this->getEntityManager()->getConnection();

        $thingsImagesDir = __DIR__ . '/../../../web/images/things';

        $sql = <<<SQL
SELECT
    :things_images_dir || '/' || ut.img || '.png'
FROM user_profile_user_thing_relations uputr
INNER JOIN user_things ut ON uputr.thing_id = ut.id
WHERE uputr.profile_id = :profile_id AND uputr.wearing = TRUE
ORDER BY ut.layer_level ASC
SQL;

        $stmt = $connection->prepare($sql);
        $stmt->execute([
            'profile_id' => $profileId,
            'things_images_dir' => $thingsImagesDir
        ]);

        return $stmt->fetchAll(\PDO::FETCH_COLUMN);
    }
}
