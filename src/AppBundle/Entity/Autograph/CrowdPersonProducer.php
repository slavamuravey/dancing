<?php

namespace AppBundle\Entity\Autograph;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="autograph_crowd_person_producers")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Autograph\CrowdPersonProducerRepository")
 */
class CrowdPersonProducer extends CrowdPerson
{
    const PRODUCER_1 = 'producer_1';

    /**
     * @var CrowdPersonProducerUserRelation
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Autograph\CrowdPersonProducerUserRelation", mappedBy="crowdPersonProducer")
     */
    private $crowdPersonProducerUserRelation;

    /**
     * @return CrowdPersonProducerUserRelation
     */
    public function getCrowdPersonProducerUserRelation()
    {
        return $this->crowdPersonProducerUserRelation;
    }

    /**
     * @param CrowdPersonProducerUserRelation $crowdPersonProducerUserRelation
     */
    public function setCrowdPersonProducerUserRelation($crowdPersonProducerUserRelation)
    {
        $this->crowdPersonProducerUserRelation = $crowdPersonProducerUserRelation;
    }

    public function getType()
    {
        return self::PRODUCER_TYPE;
    }
}