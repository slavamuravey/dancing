<?php

namespace Tests\AppBundle\Service\ThingsImagesCompositor;

use AppBundle\Service\ThingsImagesCompositor;
use Symfony\Component\Filesystem\Filesystem;

class ThingsImagesCompositorTest extends \PHPUnit_Framework_TestCase
{
    public function testGenerate()
    {
        $cacheDir = __DIR__ . '/images/cache';
        $fs = new FileSystem();

        $images = [
            __DIR__ . '/images/1.png',
            __DIR__ . '/images/2.png',
            __DIR__ . '/images/3.png',
        ];

        $thingsImagesCompositor = new ThingsImagesCompositor($cacheDir, $fs);
        $destImagePath = $thingsImagesCompositor->generate($images);

        $this->assertTrue(is_file($destImagePath));

        unlink($destImagePath);
        rmdir(dirname($destImagePath));
    }
}
