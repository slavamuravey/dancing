<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\UserThing;
use AppBundle\Entity\UserThingCategory;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserThingsData implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $userThingCategoryHead = new UserThingCategory();
        $userThingCategoryHead->setCode(UserThingCategory::HEAD);
        $userThingCategoryHead->setName('Голова');
        $manager->persist($userThingCategoryHead);

        $userThingCategoryBody = new UserThingCategory();
        $userThingCategoryBody->setCode(UserThingCategory::BODY);
        $userThingCategoryBody->setName('Тело');
        $manager->persist($userThingCategoryBody);

        $userThingCategoryShoes = new UserThingCategory();
        $userThingCategoryShoes->setCode(UserThingCategory::SHOES);
        $userThingCategoryShoes->setName('Обувь');
        $manager->persist($userThingCategoryShoes);

        $userThingCategoryJewelry = new UserThingCategory();
        $userThingCategoryJewelry->setCode(UserThingCategory::JEWELRY);
        $userThingCategoryJewelry->setName('Украшения');
        $manager->persist($userThingCategoryJewelry);

        $userThing = new UserThing();
        $userThing->setCode(UserThing::HAT);
        $userThing->setSex('m');
        $userThing->setLayerLevel(0);
        $userThing->setImg('1');
        $userThing->setCategory($userThingCategoryHead);
        $manager->persist($userThing);

        $userThing = new UserThing();
        $userThing->setCode(UserThing::TOP);
        $userThing->setSex('f');
        $userThing->setLayerLevel(1);
        $userThing->setImg('2');
        $userThing->setCategory($userThingCategoryJewelry);
        $manager->persist($userThing);

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 5;
    }
}