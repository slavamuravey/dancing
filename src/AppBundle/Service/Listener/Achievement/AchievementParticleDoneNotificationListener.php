<?php

namespace AppBundle\Service\Listener\Achievement;

use AppBundle\Service\Event\Achievement\AchievementParticleDoneEvent;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class AchievementParticleDoneNotificationListener
{
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @param SessionInterface $session
     */
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function onNotify(AchievementParticleDoneEvent $event)
    {
        $this->session->getFlashBag()->add('achievement_particle_done_notice', 'Достижение ' . $event->getAchievementParticleUserRelation()->getAchievementParticle()->getCode() . ' выполнено');
    }
}