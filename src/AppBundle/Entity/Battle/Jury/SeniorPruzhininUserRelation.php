<?php

namespace AppBundle\Entity\Battle\Jury;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Battle\Jury\SeniorPruzhininUserRelationRepository")
 */
class SeniorPruzhininUserRelation extends MemberUserRelation
{
    /** @var float */
    private $userParamBuff = 100;

    /**
     * @return float
     */
    public function calculate()
    {
        return ceil(
            $this->getStage()->getStageValue()
            * (mt_rand($this->getMinDispersionValue(), $this->getMaxDispersionValue()) / 100)
            * ($this->getSpirits()->getValue() / 100)
            * ($this->userParamBuff / 100)
        );
    }

    public function applyUserParamsBuff()
    {
        $profile = $this->getUser()->getProfile();
        if ($profile->getSportsTraining() > $profile->getPopularity()) {
            $this->userParamBuff = 90;
        }
    }
}