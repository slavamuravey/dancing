<?php

namespace AppBundle\Service\Event;

use AppBundle\Service\Event\Achievement\AchievementParticleDataAwareInterface;
use AppBundle\Service\Event\Achievement\AchievementParticleDataAwareTrait;

class UserLevelUpTo2Event extends AbstractEvent implements AchievementParticleDataAwareInterface
{
    use AchievementParticleDataAwareTrait;
}