<?php

namespace AppBundle\Service\Listener;

use AppBundle\Entity\Battle\Jury\MemberUserRelation;
use AppBundle\Entity\Battle\Spirits;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;

class SpiritsSetterListener
{
    /**
     * @ORM\PrePersist
     *
     * @param MemberUserRelation $memberUserRelation
     * @param LifecycleEventArgs $event
     */
    public function setSpirits(MemberUserRelation $memberUserRelation, $event)
    {
        $em = $event->getEntityManager();

        $spiritsRepository = $em->getRepository(Spirits::class);

        switch (mt_rand(0, 2)) {
            case 0:
                $code = Spirits::CODE_BAD;
                break;
            case 1:
                $code = Spirits::CODE_NORMAL;
                break;
            default:
                $code = Spirits::CODE_GOOD;
                break;
        }

        /** @var Spirits $spirits */
        $spirits = $spiritsRepository->findOneBy(['code' => $code]);
        $memberUserRelation->setSpirits($spirits);
    }
}