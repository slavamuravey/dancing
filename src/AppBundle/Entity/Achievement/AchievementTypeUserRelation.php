<?php

namespace AppBundle\Entity\Achievement;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="achievement_type_user_relations")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="rel", type="string")
 * @ORM\DiscriminatorMap({
 *     "quest" = "AchievementQuestUserRelation",
 *     "status" = "AchievementStatusUserRelation",
 *     "particle" = "AchievementParticleUserRelation"
 * })
 */
abstract class AchievementTypeUserRelation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="done", type="boolean", options={"default": false})
     */
    private $done = false;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return boolean
     */
    public function isDone()
    {
        return $this->done;
    }

    /**
     * @param boolean $done
     */
    public function setDone($done)
    {
        $this->done = $done;
    }
}