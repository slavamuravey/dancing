<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160703000941 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE bonus_system_bonuses DROP CONSTRAINT fk_38b2030a44ac3583');
        $this->addSql('ALTER TABLE bonus_system_bonuses DROP CONSTRAINT fk_38b2030ac54c8c93');
        $this->addSql('ALTER TABLE bonus_system_user_bonus_relations DROP CONSTRAINT fk_e327b62169545666');
        $this->addSql('DROP SEQUENCE bonus_system_operations_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE bonus_system_types_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE bonus_system_bonuses_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE bonus_system_user_bonus_relations_id_seq CASCADE');
        $this->addSql('DROP TABLE bonus_system_operations');
        $this->addSql('DROP TABLE bonus_system_types');
        $this->addSql('DROP TABLE bonus_system_bonuses');
        $this->addSql('DROP TABLE bonus_system_user_bonus_relations');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE bonus_system_operations_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE bonus_system_types_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE bonus_system_bonuses_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE bonus_system_user_bonus_relations_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE bonus_system_operations (id SERIAL NOT NULL, service_name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX uniq_bf1db9bc9060caea ON bonus_system_operations (service_name)');
        $this->addSql('CREATE TABLE bonus_system_types (id SERIAL NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX uniq_d2a5b7515e237e06 ON bonus_system_types (name)');
        $this->addSql('CREATE TABLE bonus_system_bonuses (id SERIAL NOT NULL, operation_id INT NOT NULL, type_id INT NOT NULL, code VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, applying_value DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_38b2030ac54c8c93 ON bonus_system_bonuses (type_id)');
        $this->addSql('CREATE UNIQUE INDEX uniq_38b2030a77153098 ON bonus_system_bonuses (code)');
        $this->addSql('CREATE INDEX idx_38b2030a44ac3583 ON bonus_system_bonuses (operation_id)');
        $this->addSql('CREATE TABLE bonus_system_user_bonus_relations (id SERIAL NOT NULL, user_id INT NOT NULL, bonus_id INT NOT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_e327b621a76ed395 ON bonus_system_user_bonus_relations (user_id)');
        $this->addSql('CREATE INDEX idx_e327b62169545666 ON bonus_system_user_bonus_relations (bonus_id)');
        $this->addSql('ALTER TABLE bonus_system_bonuses ADD CONSTRAINT fk_38b2030a44ac3583 FOREIGN KEY (operation_id) REFERENCES bonus_system_operations (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bonus_system_bonuses ADD CONSTRAINT fk_38b2030ac54c8c93 FOREIGN KEY (type_id) REFERENCES bonus_system_types (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bonus_system_user_bonus_relations ADD CONSTRAINT fk_e327b621a76ed395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bonus_system_user_bonus_relations ADD CONSTRAINT fk_e327b62169545666 FOREIGN KEY (bonus_id) REFERENCES bonus_system_bonuses (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
