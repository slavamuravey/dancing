<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Autograph\City;
use AppBundle\Repository\Autograph\CityRepository;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadCitiesData implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        /** @var CityRepository $cityRepository */
        $cityRepository = $manager->getRepository(City::class);
        $cityRepository->fillCitiesData();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 8;
    }
}