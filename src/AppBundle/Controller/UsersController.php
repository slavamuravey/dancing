<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use AppBundle\Entity\UserLevel;
use AppBundle\Entity\UserProfile;
use AppBundle\Form\Model\Registration;
use AppBundle\Form\Type\RegistrationType;
use AppBundle\Repository\UserLevelRepository;
use AppBundle\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;

/**
 * @var Route
 */
class UsersController extends AbstractController
{
    const SECURED_AREA_FIREWALL = 'secured_area';

    /**
     * @Route("/users/register", name="users_register")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function registerAction(Request $request)
    {
        $form = $this->createForm(RegistrationType::class, new Registration());

        $form->handleRequest($request);

        if (!$form->isValid() || !$form->isSubmitted()) {
            return $this->render('app/users/register.html.twig', [
                'form' => $form->createView(),
            ]);
        }

        /** @var Registration $registration */
        $registration = $form->getData();

        /** @var User $user */
        $user = $registration->getUser();

        /** @var UserProfile $profile */
        $profile = $user->getProfile();

        $role = new Role();
        $role->setName($user->getUsername());
        $role->setRole(Role::ROLE_USER);
        $user->addRole($role);

        $profile->setUser($user);
        /** @var UserLevelRepository $userLevelRepository */
        $userLevelRepository = $this->getRepository(UserLevel::class);
        $profile->setLevel($userLevelRepository->getLevelOne());
        $user->setProfile($profile);

        $factory = $this->get('security.encoder_factory');
        /** @var PasswordEncoderInterface $encoder */
        $encoder = $factory->getEncoder($user);
        $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
        $user->setPassword($password);

        $em = $this->getEntityManager();
        $em->persist($user);
        $em->flush();

        $this->authorizeUser($user);

        return $this->redirect($this->generateUrl('clients'));
    }

    /**
     * @param User $user
     * @param string $firewall
     */
    private function authorizeUser(User $user, $firewall = self::SECURED_AREA_FIREWALL)
    {
        $token = new UsernamePasswordToken($user, null, $firewall, $user->getRoles());
        $this->get('security.token_storage')->setToken($token);
    }

    /**
     * @Route("/users/list", name="users_list")
     */
    public function listAction()
    {
        /** @var UserRepository $userRepository */
        $userRepository = $this->getRepository(User::class);

        return $this->render('app/users/list.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }
}
