<?php

namespace AppBundle\Service\BonusSystem\Source;

interface SourceInterface
{
    /**
     * @param mixed $object
     * @return array
     */
    public function extractBonuses($object):array;
}
