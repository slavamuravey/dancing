<?php

namespace AppBundle;

final class StoreEvents
{
    const USER_REWARD = 'user.reward';
    const USER_TRY_BUY = 'user.try_buy';
    const USER_DEBIT = 'user.debit';
    const USER_LEVEL_UP = 'user.level_up';
    const USER_LEVEL_UP_TO_2 = 'user.level_up_to_2';
    const ACHIEVEMENT_PARTICLE_DONE = 'achievement.particle.done';
    const ACHIEVEMENT_STATUS_DONE = 'achievement.status.done';
    const ACHIEVEMENT_QUEST_DONE = 'achievement.quest.done';
    const USER_VISIT_PROFILE = 'user.visit_profile';
    const USER_ADD_CLIENT = 'user.add_client';
    const USER_ADD_CLIENT_FACTORY = 'user.add_client_factory';
    const USER_REWARD_EXP = 'user.reward_exp';
    const USER_REWARD_MANI = 'user.reward_mani';
    const USER_REWARD_DANCES = 'user.reward_dances';
}