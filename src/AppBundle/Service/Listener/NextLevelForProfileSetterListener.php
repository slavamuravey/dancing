<?php

namespace AppBundle\Service\Listener;

use AppBundle\Entity\UserLevel;
use AppBundle\Entity\UserProfile;
use AppBundle\Repository\UserLevelRepository;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;

class NextLevelForProfileSetterListener
{
    /**
     * @ORM\PostLoad
     * @ORM\PreFlush
     *
     * @param UserProfile $profile
     * @param LifecycleEventArgs $event
     */
    public function setNextLevel(UserProfile $profile, $event)
    {
        $em = $event->getEntityManager();

        /** @var UserLevelRepository $userLevelRepository */
        $userLevelRepository = $em->getRepository(UserLevel::class);
        $profile->setNextLevel($userLevelRepository->getNextLevelAfter($profile->getLevel()->getLevel()));
    }
}