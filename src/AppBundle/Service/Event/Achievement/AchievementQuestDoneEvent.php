<?php

namespace AppBundle\Service\Event\Achievement;

use AppBundle\Entity\Achievement\AchievementQuestUserRelation;
use AppBundle\Service\Event\AbstractEvent;

class AchievementQuestDoneEvent extends AbstractEvent
{
    /** @var AchievementQuestUserRelation */
    private $achievementQuestUserRelation;

    /**
     * @return AchievementQuestUserRelation
     */
    public function getAchievementQuestUserRelation()
    {
        return $this->achievementQuestUserRelation;
    }

    /**
     * @param AchievementQuestUserRelation $achievementQuestUserRelation
     */
    public function setAchievementQuestUserRelation(AchievementQuestUserRelation $achievementQuestUserRelation)
    {
        $this->achievementQuestUserRelation = $achievementQuestUserRelation;
    }
}