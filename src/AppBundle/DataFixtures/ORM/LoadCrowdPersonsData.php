<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Autograph\Bag;
use AppBundle\Entity\Autograph\CrowdPersonFan;
use AppBundle\Entity\Autograph\CrowdPersonHater;
use AppBundle\Entity\Autograph\CrowdPersonProducer;
use AppBundle\Entity\Autograph\CrowdPersonsUserRelation;
use AppBundle\Entity\UserProfile;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadCrowdPersonsData implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $crowdPersonFan = new CrowdPersonFan();
        $crowdPersonFan->setCode(CrowdPersonFan::FAN_1);
        $crowdPersonFan->setReward([
            Bag::class => [
                'mani' => 50,
            ],
        ]);
        $manager->persist($crowdPersonFan);

        $crowdPersonFan = new CrowdPersonFan();
        $crowdPersonFan->setCode(CrowdPersonFan::FAN_2);
        $crowdPersonFan->setReward([
            UserProfile::class => [
                'exp' => 10,
            ],
        ]);
        $manager->persist($crowdPersonFan);

        $crowdPersonFan = new CrowdPersonFan();
        $crowdPersonFan->setCode(CrowdPersonFan::FAN_3);
        $crowdPersonFan->setReward([
            CrowdPersonsUserRelation::class => [
                'securityEnergy' => 1,
            ],
        ]);
        $manager->persist($crowdPersonFan);

        $crowdPersonFan = new CrowdPersonFan();
        $crowdPersonFan->setCode(CrowdPersonFan::FAN_4);
        $crowdPersonFan->setReward([
            UserProfile::class => [
                'exp' => 20,
            ],
        ]);
        $manager->persist($crowdPersonFan);

        $crowdPersonFan = new CrowdPersonFan();
        $crowdPersonFan->setCode(CrowdPersonFan::FAN_EXTRA_1);
        $crowdPersonFan->setReward([
            Bag::class => [
                'mani' => 100,
            ],
        ]);
        $manager->persist($crowdPersonFan);

        $crowdPersonFan = new CrowdPersonFan();
        $crowdPersonFan->setCode(CrowdPersonFan::FAN_EXTRA_2);
        $crowdPersonFan->setReward([
            Bag::class => [
                'mani' => 200,
            ],
        ]);
        $manager->persist($crowdPersonFan);

        $crowdPersonFan = new CrowdPersonFan();
        $crowdPersonFan->setCode(CrowdPersonFan::FAN_EXTRA_3);
        $crowdPersonFan->setReward([
            Bag::class => [
                'mani' => 300,
            ],
        ]);
        $manager->persist($crowdPersonFan);

        $crowdPersonProducer = new CrowdPersonProducer();
        $crowdPersonProducer->setCode(CrowdPersonProducer::PRODUCER_1);
        $manager->persist($crowdPersonProducer);

        $crowdPersonHater = new CrowdPersonHater();
        $crowdPersonHater->setCost([
            CrowdPersonsUserRelation::class => [
                'securityEnergy' => CrowdPersonHater::SECURITY_ENERGY_1,
            ],
        ]);
        $crowdPersonHater->setCode(CrowdPersonHater::HATER_LIGHT_1);
        $manager->persist($crowdPersonHater);

        $crowdPersonHater = new CrowdPersonHater();
        $crowdPersonHater->setCost([
            CrowdPersonsUserRelation::class => [
                'securityEnergy' => CrowdPersonHater::SECURITY_ENERGY_1,
            ],
        ]);
        $crowdPersonHater->setCode(CrowdPersonHater::HATER_LIGHT_2);
        $manager->persist($crowdPersonHater);

        $crowdPersonHater = new CrowdPersonHater();
        $crowdPersonHater->setCost([
            CrowdPersonsUserRelation::class => [
                'securityEnergy' => CrowdPersonHater::SECURITY_ENERGY_2,
            ],
        ]);
        $crowdPersonHater->setCode(CrowdPersonHater::HATER_NORMAL_1);
        $manager->persist($crowdPersonHater);

        $crowdPersonHater = new CrowdPersonHater();
        $crowdPersonHater->setCost([
            CrowdPersonsUserRelation::class => [
                'securityEnergy' => CrowdPersonHater::SECURITY_ENERGY_2,
            ],
        ]);
        $crowdPersonHater->setCode(CrowdPersonHater::HATER_NORMAL_2);
        $manager->persist($crowdPersonHater);

        $crowdPersonHater = new CrowdPersonHater();
        $crowdPersonHater->setCost([
            CrowdPersonsUserRelation::class => [
                'securityEnergy' => CrowdPersonHater::SECURITY_ENERGY_3,
            ],
        ]);
        $crowdPersonHater->setCode(CrowdPersonHater::HATER_STRONG_1);
        $manager->persist($crowdPersonHater);

        $crowdPersonHater = new CrowdPersonHater();
        $crowdPersonHater->setCost([
            CrowdPersonsUserRelation::class => [
                'securityEnergy' => CrowdPersonHater::SECURITY_ENERGY_3,
            ],
        ]);
        $crowdPersonHater->setCode(CrowdPersonHater::HATER_STRONG_2);
        $manager->persist($crowdPersonHater);

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 9;
    }
}