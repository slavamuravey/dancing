<?php

namespace AppBundle\Repository;

use AppBundle\Entity\User;
use AppBundle\Entity\UserProfile;
use Doctrine\ORM\EntityRepository;

class UserProfileRepository extends EntityRepository implements SearcherByUserInterface
{
    const SPORTS_TRAINING_START_PERCENT = 60;

    const TURNS_COUNT = 5;

    const DISPERSION_POPULARITY = 30;

    const DISPERSION_SPORTS_TRAINING = 50;

    const MAX_PERCENT = 99;

    public function calculateDanceResult(UserProfile $profile)
    {
        $result = 0;

        for ($i = 1; $i <= self::TURNS_COUNT; $i++) {
            $popularity = $profile->getPopularity();
            $sportsTraining = $profile->getSportsTraining();

            $chancePercentPopularity = floor(
                ($popularity / ($popularity + self::DISPERSION_POPULARITY)) * self::MAX_PERCENT
            );
            $chancePercentSportsTraining = floor(
                ($sportsTraining / ($sportsTraining + self::DISPERSION_SPORTS_TRAINING))
                * (self::MAX_PERCENT - self::SPORTS_TRAINING_START_PERCENT)
            ) + self::SPORTS_TRAINING_START_PERCENT;

            $hasChancePopularity = mt_rand(0, self::MAX_PERCENT) <= $chancePercentPopularity;
            $hasChanceSportsTraining = mt_rand(0, self::MAX_PERCENT) <= $chancePercentSportsTraining;

            $multiplicationPopularity = $hasChancePopularity ? 2 : 1;
            $multiplicationSportsTraining = $hasChanceSportsTraining ? 1 : 0;

            $result = $result + $profile->getActing() * $multiplicationPopularity * $multiplicationSportsTraining;
        }

        return $result;
    }

    /**
     * @param User $user
     * @return null|UserProfile
     */
    public function findOneByUser(User $user)
    {
        return $this->findOneBy(['user' => $user]);
    }
}
